/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 04/11/2017
 */
-- Relatorio - Arquivos
CREATE OR REPLACE VIEW view_arquivos(
            projeto_tarefa_arquivos_codigo_arquivo, projeto_tarefa_arquivos_nome_arquivo, projeto_tarefa_arquivos_descricao, 
            projeto_tarefa_arquivos_codigo_usuario, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
            projeto_tarefa_arquivos_codigo_tarefa, projeto_tarefa_arquivos_codigo_projeto)
    AS SELECT projeto_tarefa_arquivos.codigo_arquivo, projeto_tarefa_arquivos.nome_arquivo, projeto_tarefa_arquivos.descricao, 
            projeto_tarefa_arquivos.codigo_usuario, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
            projeto_tarefa_arquivos.codigo_tarefa, 
            projeto_tarefa_arquivos.codigo_projeto
        FROM projeto_tarefa_arquivos 
            LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto_tarefa_arquivos.codigo_usuario)        
       ORDER BY projeto_tarefa_arquivos.codigo_tarefa, projeto_tarefa_arquivos.codigo_arquivo;