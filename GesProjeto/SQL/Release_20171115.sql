/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 15/11/2017
 */
-- Para cada insert, update ou delete na tabela de "projetos" ou de "tarefas de projetos".
-- Após cada ação o gatilho será disparado. 
CREATE OR REPLACE FUNCTION iudProjeto_ProjetoTarefa()
RETURNS TRIGGER AS $iudProjeto_ProjetoTarefa$
    DECLARE
        sequencia INT;
    BEGIN
        SELECT (COUNT(alteracoes_prazos.codigo_alteracao) + 1) INTO sequencia
          FROM alteracoes_prazos 
         WHERE alteracoes_prazos.codigo_projeto = NEW.cod_projeto
           AND alteracoes_prazos.codigo_tarefa = NEW.id_tarefa;
		
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO alteracoes_prazos(codigo_alteracao, codigo_projeto, codigo_tarefa, data_alteracao, data_ini_prev_old, data_ini_prev_new, 
                                          data_fin_prev_old, data_fin_prev_new, data_ini_real_old, data_ini_real_new, data_fin_real_old, data_fin_real_new) 
                VALUES (sequencia, NEW.cod_projeto, NEW.id_tarefa, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NEW.data_previsto_inicial, CURRENT_TIMESTAMP, 
                        NEW.data_previsto_final, NULL, NEW.data_real_inicial, NULL, NEW.data_real_final);
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO alteracoes_prazos(codigo_alteracao, codigo_projeto, codigo_tarefa, data_alteracao, data_ini_prev_old, data_ini_prev_new, 
                                          data_fin_prev_old, data_fin_prev_new, data_ini_real_old, data_ini_real_new, data_fin_real_old, data_fin_real_new) 
                VALUES (sequencia, NEW.cod_projeto, NEW.id_tarefa, CURRENT_TIMESTAMP, OLD.data_previsto_inicial, NEW.data_previsto_inicial, OLD.data_previsto_final, 
                        NEW.data_previsto_final, OLD.data_real_inicial, NEW.data_real_inicial, OLD.data_real_final, NEW.data_real_final);
        ELSIF (TG_OP = 'DELETE') THEN
            INSERT INTO alteracoes_prazos(codigo_alteracao, codigo_projeto, codigo_tarefa, data_alteracao, data_ini_prev_old, data_ini_prev_new, 
                                          data_fin_prev_old, data_fin_prev_new, data_ini_real_old, data_ini_real_new, data_fin_real_old, data_fin_real_new) 
                VALUES (sequencia, OLD.cod_projeto, OLD.id_tarefa, CURRENT_TIMESTAMP, OLD.data_previsto_inicial, CURRENT_TIMESTAMP, OLD.data_previsto_final, 
                        CURRENT_TIMESTAMP, OLD.data_real_inicial, NULL, OLD.data_real_final, NULL);
        END IF;

        RETURN NEW;
    END;
$iudProjeto_ProjetoTarefa$ LANGUAGE plpgsql;
