/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 03/11/2017
 */
-- Relatorio - Tarefas
CREATE OR REPLACE VIEW view_tarefas(
        projeto_tarefa_id_tarefa, projeto_tarefa_titulo, projeto_tarefa_progresso, projeto_tarefa_resolvido, projeto_tarefa_descricao, projeto_tarefa_data_inclusao, projeto_tarefa_data_previsto_inicial, projeto_tarefa_data_previsto_final, projeto_tarefa_data_real_inicial, projeto_tarefa_data_real_final,
        projeto_tarefa_cod_projeto,
        projeto_tarefa_fase_atual, projeto_fases1_descricao, projeto_fases1_ordem_fase,
        projeto_tarefa_responsavel, usuario1_nome, usuario1_email, usuario1_nivel_usuario)
    AS SELECT projeto_tarefa.id_tarefa, projeto_tarefa.titulo, projeto_tarefa.progresso, projeto_tarefa.resolvido, projeto_tarefa.descricao, projeto_tarefa.data_inclusao, projeto_tarefa.data_previsto_inicial, projeto_tarefa.data_previsto_final, projeto_tarefa.data_real_inicial, projeto_tarefa.data_real_final,
        projeto_tarefa.cod_projeto,
        projeto_tarefa.fase_atual, projeto_fases1.descricao, projeto_fases1.ordem_fase,
        projeto_tarefa.responsavel, usuario1.nome, usuario1.email, usuario1.nivel_usuario
    FROM projeto_tarefa 
        LEFT OUTER JOIN projeto_fases AS projeto_fases1 ON (projeto_fases1.id_fase_projeto = projeto_tarefa.fase_atual AND projeto_tarefa.cod_projeto = projeto_fases1.codigo_projeto)
            LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto_tarefa.responsavel)        
    ORDER BY projeto_tarefa.cod_projeto, projeto_tarefa.fase_atual, projeto_tarefa.id_tarefa;