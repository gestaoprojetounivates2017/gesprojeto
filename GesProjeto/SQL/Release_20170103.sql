/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 03/10/2017
 */

-- Criação de procedimento para gerar alertas a respeito de tarefas que irão iniciar em X dias
-- Criação de procedimento para gerar alertas a respeito de tarefas que ja iniciaram
-- Criação de procedimento para gerar alertas a respeito de tarefas que ainda não iniciaram até o dia atual
-- Criação de procedimento para gerar alertas a respeito de tarefas atrasadas
-- Criação de procedimento para gerar alertas a respeito de tarefas adiantadas


-- Criação de procedimento para gerar alertas a respeito de tarefas que irão iniciar em X dias
--	Esta rotina deve ser executada caso: a tarefa iniciar em x dias informado por parâmetro.
--	Exemplo de utilização: SELECT verificar_inicio_tarefa(em_dias int);
CREATE OR REPLACE FUNCTION verificar_inicio_tarefa(em_dias int)
RETURNS TABLE (id_tarefa INT, descricao_resumida VARCHAR, cod_usuario INT, nome VARCHAR) AS $inicio_tarefa$
        BEGIN	
                -- Obtém as tarefas que irão iniciar em N dias e altera o status para ALERTA
                RETURN QUERY SELECT t.id_tarefa, t.descricao_resumida, tu.cod_usuario, u.nome 
                                           FROM (tarefa t 
                                                  INNER JOIN tarefa_usuario tu ON (t.id_tarefa = tu.cod_tarefa))
                                                        INNER JOIN usuario u ON (u.id_usuario = tu.cod_usuario)
                                      WHERE cast(t.data_inicial_prev AS DATE) IN (SELECT cast(CURRENT_TIMESTAMP AS DATE) + em_dias);

                -- Alterar o status do projeto, pois esta ação implica em possível alteraçào no praso de entrega

                RETURN;
        END;
$inicio_tarefa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION verificar_inicio_tarefa(); 
--


-- Criação de procedimento para gerar alertas a respeito de tarefas que ja iniciaram
--	Esta rotina deve ser executada caso: a tarefa ja tiver sido iniciada.
--	Exemplo de utilização: SELECT verificar_inicio_tarefa();
CREATE OR REPLACE FUNCTION verificar_inicio_tarefa()
RETURNS TABLE (id_tarefa INT, descricao_resumida VARCHAR, cod_usuario INT, nome VARCHAR) AS $inicio_tarefa$
        BEGIN	
                -- Obtém as tarefas que iniciarão antes da data atual
                RETURN QUERY SELECT t.id_tarefa, t.descricao_resumida, tu.cod_usuario, u.nome 
                                           FROM (tarefa t 
                                                  INNER JOIN tarefa_usuario tu ON (t.id_tarefa = tu.cod_tarefa))
                                                        INNER JOIN usuario u ON (u.id_usuario = tu.cod_usuario)
                                      WHERE t.data_inicial_real > CURRENT_TIMESTAMP;
                RETURN;
        END;
$inicio_tarefa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION verificar_inicio_tarefa(); 
--


-- Criação de procedimento para gerar alertas a respeito de tarefas que ainda não iniciaram até o dia atual
--	Esta rotina deve ser executada caso: a tarefa deveria iniciar em x dias informado por parâmetro.
--	Exemplo de utilização: SELECT verificar_atraso_inicio_tarefa(em_dias int);
CREATE OR REPLACE FUNCTION verificar_atraso_inicio_tarefa(em_dias int)
RETURNS TABLE (id_tarefa INT, descricao_resumida VARCHAR, cod_usuario INT, nome VARCHAR) AS $inicio_tarefa$
        BEGIN	
                -- Obtém as tarefas que irão iniciar em N dias e altera o status para ALERTA
                RETURN QUERY SELECT t.id_tarefa, t.descricao_resumida, tu.cod_usuario, u.nome 
                                           FROM (tarefa t 
                                                  INNER JOIN tarefa_usuario tu ON (t.id_tarefa = tu.cod_tarefa))
                                                        INNER JOIN usuario u ON (u.id_usuario = tu.cod_usuario)
                                      WHERE cast(t.data_inicial_prev AS DATE) IN (SELECT cast(CURRENT_TIMESTAMP AS DATE) - em_dias);
                RETURN;
        END;
$inicio_tarefa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION verificar_atraso_inicio_tarefa(em_dias int); 
--


-- Criação de procedimento para gerar alertas a respeito de tarefas atrasadas
--	Esta rotina deve ser executada caso: a tarefa estiver atrasada.
--	Exemplo de utilização: SELECT verificar_atraso_tarefa();
CREATE OR REPLACE FUNCTION verificar_atraso_tarefa()
RETURNS TABLE (id_tarefa INT, descricao_resumida VARCHAR, cod_usuario INT, nome VARCHAR) AS $inicio_tarefa$
        BEGIN	
                -- Obtém as tarefas que estão atrasadas e altera o status para ALERTA
                RETURN QUERY SELECT t.id_tarefa, t.descricao_resumida, tu.cod_usuario, u.nome 
                                           FROM (tarefa t 
                                                  INNER JOIN tarefa_usuario tu ON (t.id_tarefa = tu.cod_tarefa))
                                                        INNER JOIN usuario u ON (u.id_usuario = tu.cod_usuario)
                                      WHERE t.data_final_real > CURRENT_TIMESTAMP;
                RETURN;
        END;
$inicio_tarefa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION verificar_atraso_tarefa(); 
--


-- Criação de procedimento para gerar alertas a respeito de tarefas adiantadas
--	Esta rotina deve ser executada caso: a tarefa estiver atrasada.
--	Exemplo de utilização: SELECT verificar_adiantamento_tarefa();
CREATE OR REPLACE FUNCTION verificar_adiantamento_tarefa()
RETURNS TABLE (id_tarefa INT, descricao_resumida VARCHAR, cod_usuario INT, nome VARCHAR) AS $inicio_tarefa$
        BEGIN	
                -- Obtém as tarefas que estão adiantadas e altera o status para ALERTA
                RETURN QUERY SELECT t.id_tarefa, t.descricao_resumida, tu.cod_usuario, u.nome 
                                           FROM (tarefa t 
                                                  INNER JOIN tarefa_usuario tu ON (t.id_tarefa = tu.cod_tarefa))
                                                        INNER JOIN usuario u ON (u.id_usuario = tu.cod_usuario)
                                      WHERE t.data_final_real < CURRENT_TIMESTAMP;
                RETURN;
        END;
$inicio_tarefa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION verificar_adiantamento_tarefa(); 
--
