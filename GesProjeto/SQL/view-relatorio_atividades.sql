/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 04/11/2017
 */
-- Relatorio - Atividades
CREATE OR REPLACE VIEW view_atividades(
            projeto_tarefa_atividades_codigo_atividade, projeto_tarefa_atividades_codigo_tarefa, 
            projeto_tarefa_atividades_codigo_usuario, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
            projeto_tarefa_atividades_tempo_atividade, projeto_tarefa_atividades_descricao_atividade)
    AS SELECT projeto_tarefa_atividades.codigo_atividade, projeto_tarefa_atividades.codigo_tarefa, 
            projeto_tarefa_atividades.codigo_usuario, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
            projeto_tarefa_atividades.tempo_atividade, projeto_tarefa_atividades.descricao_atividade      
        FROM projeto_tarefa_atividades 
            LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto_tarefa_atividades.codigo_usuario)        
       ORDER BY projeto_tarefa_atividades.codigo_tarefa, projeto_tarefa_atividades.codigo_atividade;