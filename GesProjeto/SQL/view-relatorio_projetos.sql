/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 02/11/2017
 */
-- Relatorio - Projeto
CREATE OR REPLACE VIEW view_projetos(
        projeto_id_projeto, projeto_descricao, projeto_data_inclusao, projeto_observacoes, projeto_descricao_resumida, projeto_custo_previsto, projeto_concluido,
        projeto_cod_usuario_chefe, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
        projeto_cod_setor, empresa_setores1_descricao_resumida, 
            empresa_setores1_cod_usuario_chefe, usuario2_nome, usuario2_email, usuario2_nivel_usuario,
            empresa_setores1_codigo_empresa, empresa1_nome_empresa, empresa1_cnpj, 
                empresa1_usuario_administrador, usuario3_nome, usuario3_email, usuario3_nivel_usuario,
                empresa1_codigo_cidade, cidade1_descricao_resumida, 
                    cidade1_cod_estado, estado1_sgl_estado, estado1_descricao, 
                        estado1_codigo_pais, pais1_sigla, pais1_descricao_resumida,
        projeto_cod_categoria_projeto, categoria_projeto1_descricao, categoria_projeto1_observacoes, 
            categoria_projeto1_codigo_empresa,empresa2_nome_empresa, empresa2_cnpj, 
                empresa2_usuario_administrador, usuario4_nome, usuario4_email, usuario4_nivel_usuario,
                empresa2_codigo_cidade, cidade2_descricao_resumida, 
                    cidade2_cod_estado, estado2_sgl_estado, estado2_descricao,
                        estado2_codigo_pais, pais2_sigla, pais2_descricao_resumida,
        projeto_cod_status_projeto, projeto_status1_descricao, projeto_status1_codigo_projeto, projeto_status1_percentual_concluido)
    AS SELECT
        projeto.id_projeto, projeto.descricao, projeto.data_inclusao, projeto.observacoes, projeto.descricao_resumida, projeto.custo_previsto, projeto.concluido,
        projeto.cod_usuario_chefe, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
        projeto.cod_setor, empresa_setores1.descricao_resumida, 
            empresa_setores1.cod_usuario_chefe, usuario2.nome, usuario2.email, usuario2.nivel_usuario,
            empresa_setores1.codigo_empresa, empresa1.nome_empresa, empresa1.cnpj, 
                empresa1.usuario_administrador, usuario3.nome, usuario3.email, usuario3.nivel_usuario,
                empresa1.codigo_cidade, cidade1.descricao_resumida, 
                    cidade1.cod_estado, estado1.sgl_estado, estado1.descricao, 
                        estado1.codigo_pais, pais1.sigla, pais1.descricao_resumida,
        projeto.cod_categoria_projeto, categoria_projeto1.descricao, categoria_projeto1.observacoes, 
            categoria_projeto1.codigo_empresa,empresa2.nome_empresa, empresa2.cnpj, 
                empresa2.usuario_administrador, usuario4.nome, usuario4.email, usuario4.nivel_usuario,
                empresa2.codigo_cidade, cidade2.descricao_resumida, 
                    cidade2.cod_estado, estado2.sgl_estado, estado2.descricao,
                        estado2.codigo_pais, pais2.sigla, pais2.descricao_resumida,
        projeto.cod_status_projeto, projeto_status1.descricao, projeto_status1.codigo_projeto, projeto_status1.percentual_concluido
     FROM projeto 
        LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto.cod_usuario_chefe)
        LEFT OUTER JOIN empresa_setores AS empresa_setores1 ON(empresa_setores1.id_setor = projeto.cod_setor) 
            LEFT OUTER JOIN usuario AS usuario2 ON(usuario2.id_usuario = empresa_setores1.cod_usuario_chefe) 
            LEFT OUTER JOIN empresa AS empresa1 ON(empresa1.cod_empresa = empresa_setores1.codigo_empresa)
                LEFT OUTER JOIN usuario AS usuario3 ON(usuario3.id_usuario = empresa1.usuario_administrador) 
                LEFT OUTER JOIN cidade AS cidade1 ON(cidade1.id_cidade = empresa1.codigo_cidade) 
                    LEFT OUTER JOIN estado AS estado1 ON(estado1.codigo_estado = cidade1.cod_estado)
                        LEFT OUTER JOIN pais AS pais1 ON(pais1.id_pais = estado1.codigo_pais) 
        LEFT OUTER JOIN categoria_projeto AS categoria_projeto1 ON(categoria_projeto1.id_categoria_projeto = projeto.cod_categoria_projeto)
            LEFT OUTER JOIN empresa AS empresa2 ON(empresa2.cod_empresa = categoria_projeto1.codigo_empresa)
                LEFT OUTER JOIN usuario AS usuario4 ON(usuario4.id_usuario = empresa2.usuario_administrador) 
                LEFT OUTER JOIN cidade AS cidade2 ON(cidade2.id_cidade = empresa2.codigo_cidade) 
                    LEFT OUTER JOIN estado AS estado2 ON(estado2.codigo_estado = cidade2.cod_estado)
                        LEFT OUTER JOIN pais AS pais2 ON(pais2.id_pais = estado2.codigo_pais) 
        LEFT OUTER JOIN projeto_status AS projeto_status1 ON(projeto_status1.id_status_projeto = projeto.cod_status_projeto)
    ORDER BY projeto.id_projeto;