/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 24/10/2017
 */
-- Relatorio - Termo de abertura de Projeto
CREATE OR REPLACE VIEW  view_termo_abertura_projeto(
    cod_projeto, titulo, orcamento, cadastro, descricao, observacoes, gerente, 
    empresa, setor, categoria, escopo, prev_ini, prev_fin) 
    AS SELECT 
            projeto.id_projeto AS cod_projeto,
            projeto.descricao_resumida AS titulo, 
            projeto.custo_previsto AS orcamento, 
            projeto.data_inclusao AS cadastro,
            projeto.descricao AS descricao,
            projeto.observacoes AS observacoes, 
            usuario.nome AS gerente,
            empresa.nome_empresa AS empresa,
            empresa_setores.descricao_resumida AS setor,
            categoria_projeto.descricao AS categoria,
            pt.descricao AS escopo,
            pt.data_previsto_inicial AS prev_ini,
            pt1.data_previsto_final AS prev_fin
        FROM
            (((((projeto INNER JOIN usuario ON(usuario.id_usuario = projeto.cod_usuario_chefe)) 
                INNER JOIN empresa_setores ON (empresa_setores.id_setor = projeto.cod_setor))
                    INNER JOIN empresa ON(empresa.cod_empresa = empresa_setores.codigo_empresa))
                        INNER JOIN categoria_projeto ON(categoria_projeto.id_categoria_projeto = projeto.cod_categoria_projeto))
                            INNER JOIN projeto_tarefa pt ON(projeto.id_projeto = pt.cod_projeto 
                                    AND pt.fase_atual IN (
                                        SELECT min(ordem_fase) FROM projeto_fases WHERE projeto_fases.codigo_projeto = projeto.id_projeto)))
                                INNER JOIN projeto_tarefa pt1 ON(projeto.id_projeto = pt1.cod_projeto 
                                        AND pt1.fase_atual IN (
                                            SELECT max(ordem_fase) FROM projeto_fases WHERE projeto_fases.codigo_projeto = projeto.id_projeto))
       ORDER BY projeto.id_projeto;