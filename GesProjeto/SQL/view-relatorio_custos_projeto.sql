/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 28/10/2017
 */
-- Relatorio - Custo do Projeto
CREATE OR REPLACE VIEW  view_custos(
    data_lancamento, mes, ano, descricao, valor_lancamento, cod_projeto, id_tarefa, 
    nome_tarefa, responsavel, projeto) 
    AS SELECT 
            projeto_tarefa_custos.data_lancamento,
            concat('', date_part('month'::text, projeto_tarefa_custos.data_lancamento)) AS mes,
            concat('', date_part('year'::text, projeto_tarefa_custos.data_lancamento)) AS ano,
            concat('', projeto_tarefa_custos.descricao) AS descricao,
            projeto_tarefa_custos.valor_lancamento,
            projeto_tarefa.cod_projeto,
            projeto_tarefa.id_tarefa,
            concat('', projeto_tarefa.titulo) AS nome_tarefa,
            usuario.nome AS responsavel,
            projeto.descricao_resumida AS projeto
        FROM ((projeto_tarefa_custos
            INNER JOIN projeto_tarefa ON (projeto_tarefa.id_tarefa = projeto_tarefa_custos.codigo_tarefa))
                INNER JOIN usuario ON (usuario.id_usuario = projeto_tarefa.responsavel))
                    INNER JOIN projeto ON (projeto.id_projeto = projeto_tarefa.cod_projeto)
       ORDER BY projeto_tarefa.cod_projeto;