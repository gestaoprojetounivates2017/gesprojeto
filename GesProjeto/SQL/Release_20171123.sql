/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 23/11/2017
 */
-- Criação de função para inserir membros aos projetos ao criar o projeto
-- Criação de gatilho para projeto_tarefa
-- Correção de função iudProjeto_ProjetoTarefa


-- Para cada insert, update ou delete na tabela de "membros_projeto".
-- Este procedimento inclui um registro na tabela de membros_projeto com os registros de cada usuário incluído no projeto.
-- Insere o usuário com o nível 2
-- Após cada ação o gatilho será disparado. 
CREATE OR REPLACE FUNCTION iudMembrosProjeto_Tarefa()
RETURNS TRIGGER AS $iudMembrosProjeto_Tarefa$
    DECLARE
        codigo INT;
    BEGIN	
        IF (TG_OP = 'INSERT') THEN
            SELECT cod_usuario INTO codigo
              FROM projeto_membros 
             WHERE projeto_membros.cod_projeto = NEW.cod_projeto
               AND projeto_membros.cod_usuario = NEW.responsavel;

            IF codigo = 0 THEN
                INSERT INTO projeto_membros(cod_projeto, cod_usuario, nivel) 
                    VALUES (NEW.cod_projeto, NEW.responsavel, 2);
            END IF;

            RETURN NEW;
        ELSIF (TG_OP = 'UPDATE') THEN
            UPDATE projeto_membros SET cod_projeto = NEW.cod_projeto WHERE cod_projeto = OLD.cod_projeto AND cod_usuario = OLD.responsavel;
            UPDATE projeto_membros SET cod_usuario = NEW.responsavel WHERE cod_projeto = OLD.cod_projeto AND cod_usuario = OLD.responsavel;
            UPDATE projeto_membros SET nivel = 2 WHERE cod_projeto = OLD.cod_projeto AND cod_usuario = OLD.responsavel;

            RETURN NEW;
        ELSIF (TG_OP = 'DELETE') THEN
            DELETE FROM projeto_membros WHERE cod_projeto = OLD.cod_projeto AND cod_usuario = OLD.responsavel;
             
            RETURN OLD;
        END IF;
    END;
$iudMembrosProjeto_Tarefa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION iudMembrosProjeto_Tarefa(); 
--

-- Criação de gatilho para procedimento de inclusão de usuário, projeto e nível projeto_tarefa apos incluir uma tarefa
CREATE TRIGGER t_iudMembrosProjeto_Tarefa AFTER INSERT OR UPDATE OR DELETE ON projeto_tarefa FOR EACH ROW EXECUTE PROCEDURE iudMembrosProjeto_Tarefa();
--
-- DROP gatilho: DROP TRIGGER t_iudMembrosProjeto_Tarefa ON projeto_tarefa; 
--



-- Correção de função iudProjeto_ProjetoTarefa
CREATE OR REPLACE FUNCTION iudProjeto_ProjetoTarefa()
RETURNS TRIGGER AS $iudProjeto_ProjetoTarefa$
    DECLARE
        sequencia INT;
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            SELECT (COUNT(alteracoes_prazos.codigo_alteracao) + 1) INTO sequencia
              FROM alteracoes_prazos 
             WHERE alteracoes_prazos.codigo_projeto = NEW.cod_projeto
               AND alteracoes_prazos.codigo_tarefa = NEW.id_tarefa;

            INSERT INTO alteracoes_prazos(codigo_alteracao, codigo_projeto, codigo_tarefa, data_alteracao, data_ini_prev_old, data_ini_prev_new, 
                                          data_fin_prev_old, data_fin_prev_new, data_ini_real_old, data_ini_real_new, data_fin_real_old, data_fin_real_new) 
                VALUES (sequencia, NEW.cod_projeto, NEW.id_tarefa, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NEW.data_previsto_inicial, CURRENT_TIMESTAMP, 
                        NEW.data_previsto_final, NULL, NEW.data_real_inicial, NULL, NEW.data_real_final);
            RETURN NEW;
        ELSIF (TG_OP = 'UPDATE') THEN
            SELECT (COUNT(alteracoes_prazos.codigo_alteracao) + 1) INTO sequencia
              FROM alteracoes_prazos 
             WHERE alteracoes_prazos.codigo_projeto = NEW.cod_projeto
               AND alteracoes_prazos.codigo_tarefa = NEW.id_tarefa;

            INSERT INTO alteracoes_prazos(codigo_alteracao, codigo_projeto, codigo_tarefa, data_alteracao, data_ini_prev_old, data_ini_prev_new, 
                                          data_fin_prev_old, data_fin_prev_new, data_ini_real_old, data_ini_real_new, data_fin_real_old, data_fin_real_new) 
                VALUES (sequencia, NEW.cod_projeto, NEW.id_tarefa, CURRENT_TIMESTAMP, OLD.data_previsto_inicial, NEW.data_previsto_inicial, OLD.data_previsto_final, 
                        NEW.data_previsto_final, OLD.data_real_inicial, NEW.data_real_inicial, OLD.data_real_final, NEW.data_real_final);
            RETURN NEW;
        ELSIF (TG_OP = 'DELETE') THEN
            SELECT (COUNT(alteracoes_prazos.codigo_alteracao) + 1) INTO sequencia
              FROM alteracoes_prazos 
             WHERE alteracoes_prazos.codigo_projeto = OLD.cod_projeto
               AND alteracoes_prazos.codigo_tarefa = OLD.id_tarefa;

            INSERT INTO alteracoes_prazos(codigo_alteracao, codigo_projeto, codigo_tarefa, data_alteracao, data_ini_prev_old, data_ini_prev_new, 
                                          data_fin_prev_old, data_fin_prev_new, data_ini_real_old, data_ini_real_new, data_fin_real_old, data_fin_real_new) 
                VALUES (sequencia, OLD.cod_projeto, OLD.id_tarefa, CURRENT_TIMESTAMP, OLD.data_previsto_inicial, CURRENT_TIMESTAMP, OLD.data_previsto_final, 
                        CURRENT_TIMESTAMP, OLD.data_real_inicial, NULL, OLD.data_real_final, NULL);
            RETURN OLD;
        END IF;
    END;
$iudProjeto_ProjetoTarefa$ LANGUAGE plpgsql;