/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 04/11/2017
 */
-- Relatorio - Membros
CREATE OR REPLACE VIEW view_membros(
            projeto_membros_id_membro_projeto, projeto_membros_cod_projeto, 
            projeto_membros_cod_usuario, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
            projeto_membros_nivel)
    AS SELECT projeto_membros.id_membro_projeto, projeto_membros.cod_projeto, 
            projeto_membros.cod_usuario, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
            projeto_membros.nivel
         FROM projeto_membros 
            LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto_membros.cod_usuario)        
        ORDER BY projeto_membros.cod_projeto;