/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 08/11/2017
 */
-- DROP "view_projetos".
DROP VIEW view_projetos;

-- Alteração em "categoria_projeto".
-- Removida a FK de categoria_projeto, assim esta tabela possiurá dados genéricos e não exclusivos de uma empresa.
ALTER TABLE categoria_projeto DROP CONSTRAINT fk_categoria_projeto_empresa;
ALTER TABLE categoria_projeto DROP COLUMN codigo_empresa;
DELETE FROM estruturador WHERE tabela = 'categoria_projeto' AND coluna = 'codigo_empresa';
	
-- Alteração em "estado".
-- Campos alterados para obrigatórios para:
-- 1 - forçar a inclusão de um estado
-- 2 - não deixar um estado sem nome.
ALTER TABLE estado ALTER COLUMN sgl_estado SET NOT NULL;
ALTER TABLE estado ALTER COLUMN descricao SET NOT NULL;
UPDATE estruturador SET obrigatorio = 'TRUE' WHERE tabela = 'estado' AND coluna = 'sgl_estado';
UPDATE estruturador SET obrigatorio = 'TRUE' WHERE tabela = 'estado' AND coluna = 'descricao';

-- Alteração em "empresa_usuarios".
-- Criado campo "funcao" para o usuário dentro da empresa.
-- Alteração de chave primária para chave composta, pois não podemos ter o mesmo usuário com funções diferentes para uma mesma empresa.
ALTER TABLE empresa_usuarios ADD COLUMN funcao character varying(100) NOT NULL;
ALTER TABLE empresa_usuarios ADD CONSTRAINT pk_empresa_usuarios PRIMARY KEY (cod_empresa, cod_usuario);
INSERT INTO estruturador (tabela, coluna, rotulo, obrigatorio, ordenacao, tabela_dependencia, coluna_dependencia, exibicao_dependencia, exibir_cadastro, e_filtro, tipo_campo, tipo_valor, where_dependencia) 
    VALUES ('empresa_usuarios', 'funcao', 'Função', 'TRUE', 3, null, null, null, 'TRUE', 'FALSE', 1, 'text', null);

-- DROP "view_projetos".
DROP VIEW view_membros;
	
-- Alteração em "projeto_membros".
-- Campo removido pois um membro pode aparecer somente uma vez no projeto, senão poderá ter vários níveis de acesso.
ALTER TABLE projeto_membros DROP CONSTRAINT pkprojeto_membros;
ALTER TABLE projeto_membros DROP COLUMN id_membro_projeto;
DELETE FROM estruturador WHERE tabela = 'projeto_membros' AND coluna = 'id_membro_projeto';		
ALTER TABLE projeto_membros ADD CONSTRAINT pkprojeto_membros PRIMARY KEY (cod_projeto, cod_usuario);
UPDATE estruturador SET ordenacao = 1 WHERE tabela = 'projeto_membros' AND ordenacao = 2;
UPDATE estruturador SET ordenacao = 2 WHERE tabela = 'projeto_membros' AND ordenacao = 3;
UPDATE estruturador SET ordenacao = 3 WHERE tabela = 'projeto_membros' AND ordenacao = 4;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 08/11/2017
 */
-- Relatorio - Membros
CREATE OR REPLACE VIEW view_membros(
            projeto_membros_cod_projeto, 
            projeto_membros_cod_usuario, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
            projeto_membros_nivel)
    AS SELECT projeto_membros.cod_projeto, 
            projeto_membros.cod_usuario, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
            projeto_membros.nivel
         FROM projeto_membros 
            LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto_membros.cod_usuario)        
        ORDER BY projeto_membros.cod_projeto;

-- Alteração em "projeto_status".
-- Removida a FK de "projeto_status", assim esta tabela possiura dados genéricos e não exclusivos de um projeto.
ALTER TABLE projeto_status DROP CONSTRAINT fk_projeto_status_projeto;
ALTER TABLE projeto_status DROP COLUMN codigo_projeto;
DELETE FROM estruturador WHERE tabela = 'projeto_status' AND coluna = 'codigo_projeto';		
UPDATE estruturador SET ordenacao = 3 WHERE tabela = 'projeto_status' AND ordenacao = 4;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 08/11/2017
 */
-- Relatorio - Projeto
CREATE OR REPLACE VIEW view_projetos(
        projeto_id_projeto, projeto_descricao, projeto_data_inclusao, projeto_observacoes, projeto_descricao_resumida, projeto_custo_previsto, projeto_concluido,
        projeto_cod_usuario_chefe, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
        projeto_cod_setor, empresa_setores1_descricao_resumida, 
            empresa_setores1_cod_usuario_chefe, usuario2_nome, usuario2_email, usuario2_nivel_usuario,
            empresa_setores1_codigo_empresa, empresa1_nome_empresa, empresa1_cnpj, 
                empresa1_usuario_administrador, usuario3_nome, usuario3_email, usuario3_nivel_usuario,
                empresa1_codigo_cidade, cidade1_descricao_resumida, 
                    cidade1_cod_estado, estado1_sgl_estado, estado1_descricao, 
                        estado1_codigo_pais, pais1_sigla, pais1_descricao_resumida,
        projeto_cod_categoria_projeto, categoria_projeto1_descricao, categoria_projeto1_observacoes,
        projeto_cod_status_projeto, projeto_status1_descricao, projeto_status1_percentual_concluido)
    AS SELECT
        projeto.id_projeto, projeto.descricao, projeto.data_inclusao, projeto.observacoes, projeto.descricao_resumida, projeto.custo_previsto, projeto.concluido,
        projeto.cod_usuario_chefe, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
        projeto.cod_setor, empresa_setores1.descricao_resumida, 
            empresa_setores1.cod_usuario_chefe, usuario2.nome, usuario2.email, usuario2.nivel_usuario,
            empresa_setores1.codigo_empresa, empresa1.nome_empresa, empresa1.cnpj, 
                empresa1.usuario_administrador, usuario3.nome, usuario3.email, usuario3.nivel_usuario,
                empresa1.codigo_cidade, cidade1.descricao_resumida, 
                    cidade1.cod_estado, estado1.sgl_estado, estado1.descricao, 
                        estado1.codigo_pais, pais1.sigla, pais1.descricao_resumida,
        projeto.cod_categoria_projeto, categoria_projeto1.descricao, categoria_projeto1.observacoes, 
        projeto.cod_status_projeto, projeto_status1.descricao, projeto_status1.percentual_concluido
     FROM projeto 
        LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto.cod_usuario_chefe)
        LEFT OUTER JOIN empresa_setores AS empresa_setores1 ON(empresa_setores1.id_setor = projeto.cod_setor) 
            LEFT OUTER JOIN usuario AS usuario2 ON(usuario2.id_usuario = empresa_setores1.cod_usuario_chefe) 
            LEFT OUTER JOIN empresa AS empresa1 ON(empresa1.cod_empresa = empresa_setores1.codigo_empresa)
                LEFT OUTER JOIN usuario AS usuario3 ON(usuario3.id_usuario = empresa1.usuario_administrador) 
                LEFT OUTER JOIN cidade AS cidade1 ON(cidade1.id_cidade = empresa1.codigo_cidade) 
                    LEFT OUTER JOIN estado AS estado1 ON(estado1.codigo_estado = cidade1.cod_estado)
                        LEFT OUTER JOIN pais AS pais1 ON(pais1.id_pais = estado1.codigo_pais) 
        LEFT OUTER JOIN categoria_projeto AS categoria_projeto1 ON(categoria_projeto1.id_categoria_projeto = projeto.cod_categoria_projeto)
        LEFT OUTER JOIN projeto_status AS projeto_status1 ON(projeto_status1.id_status_projeto = projeto.cod_status_projeto)
    ORDER BY projeto.id_projeto;

-- Alteração em "projeto_tarefa".
-- Alteradas as datas "data_previsto_inicial" e "data_previsto_final" para "not null", no momento do cadastro estas podem ser informadas, já que se tratam de previsões e por default serão now(),
-- Alteradas as datas "data_real_inicial" e "data_real_final" para "null", pode ser que no momento da inclusão não se saiba intormar o valor destes campos.
ALTER TABLE projeto_tarefa ALTER COLUMN data_previsto_inicial SET NOT NULL;
ALTER TABLE projeto_tarefa ALTER COLUMN data_previsto_final SET NOT NULL;
ALTER TABLE projeto_tarefa ALTER COLUMN data_real_inicial DROP NOT NULL;
ALTER TABLE projeto_tarefa ALTER COLUMN data_real_final DROP NOT NULL;
UPDATE estruturador SET obrigatorio = 'FALSE' WHERE tabela = 'projeto_tarefa' AND coluna = 'data_previsto_inicial';
UPDATE estruturador SET obrigatorio = 'FALSE' WHERE tabela = 'projeto_tarefa' AND coluna = 'data_previsto_final'; 
UPDATE estruturador SET obrigatorio = 'TRUE' WHERE tabela = 'projeto_tarefa' AND coluna = 'data_real_inicial';
UPDATE estruturador SET obrigatorio = 'TRUE' WHERE tabela = 'projeto_tarefa' AND coluna = 'data_real_final';

-- DROP "view_arquivos".
DROP VIEW view_arquivos;

-- Alteração em "projeto_tarefa_arquivos".
-- Removida a FK de codigo_projeto, pois está redundante.
ALTER TABLE projeto_tarefa_arquivos DROP CONSTRAINT fk_projeto_tarefa_arquivos_projeto;
ALTER TABLE projeto_tarefa_arquivos DROP COLUMN codigo_projeto;
DELETE FROM estruturador WHERE tabela = 'projeto_tarefa_arquivos' AND coluna = 'codigo_projeto';  

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergioa
 * Created: 08/11/2017
 */
-- Relatorio - Arquivos
CREATE OR REPLACE VIEW view_arquivos(
            projeto_tarefa_arquivos_codigo_arquivo, projeto_tarefa_arquivos_nome_arquivo, projeto_tarefa_arquivos_descricao, 
            projeto_tarefa_arquivos_codigo_usuario, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
            projeto_tarefa_arquivos_codigo_tarefa)
    AS SELECT projeto_tarefa_arquivos.codigo_arquivo, projeto_tarefa_arquivos.nome_arquivo, projeto_tarefa_arquivos.descricao, 
            projeto_tarefa_arquivos.codigo_usuario, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
            projeto_tarefa_arquivos.codigo_tarefa
        FROM projeto_tarefa_arquivos 
            LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto_tarefa_arquivos.codigo_usuario)        
       ORDER BY projeto_tarefa_arquivos.codigo_tarefa, projeto_tarefa_arquivos.codigo_arquivo;

-- Alteração em "projeto_tarefa_atividades".
-- Alterado campo para obrigatório para não deixar o atividade sem descrição.
ALTER TABLE projeto_tarefa_atividades ALTER COLUMN descricao_atividade SET NOT NULL;
UPDATE estruturador SET obrigatorio = 'TRUE' WHERE tabela = 'projeto_tarefa_atividades' AND coluna = 'descricao_atividade';   

-- Alteração em "projeto_tarefa_custos".
-- Alterado campo para obrigatório para não deixar o custo sem descrição.
ALTER TABLE projeto_tarefa_custos ALTER COLUMN descricao SET NOT NULL;
UPDATE estruturador SET obrigatorio = 'TRUE' WHERE tabela = 'projeto_tarefa_custos' AND coluna = 'descricao';   

-- Alteração em "estruturador".
-- Criada a fk do campo "tipo_valor" para a tabela "estruturador_tipos_valor".
ALTER TABLE estruturador ADD CONSTRAINT fk_estruturador_estruturador_tipos_valor FOREIGN KEY (tipo_valor) REFERENCES estruturador_tipos_valor(tipo_codigo_valor);

-- Dropada a tabela de "justificativas" que dará lugar para a tabela de "alteracoes_prazos".
DROP TABLE justificativas;

-- Criação de tabela "alteracoes_prazos" para substituir a tabela de "justificativas"
CREATE TABLE alteracoes_prazos (
    codigo_alteracao INTEGER NOT NULL,
    codigo_projeto INTEGER NOT NULL,
    codigo_tarefa INTEGER NOT NULL,
    data_alteracao TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    data_ini_prev_old TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    data_ini_prev_new TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    data_fin_prev_old TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    data_fin_prev_new TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    data_ini_real_old TIMESTAMP WITHOUT TIME ZONE,
    data_ini_real_new TIMESTAMP WITHOUT TIME ZONE,
    data_fin_real_old TIMESTAMP WITHOUT TIME ZONE,
    data_fin_real_new TIMESTAMP WITHOUT TIME ZONE,

    CONSTRAINT pk_alteracoes_prazos PRIMARY KEY(codigo_alteracao, codigo_projeto, codigo_tarefa)
);

-- Para cada insert, update ou delete na tabela de "projetos" ou de "tarefas de projetos".
-- Após cada ação o gatilho será disparado. 
CREATE OR REPLACE FUNCTION iudProjeto_ProjetoTarefa()
RETURNS TRIGGER AS $iudProjeto_ProjetoTarefa$
    DECLARE
        sequencia INT;
    BEGIN
        SELECT (CONT(alteracoes_prazos.codigo_alteracao) + 1) INTO sequencia
          FROM alteracoes_prazos 
         WHERE alteracoes_prazos.codigo_projeto = NEW.cod_projeto
           AND alteracoes_prazos.codigo_tarefa = NEW.id_tarefa
           AND alteracoes_prazos.data_alteracao = NEW.data_alteracao;
		
        INSERT INTO alteracoes_prazos(sequencia, codigo_projeto, codigo_tarefa, data_alteracao, data_ini_prev_old, data_ini_prev_new, 
                    data_fin_prev_old, data_fin_prev_new, data_ini_real_old, data_ini_real_new, data_fin_real_old, data_fin_real_new) 
            VALUES (sequencia, cod_projeto, id_tarefa, CURRENT_TIMESTAMP, OLD.data_previsto_inicial, NEW.data_previsto_inicial, OLD.data_previsto_final, 
                    NEW.data_previsto_final, OLD.data_real_inicial, NEW.data_real_inicial, OLD.data_real_final, NEW.data_real_final);
        
        RETURN NEW;
    END;
$iudProjeto_ProjetoTarefa$ LANGUAGE plpgsql;
 
--
-- DROP gatilho: DROP TRIGGER tg_iudProjeto_ProjetoTarefa ON projeto_tarefa; 
--
CREATE TRIGGER tg_iudProjeto_ProjetoTarefa AFTER INSERT OR UPDATE OR DELETE ON projeto_tarefa FOR EACH ROW EXECUTE PROCEDURE iudProjeto_ProjetoTarefa();
