/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 16/10/2017
 */
-- Alteração 20171016
-- Criação de procedimento inclusão de registro na tabela de fases do projeto
--	Esta rotina deve ser executada caso: um novo registro for incluído na tabela de projetos
--	Exemplo de utilização: SELECT atualizar_projeto_fase();
CREATE OR REPLACE FUNCTION atualizar_projeto_fase()
RETURNS TRIGGER AS $new_projeto_fase$
    DECLARE
        rx RECORD;
    BEGIN	
        FOR rx IN (SELECT cfp.descricao, cfp.ordem_fase 
                     FROM categoria_fases_padrao cfp	
                    WHERE NEW.cod_categoria_projeto = cfp.codigo_categoria
                    ORDER BY id_fase_projeto) 
        LOOP
            BEGIN
                IF (TG_OP = 'INSERT') THEN
                       INSERT INTO projeto_fases(descricao, ordem_fase, codigo_projeto) VALUES (rx.descricao, rx.ordem_fase, NEW.id_projeto);
                END IF;
            END;
        END LOOP;

        RETURN NEW;
    END;
$new_projeto_fase$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION atualizar_projeto_fase(); 
--

-- Criação de gatilho para procedimento para atualizar fases ao incluir um projeto
CREATE TRIGGER t_atualizar_projeto_fase AFTER INSERT OR UPDATE OR DELETE ON projeto FOR EACH ROW EXECUTE PROCEDURE atualizar_projeto_fase();
--
-- DROP gatilho: DROP TRIGGER t_atualizar_projeto_fase ON projeto; 
--

