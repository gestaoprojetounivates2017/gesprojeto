/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 04/11/2017
 */
-- Relatorio - Envolvidos
CREATE OR REPLACE VIEW view_envolvidos(
            projeto_tarefa_envolvidos_codigo_tarefa, 
            projeto_tarefa_envolvidos_codigo_usuario, usuario1_nome, usuario1_email, usuario1_nivel_usuario,
            projeto_tarefa_envolvidos_nivel)
    AS SELECT projeto_tarefa_envolvidos.codigo_tarefa, 
            projeto_tarefa_envolvidos.codigo_usuario, usuario1.nome, usuario1.email, usuario1.nivel_usuario,
            projeto_tarefa_envolvidos.nivel
        FROM projeto_tarefa_envolvidos 
            LEFT OUTER JOIN usuario AS usuario1 ON(usuario1.id_usuario = projeto_tarefa_envolvidos.codigo_usuario)        
       ORDER BY projeto_tarefa_envolvidos.codigo_tarefa, projeto_tarefa_envolvidos.nivel;