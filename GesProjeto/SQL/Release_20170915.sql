/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 15/09/2017
 */

-- Criação de função para excluir registro sem que seja preciso uma restrição de cascade nas tabelas do banco
-- Alteração da tabela de permissão para troca o tipo de SERIAL para INT para que seja possível entrar com valor manual
-- Alteração da tabela de permissão para retira a restrição NOT NULL para que a cada tabela que for inserida no banco ela possa fazer parte das permissões
-- Criação de permissões para tabelas do banco na tabela de "permissao"
-- Criação de procedimento de verificação de alterações de salário minimo da função
-- Criação de gatilho para procedimento de verificação de alterações de salário minimo da função
-- Criação de procedimento de verificação de alterações de salário do usuário
-- Criação de gatilho para procedimento de verificação de alterações de salário do usuário
-- Alteração da tabela projeto para retira a restrição NOT NULL para não ser necessário informar o setor, uma vez que o usuário também não precisa de setor
-- Criação de procedimento para incluir usuário chefe em membros do projeto
-- Criação de gatilho para procedimento de inclusão de usuário chefe do projeto em membros do projeto
-- Criação de procedimento para atualizar custo ao incluir/alterar/excluir usuário ao grupo de membros do projeto
-- Criação de gatilho para procedimento para atualizar custo ao incluir/alterar/excluir usuário ao grupo de membros do projeto
-- Alteração da tabela de logs de custos do projeto para incluir campo com o valor do custo


-- Criação de função para excluir registro sem que seja preciso uma restrição de cascade nas tabelas do banco
-- Função obtida em: http://stackoverflow.com/questions/129265/cascade-delete-just-once em 25/04/2017
-- Exemplo de utilização: SELECT delete_cascade('public','my_table','my_table.key');
CREATE OR REPLACE FUNCTION delete_cascade(p_schema VARCHAR, p_table VARCHAR, p_key VARCHAR, p_recursion VARCHAR[] DEFAULT NULL)
RETURNS INTEGER AS $$
        DECLARE
                rx RECORD;
                rd RECORD;
                v_sql VARCHAR;
                v_recursion_key VARCHAR;
                recnum INTEGER;
                v_primary_key VARCHAR;
                v_rows INTEGER;
        BEGIN
                recnum := 0;
                SELECT ccu.column_name INTO v_primary_key
                  FROM information_schema.table_constraints  tc
                        JOIN information_schema.constraint_column_usage AS ccu 
                          ON ccu.constraint_name = tc.constraint_name 
                                AND ccu.constraint_schema=tc.constraint_schema
                                AND tc.constraint_type='PRIMARY KEY'
                                AND tc.table_name=p_table
                                AND tc.table_schema=p_schema;

                FOR rx IN (SELECT kcu.table_name AS foreign_table_name, 
                                                  kcu.column_name AS foreign_column_name, 
                                                  kcu.table_schema foreign_table_schema,
                                                  kcu2.column_name AS foreign_table_primary_key
                                         FROM information_schema.constraint_column_usage ccu
                                                JOIN information_schema.table_constraints tc ON tc.constraint_name=ccu.constraint_name AND tc.constraint_catalog=ccu.constraint_catalog AND ccu.constraint_schema=ccu.constraint_schema 
                                                JOIN information_schema.key_column_usage kcu ON kcu.constraint_name=ccu.constraint_name AND kcu.constraint_catalog=ccu.constraint_catalog AND kcu.constraint_schema=ccu.constraint_schema
                                                JOIN information_schema.table_constraints tc2 ON tc2.table_name=kcu.table_name AND tc2.table_schema=kcu.table_schema
                                                JOIN information_schema.key_column_usage kcu2 ON kcu2.constraint_name=tc2.constraint_name AND kcu2.constraint_catalog=tc2.constraint_catalog AND kcu2.constraint_schema=tc2.constraint_schema
                                        WHERE ccu.table_name=p_table
                                          AND ccu.table_schema=p_schema
                                          AND TC.CONSTRAINT_TYPE='FOREIGN KEY'
                                          AND tc2.constraint_type='PRIMARY KEY'
                )
                LOOP
                v_sql := 'SELECT '||rx.foreign_table_primary_key||' AS key 
                                        FROM '||rx.foreign_table_schema||'.'||rx.foreign_table_name||'
                                   WHERE '||rx.foreign_column_name||'='||quote_literal(p_key)||' 
                                         FOR UPDATE';
                --raise notice '%',v_sql;
                --found a foreign key, now find the primary keys for any data that exists in any of those tables.
                        FOR rd IN EXECUTE v_sql
                        LOOP
                                v_recursion_key=rx.foreign_table_schema||'.'||rx.foreign_table_name||'.'||rx.foreign_column_name||'='||rd.key;
                                IF (v_recursion_key = ANY (p_recursion)) THEN
                                        --raise notice 'Avoiding infinite loop';
                                ELSE
                                        --raise notice 'Recursing to %,%',rx.foreign_table_name, rd.key;
                                        recnum:= recnum +delete_cascade(rx.foreign_table_schema::VARCHAR, rx.foreign_table_name::VARCHAR, rd.key::VARCHAR, p_recursion||v_recursion_key);
                                END IF;
                        END LOOP;
                END LOOP;
                BEGIN
                        --actually delete original record.
                        v_sql := 'delete from '||p_schema||'.'||p_table||' where '||v_primary_key||'='||quote_literal(p_key);
                        EXECUTE v_sql;
                        GET DIAGNOSTICS v_rows= row_count;
                        --raise notice 'Deleting %.% %=%',p_schema,p_table,v_primary_key,p_key;
                        recnum:= recnum +v_rows;
                        EXCEPTION WHEN OTHERS THEN recnum=0;
                END;

                RETURN recnum;
        END;
$$ LANGUAGE PLPGSQL;
--
-- DROP função: DROP FUNCTION delete_cascade(p_schema VARCHAR, p_table VARCHAR, p_key VARCHAR, p_recursion VARCHAR[] DEFAULT NULL);
--


-- Alteração da tabela de permissão para troca o tipo de SERIAL para INT para que seja possível entrar com valor manual
ALTER TABLE permissao ALTER COLUMN id_permissao TYPE INT;
-- Alteração da tabela de permissão para retira a restrição NOT NULL para que a cada tabela que for inserida no banco ela possa fazer parte das permissões
ALTER TABLE permissao ALTER COLUMN cod_usuario_pai DROP NOT NULL;


-- Criação de permissões para tabelas do banco na tabela de "permissao"
INSERT INTO permissao (
        SELECT (
                SELECT nr_ordem
                  FROM (SELECT * FROM pg_tables WHERE schemaname = 'public') As oldtable
                        CROSS JOIN (SELECT ARRAY(SELECT tablename FROM pg_tables WHERE schemaname = 'public') As id_join) AS oldids
                          CROSS JOIN generate_series(1, (SELECT COUNT(*) FROM pg_tables WHERE schemaname = 'public')) AS nr_ordem
                 WHERE oldids.id_join[nr_ordem] = oldtable.tablename AND oldtable.tablename = pg_tables.tablename) AS ordem, null, tablename,
                        (SELECT obj_description(c.oid, 'pg_class') FROM pg_class c LEFT JOIN pg_namespace n ON (n.oid = c.relnamespace) 
                                WHERE c.relkind = 'r' AND nspname = 'public' AND c.relname = tablename) AS comentario
          FROM pg_tables 
         WHERE schemaname = 'public' 
         ORDER BY ordem
);


-- Criação de procedimento de verificação de alterações de salário minimo da função
-- A troca de função deve respeitar o valor de salário mínimo. 
--	Neste caso altera os salários de todos os usuários que desempenham esta função e cujo salário seja inferior ao minimo estipulado.
--	Esta rotina deve ser executada caso: o usuário troque de função, ou a função tenha o valor de salário minimo alterado.
--	Exemplo de utilização: SELECT atualizar_salario_min_funcao();
CREATE OR REPLACE FUNCTION atualizar_salario_min_funcao()
RETURNS TRIGGER AS $new_salario_mim$
        DECLARE
                rx RECORD;
        BEGIN
                IF OLD.salario_min <> NEW.salario_min THEN
                        FOR rx IN SELECT cod_funcao_usuario, salario 
                                                FROM usuario
                                           WHERE cod_funcao_usuario = NEW.id_funcao_usuario
                                           ORDER BY id_usuario LOOP
                                BEGIN
                                        -- Altera os salários cujo mesmo seja inferior ao minimo estipulado
                                        UPDATE usuario SET salario = NEW.salario_min WHERE cod_funcao_usuario = rx.cod_funcao_usuario AND salario < NEW.salario_min;
                                END;
                        END LOOP;
                END IF;

                RETURN NEW;
        END;
$new_salario_mim$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION atualizar_salario_min_funcao(); 
--


-- Criação de gatilho para procedimento de verificação de alterações de salário minimo da função
CREATE TRIGGER t_atualizar_salario_min_funcao AFTER UPDATE ON funcao_usuario FOR EACH ROW EXECUTE PROCEDURE atualizar_salario_min_funcao();
--
-- DROP gatilho: DROP TRIGGER t_atualizar_salario_min_funcao ON funcao_usuario; 
--


-- Criação de procedimento de verificação de alterações de salário do usuário
-- A troca de salário implica em alteraçoes de custos em projetos ao qual o usuário esta atrelado
--	Neste caso altera os custos de todos os projetos que de alguma forma possuem o usuário atrelado.
--	Esta rotina deve ser executada caso: o usuário tenha seu salário alterado, seja incluído ou excluido do sistema.
--	Exemplo de utilização: SELECT atualizar_salario_usuario();
CREATE OR REPLACE FUNCTION atualizar_salario_usuario()
RETURNS TRIGGER AS $new_salario$
        DECLARE
                rx RECORD;
                salario_min DECIMAL(13,2);
                cont_projetos_membro INT;
        BEGIN
                SELECT fu.salario_min INTO salario_min
                  FROM funcao_usuario fu 
                 WHERE fu.id_funcao_usuario = NEW.cod_funcao_usuario;

                SELECT COUNT(*) INTO cont_projetos_membro
                  FROM membro_projeto mp 
                 WHERE mp.cod_usuario = NEW.id_usuario;

                IF (TG_OP = 'INSERT') THEN
                        -- Altera o salário do usuário caso o valor seje menor que o minimo para a função
                        IF NEW.salario < salario_min THEN
                                UPDATE usuario SET salario = salario_min WHERE id_usuario = NEW.id_usuario;
                        END IF;		
                ELSIF (TG_OP = 'UPDATE') THEN
                        -- Se o novo salário for inferior ao mínimo da função atualiza antes de proseguir 
                        IF NEW.salario < salario_min THEN
                                UPDATE usuario SET salario = salario_min WHERE id_usuario = NEW.id_usuario;
                        END IF;

                        -- Verifica se houve alteração de salário e atualiza os custos do projeto
                        IF (OLD.salario <> NEW.salario) THEN
                                FOR rx IN (SELECT p.id_projeto, p.custo_prev
                                                         FROM projeto p 
                                                                INNER JOIN membro_projeto mp ON(p.id_projeto = mp.cod_projeto AND mp.cod_usuario = NEW.id_usuario) 
                                                        ORDER BY id_projeto) LOOP
                                        BEGIN
                                                -- Altera os custos do projeto com base no aumento ou diminuição do salario do usuário
                                                -- Esta atualização leva em conta o número de projetos que o usuário está atrelado
                                                UPDATE projeto SET custo_prev = (custo_prev + ((NEW.salario - OLD.salario) / cont_projetos_membro)) WHERE id_projeto = rx.id_projeto;
                                        END;
                                END LOOP;
                        END IF;
                ELSIF (TG_OP = 'DELETE') THEN
                        FOR rx IN (SELECT p.id_projeto, p.custo_prev, mp.cod_projeto 
                                                 FROM projeto p 
                                                        INNER JOIN membro_projeto mp ON(p.id_projeto = mp.cod_projeto AND mp.cod_usuario = NEW.id_usuario) 
                                                ORDER BY id_projeto) LOOP
                                BEGIN
                                        -- Altera os custos do projeto com base no aumento ou diminuição do salario do usuário
                                        -- Esta atualização leva em conta o número de projetos que o usuário está atrelado
                                        UPDATE projeto SET custo_prev = (custo_prev - (NEW.salario / cont_projetos_membro)) WHERE id_projeto = rx.id_projeto;
                                END;
                        END LOOP;
                END IF;

                RETURN NEW;
        END;
$new_salario$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION atualizar_salario_usuario(); 
--


-- Criação de gatilho para procedimento de verificação de alterações de salário do usuário
CREATE TRIGGER t_atualizar_salario_usuario AFTER INSERT OR UPDATE OR DELETE ON usuario FOR EACH ROW EXECUTE PROCEDURE atualizar_salario_usuario();
--
-- DROP gatilho: DROP TRIGGER t_atualizar_salario_usuario ON usuario; 
--

-- Alteração da tabela de projeto para retira a restrição NOT NULL para não ser necessário informar o setor, uma vez que o usuário também não precisa de setor
ALTER TABLE projeto ALTER COLUMN cod_setor DROP NOT NULL;


-- Criação de procedimento para incluir usuário chefe em membros do projeto
-- A cada projeto novo deve ser incluido na tabela membro projeto o usuário chefe.
--	Esta rotina deve ser executada caso: o usuário tenha sido escolhido com chefe de projeto.
--	Exemplo de utilização: SELECT incluir_membro_chefe_projeto();
CREATE OR REPLACE FUNCTION incluir_membro_chefe_projeto()
RETURNS TRIGGER AS $new_membro_chefe_projeto$
        DECLARE
                cod_usuario_chefe INT;
                str_id_projeto VARCHAR;
        BEGIN
                SELECT mp.cod_usuario INTO cod_usuario_chefe
                  FROM membro_projeto mp
                 WHERE mp.cod_projeto = NEW.id_projeto
                   AND mp.cod_usuario = NEW.cod_usuario_chefe;

                IF (TG_OP = 'INSERT') THEN
                        -- Atualiza os custos do projeto somando o salário do novo membro
                        INSERT INTO membro_projeto(cod_projeto, cod_usuario, data_inclusao, data_exclusao) VALUES (NEW.id_projeto, NEW.cod_usuario_chefe, CURRENT_TIMESTAMP, null);
                ELSIF (TG_OP = 'UPDATE') THEN
                        IF cod_usuario_chefe <> NEW.cod_usuario_chefe THEN
                                UPDATE membro_projeto SET cod_usuario = NEW.cod_usuario_chefe WHERE cod_projeto = NEW.id_projeto AND cod_usuario = OLD.cod_usuario_chefe;
                        END IF;
                ELSIF (TG_OP = 'DELETE') THEN
                        -- Caso houver uma exclusão de projeto, faz um delete cascade
                        str_id_projeto = NEW.id_projeto;

                        SELECT delete_cascade('public', 'projeto', str_id_projeto);
                END IF;

                RETURN NEW;
        END;
$new_membro_chefe_projeto$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION incluir_membro_chefe_projeto(); 
--

-- Criação de gatilho para procedimento de inclusão de usuário chefe do projeto em membros do projeto
CREATE TRIGGER t_incluir_membro_chefe_projeto AFTER INSERT OR UPDATE OR DELETE ON projeto FOR EACH ROW EXECUTE PROCEDURE incluir_membro_chefe_projeto();
--
-- DROP gatilho: DROP TRIGGER t_atualizar_salario_usuario ON usuario; 
--



-- Criação de procedimento para atualizar custo ao incluir/alterar/excluir usuário ao grupo de membros do projeto
-- A cada operação com usuários implica em alteração de custo.
--	Esta rotina deve ser executada caso: o usuário tenha sofrido qualquer alteração, ou tenha sido incluido ou excluído dos membros do projeto.
--	Exemplo de utilização: SELECT atualizar_custo_membro_projeto();
CREATE OR REPLACE FUNCTION atualizar_custo_membro_projeto()
RETURNS TRIGGER AS $new_custo_membro_projeto$
        DECLARE
                salario_new DECIMAL(13,2);
                salario_old DECIMAL(13,2);
        BEGIN	
                -- Obtém o salário do novo membro do projeto
                SELECT u.salario / (SELECT COUNT(*) FROM membro_projeto mp WHERE mp.cod_usuario = NEW.cod_usuario) INTO salario_new
                  FROM usuario u 
                 WHERE u.id_usuario = NEW.cod_usuario;

                IF (TG_OP = 'INSERT') THEN
                        -- Atualiza os custos do projeto somando o salário do novo membro
                        UPDATE projeto SET custo_prev = (custo_prev + salario_new) WHERE id_projeto = NEW.cod_projeto;

                        RETURN NEW;
                ELSIF ((TG_OP = 'UPDATE') OR (TG_OP = 'DELETE')) THEN
                        -- Obtém o salário do antigo membro(pois pode ter havido mudança de membro)
                        SELECT u.salario / (SELECT COUNT(*) FROM membro_projeto mp WHERE mp.cod_usuario = OLD.cod_usuario) INTO salario_old
                          FROM usuario u 
                         WHERE u.id_usuario = OLD.cod_usuario;

                        IF (TG_OP = 'UPDATE') THEN
                                -- Verifica se o usuário trocou de projeto e atualiza os custos de ambos os projetos
                                IF NEW.cod_projeto <> OLD.cod_projeto THEN
                                        UPDATE projeto SET custo_prev = (custo_prev - salario_old) WHERE id_projeto = OLD.cod_projeto;
                                END IF;

                                UPDATE projeto SET custo_prev = (custo_prev + salario_new) WHERE id_projeto = NEW.cod_projeto;

                                RETURN NEW;
                        ELSIF (TG_OP = 'DELETE') THEN
                                -- Atualiza os custos do projeto deduzindo o salário do antigo membro
                                UPDATE projeto SET custo_prev = (custo_prev - salario_new) WHERE id_projeto = NEW.cod_projeto;

                                RETURN OLD;
                        END IF;
                END IF;
        END;
$new_custo_membro_projeto$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION atualizar_custo_membro_projeto(); 
--

-- Criação de gatilho para procedimento para atualizar custo ao incluir/alterar/excluir usuário ao grupo de membros do projeto
CREATE TRIGGER t_atualizar_custo_membro_projeto AFTER INSERT OR UPDATE OR DELETE ON membro_projeto FOR EACH ROW EXECUTE PROCEDURE atualizar_custo_membro_projeto();
--
-- DROP gatilho: DROP TRIGGER t_atualizar_custo_membro_projeto ON membro_projeto; 
--


-- Alteração da tabela de logs de custos do projeto para incluir campo com o valor do custo
ALTER TABLE log_custo_projeto ADD COLUMN valor_custo DECIMAL(13,2) NOT NULL DEFAULT 0.00;


-- Criação de procedimento para atualizar custo ao incluir/alterar/excluir um log de custo
-- A cada novo log de custo gera um valor que deve ser acrescido ao projeto.
--	Esta rotina deve ser executada caso: um log de custo tenha sofrido qualquer alteração, ou tenha sido incluido ou excluído do projeto.
--	Exemplo de utilização: SELECT atualizar_custo_log_custo();
CREATE OR REPLACE FUNCTION atualizar_custo_log_custo()
RETURNS TRIGGER AS $new_custo_log_custo$
        DECLARE
                valor_custo_new DECIMAL(13,2);
        BEGIN	
                -- Obtém asoma de custos vindos dos logs de custos do projeto
                SELECT SUM(lcp.valor_custo) INTO valor_custo_new
                  FROM log_custo_projeto lcp 
                 WHERE lcp.cod_projeto = NEW.cod_projeto;

                IF (TG_OP = 'INSERT') THEN
                        -- Atualiza os custos do projeto somando o valor do novo custo
                        UPDATE projeto SET custo_prev = (custo_prev + valor_custo_new) WHERE id_projeto = NEW.cod_projeto;

                        RETURN NEW;
                ELSIF (TG_OP = 'UPDATE') THEN
                        -- Verifica se o custo trocou de projeto e atualiza os custos de ambos os projetos
                        IF NEW.cod_projeto <> OLD.cod_projeto THEN
                                UPDATE projeto SET custo_prev = (custo_prev - OLD.valor_custo) WHERE id_projeto = OLD.cod_projeto;
                        END IF;

                        UPDATE projeto SET custo_prev = (custo_prev + valor_custo_new) WHERE id_projeto = NEW.cod_projeto;

                        RETURN NEW;
                ELSIF (TG_OP = 'DELETE') THEN
                        -- Atualiza os custos do projeto deduzindo o salário do antigo membro
                        UPDATE projeto SET custo_prev = (custo_prev - NEW.valor_custo) WHERE id_projeto = NEW.cod_projeto;

                        RETURN OLD;
                END IF;
        END;
$new_custo_log_custo$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION atualizar_custo_log_custo(); 
--

-- Criação de gatilho para procedimento para atualizar custo ao incluir/alterar/excluir usuário ao grupo de membros do projeto
CREATE TRIGGER t_atualizar_custo_log_custo AFTER INSERT OR UPDATE OR DELETE ON log_custo_projeto FOR EACH ROW EXECUTE PROCEDURE atualizar_custo_log_custo();
--
-- DROP gatilho: DROP TRIGGER t_atualizar_custo_log_custo ON log_custo_projeto; 
--