/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 27/11/2017
 */
-- Retirada a restrição de UNQUE KEY do atributo CNPJ.
-- Criado procedimento para:
--    1 - Verificar CNPJ duplicado para mesmo administrador;
--    2 - Excluir registros da tabela empresa_usuarios antes de excluir uma empresa.
-- Criado gatilho para procedimento.
-- Criado procedimento para:
--    1 - Incluir registros na tabela empresa_usuarios após incluir uma empresa.
-- Criado gatilho para procedimento.

-- Retirada a restrição de UNQUE KEY do atributo CNPJ para permitir que uma empresa possa seu novamente cadastrada e utilizada por outros administradores.
--CREATE UNIQUE INDEX "empresa_cnpj_Idx" ON public.empresa USING btree (cnpj COLLATE pg_catalog."default");
DROP INDEX public."empresa_cnpj_Idx";

-- Para cada insert ou update na tabela é preciso verificar se o CNPJ já existe para o usuário administrador, que está inserindo.
-- Para deletar é preciso antes excluir registro da tabela empres_usuario
-- Este procedimento permite incluir, alterar ou excluir um registro na tabela empresa somente se o usuário não cadastrou nenhuma empresa com aquele CNPJ.
-- Antes de cada ação o gatilho será disparado. 
CREATE OR REPLACE FUNCTION iudEmpresa()
RETURNS TRIGGER AS $iudEmpresa$
    DECLARE
        nome_empresa VARCHAR;
    BEGIN	
        IF ((TG_OP = 'INSERT') OR (TG_OP = 'UPDATE')) THEN
            SELECT empresa.nome_empresa INTO nome_empresa
              FROM empresa INNER JOIN empresa_usuarios ON(empresa_usuarios.cod_empresa = empresa.cod_empresa AND empresa_usuarios.cod_usuario = empresa.usuario_administrador)
             WHERE empresa.cnpj = NEW.cnpj AND empresa.usuario_administrador = NEW.usuario_administrador AND empresa.cod_empresa != NEW.cod_empresa;

            IF nome_empresa != '' THEN
                RAISE EXCEPTION 'Você já administra a empresa: %, que usa o CNPJ informado', nome_empresa;
            END IF;

            RETURN NEW;
        ELSIF (TG_OP = 'DELETE') THEN
            DELETE FROM empresa_usuarios 
                WHERE cod_empresa = OLD.cod_empresa 
                  AND cod_usuario = OLD.usuario_administrador;

            RETURN OLD;
        END IF;
    END;
$iudEmpresa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION iudEmpresa(); 
--

-- Criação de gatilho para procedimento de inclusão ou alteração de empresa com CNPJ duplicado para o mesmo administrador
CREATE TRIGGER t_iudEmpresa BEFORE INSERT OR UPDATE OR DELETE ON empresa FOR EACH ROW EXECUTE PROCEDURE iudEmpresa();
--
-- DROP gatilho: DROP TRIGGER t_iudEmpresa ON empresa; 
--

-- Após inserir com sucesso uma empresa então atualiza as empresa do usuário
-- Este procedimento permite incluir ou alterar um registro na tabela empresa_usuario somente se a empresa foi incluída.
-- Após cada ação o gatilho será disparado. 
CREATE OR REPLACE FUNCTION iuEmpresa()
RETURNS TRIGGER AS $iuEmpresa$
    DECLARE
        cont int;
    BEGIN	
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO empresa_usuarios(cod_empresa, cod_usuario) 
                VALUES (NEW.cod_empresa, NEW.usuario_administrador);
        ELSIF (TG_OP = 'UPDATE') THEN
            UPDATE empresa_usuarios SET cod_empresa = NEW.cod_empresa 
                WHERE cod_empresa = OLD.cod_empresa AND cod_usuario = OLD.usuario_administrador;
            UPDATE empresa_usuarios SET cod_usuario = NEW.usuario_administrador 
                WHERE cod_empresa = OLD.cod_empresa AND cod_usuario = OLD.usuario_administrador;
        END IF;

        RETURN NEW;
    END;
$iuEmpresa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION iuEmpresa(); 
--

-- Criação de gatilho para procedimento de inclusão ou alteração de empresa com CNPJ duplicado para o mesmo administrador
CREATE TRIGGER t_iuEmpresa AFTER INSERT OR UPDATE ON empresa FOR EACH ROW EXECUTE PROCEDURE iuEmpresa();
--
-- DROP gatilho: DROP TRIGGER t_iuEmpresa ON empresa; 
--