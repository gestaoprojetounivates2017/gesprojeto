/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Sergio
 * Created: 22/11/2017
 */
-- Para cada insert, update ou delete na tabela de "empresa".
-- Este procedimento inclui um registro na tabela de empresa_usuario
-- Após cada ação o gatilho será disparado. 
CREATE OR REPLACE FUNCTION iudEmpresa()
RETURNS TRIGGER AS $iudEmpresa$
    BEGIN	
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO empresa_usuarios(cod_empresa, cod_usuario) 
                VALUES (NEW.cod_empresa, NEW.usuario_administrador);
        ELSIF (TG_OP = 'UPDATE') THEN
            UPDATE empresa_usuarios SET cod_empresa = NEW.cod_empresa WHERE cod_empresa = OLD.cod_empresa AND cod_usuario = OLD.usuario_administrador;
            UPDATE empresa_usuarios SET cod_usuario = NEW.usuario_administrador WHERE cod_empresa = OLD.cod_empresa AND cod_usuario = OLD.usuario_administrador;
        ELSIF (TG_OP = 'DELETE') THEN
            DELETE FROM empresa_usuarios WHERE cod_empresa = OLD.cod_empresa AND cod_usuario = OLD.usuario_administrador;
        END IF;

        RETURN NEW;
    END;
$iudEmpresa$ LANGUAGE plpgsql;
--
-- DROP função: DROP FUNCTION iudEmpresa(); 
--

-- Criação de gatilho para procedimento de inclusão de usuário chefe e empresa em empresa_usuarios apos incluir empresa
CREATE TRIGGER t_iudEmpresa AFTER INSERT OR UPDATE OR DELETE ON empresa FOR EACH ROW EXECUTE PROCEDURE iudEmpresa();
--
-- DROP gatilho: DROP TRIGGER t_iudEmpresa ON empresa; 
--