<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controladora principal, colocar todas as funções comuns das controladoras para herdar nas controlaras principais
 * @author Lucas Leandro de Moura <lucasleandrodemoura@gmail.com:
 * @data 2017-08-20
 */
class MY_Controller extends CI_Controller {

    protected $codigo_usuario;

    /**
     * Variável que contém o cabecalho da table
     * @var array 
     */
    protected $cabecalho = array();

    /**
     *Define se a tela terá filtros, estes filtros serão pegos do estruturador, que tiver macardo "É filtro"?
     * @var type 
     */
    private $filtros = array();
    
    
    protected $filtros_enable = true;

    /**
     *Define o endereço do botão novo
     * @var type 
     */
    protected $url_novo = "";
    
    /**
     * Linhas da tabela
     * @var array
     */
    protected $linhas = array();
    
    /**
     * Determina se o botão novo irá aparecer na tela de list
     * @var boolean 
     */
    protected $botao_novo = true;
    
    /**
     *Define o endereço do botão voltar
     * @var string $botao_voltar 
     */
    protected $botao_voltar = "";




    /**
     * Qual o nome da tabela no banco de dados, que se trata este controlador
     * @var String
     */
    protected $tabela = "";

    /**
     * Define os dados de uma grid
     * @var array Dados da tabela 
     */
    protected $dados = array();

    /**
     * DataSource para realizar upgrade ou insert
     * @var array() Grade de informações
     */
    protected $data = array();

    /**
     * Ação ao submeter o formulário padrão
     * @var String 
     */
    protected $acao = "";

    /**
     * Define a View de cadastro padrão
     * @var String 
     */
    protected $view_cadastro_padrao = "default_cadastro";

    /**
     * Nome da tela
     * @var String 
     */
    protected $titulo = "";

    /**
     * Variável que define se é para exibir menu ou não
     * @var type 
     */
    private $menu = true;
    /**
     *Nome da view default para listagens
     * @var type 
     */
    private $view_default_list = "default_list";
    
    protected $nivel_minimo_acesso = 0;
    protected $erro = "";
    
    /**
     *Armazena todas as empreass que o usuário logado teria acesso
     * @var type 
     */
    protected $empresas_acesso = array();
    

    /**
     * Função responsável em realizar a checagem se existe uma sessão ativa
     * Esta função poder ser utilizado nos controladores que precisam de controle de acesso
     */
    protected function autentica() {
        if ($this->session->userdata('logado')) {
            
            global $meus_projetos;
            global $header_nav;
            $this->codigo_usuario = $this->session->userdata('id_usuario');
            
            $this->db->where("id_usuario", $this->codigo_usuario);
            $url = $this->uri->uri_string();
            if(trim($this->db->get("usuario")->result()[0]->nome)=="" 
                    && $url != "usuarios/meus_dados"
                    && $url != "usuarios/atualizar_dados"){
                redirect("usuarios/meus_dados");
            }
            //Se não tiver nivel minimo de acesso
            if($this->session->userdata('nivel_usuario')<$this->nivel_minimo_acesso){
                redirect("home");
            }
            
            $this->db->select("ROUND((select AVG(projeto_tarefa.progresso) as media from projeto_tarefa where cod_projeto = projeto_tarefa.cod_projeto),2) as p_concluido,projeto.*");
            $this->db->join("empresa_setores", "projeto.cod_setor = empresa_setores.id_setor", "inner");
            $this->db->join("empresa_usuarios", "empresa_usuarios.cod_empresa = empresa_setores.codigo_empresa AND empresa_usuarios.cod_usuario = ". $this->session->userdata('id_usuario'), "inner");
            $meus_projetos = $this->db->get("projeto")->result();   

            $header_nav["menus"] = "";
            $header_nav["categorias"] = "";
            $this->db->select("dependente");
            $this->db->where("dependente is not null");
            $this->db->where("nivel_acesso <= ".$this->session->userdata('nivel_usuario'));
            $this->db->group_by("dependente");
            
            foreach($this->db->get("estruturador_menus")->result() as $categoria){
                $this->db->where("codigo_menu",$categoria->dependente);
                $this->db->where("nivel_acesso <= ".$this->session->userdata('nivel_usuario'));
                $this->db->order_by("descricao");
                $header_nav["categorias"][$categoria->dependente] = $this->db->get("estruturador_menus")->result();
                
                $this->db->where("dependente",$categoria->dependente);
                $this->db->where("nivel_acesso <= ".$this->session->userdata('nivel_usuario'));
                $this->db->order_by("descricao");
                $header_nav["menus"][$categoria->dependente] = $this->db->get("estruturador_menus")->result();
                
            }
            
            
            if($this->tabela!=""){
                $this->setTabela($this->tabela);
            }


            $this->empresas_acesso = 
            $this->db->query("select cod_empresa as cod 
                from empresa 
                where usuario_administrador = ".$this->codigo_usuario."

                UNION

                select codigo_empresa as cod from projeto_membros 
                inner join projeto on projeto_membros.cod_projeto = projeto.id_projeto 
                inner join empresa_setores on projeto.cod_setor = empresa_setores.id_setor 
                where cod_usuario = ".$this->codigo_usuario."
                
                UNION
                select cod_empresa as cod from empresa_usuarios where cod_usuario = ".$this->codigo_usuario)->result();
        } 
        else {
            redirect("Login");
        }
    }
    
    /**
     * 
     * Retorna um grupo de empresas ao qual o usuário tem acesso
     * @param type $campo Campo identificador para verificação de empresas 
     * @return type condição para a pesquisa
     */
    function getEmpresasComAcessos($campo){
        $where_empresas = "";

        foreach($this->empresas_acesso as $empresa_acesso){
            if($where_empresas!=""){
                $where_empresas.=",";
            }
            $where_empresas.=$empresa_acesso->cod;
        }
        
        // Se não tiver resultado para empresas de acesso então a condição de pesquisa será -1
        if (!empty($where_empresas)) {
            return $campo." IN (".$where_empresas.") ";
        } else {
            return $campo." = -1";
        }
    }


    
    /**
     * Altera a view que será chamada nas telas de listagem
     * @author Lucas Leandro de Moura
     * @param type $view_default_list Nome da VIEW
     */
    function setView_default_list($view_default_list) {
        $this->view_default_list = $view_default_list;
    }
    
    /**
     * Botão boltar
     * @param string url
     */
    function setBotao_voltar($botao_voltar) {
        $this->botao_voltar = $botao_voltar;
    }

    
    /**
     * Determina se o botão novo vai ou não ser exibido
     * @param type $botao_novo
     */
    function setBotao_novo($botao_novo) {
        $this->botao_novo = $botao_novo;
    }

        
    /**
     * Função que gera um PDF
     * @author Lucas Leandro de Moura <lmoura@universo.univates.br>
     * @param String $nome Nome do arquivo
     * @param html $conteudo Conteúdo que terá no PDF
     */
    protected function geraPDF($nome, $conteudo) {
        $this->autentica();
        require_once '/var/www/gestaoprojetos/Includes/mpdf/vendor/autoload.php';
        $mpdf = new mPDF();
        
        $mpdf->WriteHTML($conteudo);
        $mpdf->Output();
    }

    /**
     * Função que envia e-mails para um destinatário
     * @link https://www.codeigniter.com/user_guide/libraries/email.html Referência
     * @author Lucas Leandro de Moura <lmoura@universo.univates.br>
     * @see 2017
     * @example $this->enviar_email("destino@destino.com.br", "Assunto", "Mensagem de corpo");
     * @param String $para Para quem será enviado o e-mail
     * @param String $assunto Assunto da mensagem
     * @param String $mensagem Mensagem que será enviada
     * @return boolean Retorna true se o e-mail foi enviado com sucesso
     */
    protected function enviar_email($para, $assunto, $mensagem) {
        //Configurações do servidor de envio de e-mails
        $this->load->library('email');
        $config["protocol"] = "smtp";
        $config["smtp_host"] = "smtp.umbler.com";
        $config["smtp_user"] = "angra@angra.me";
        $config["smtp_pass"] = "projetostcc";
        $config["smtp_port"] = "587";
        $config["mailtype"] = "html";
        //$config["smtp_crypto"] = "ssl";
        $this->email->initialize($config);
        
        //Configuração de envio de mensagens
        $this->email->from('angra@angra.me');
        $this->email->to($para);
        $this->email->subject($assunto);
        $this->email->set_newline("\r\n");
        $this->email->message($mensagem);

        return $this->email->send();
    }

    /**
     * Função que retorna uma DataTable preenchida
     * @param array $header Cabecalho da tabela
     * @param array $linhas Dados que estarão no banco de dados array[]["dados"] = array("","","","")
     * @param type $editar Haverá botão de Editar?
     * @param type $deletar Haverá botão de deletar?
     * @param type $visualizar Haverá botão de Visualizar?
     * @author Lucas Leandro de Moura <lmoura@universo.univates.br>
     * @return html Tabela em formato HTML
     */
    protected function table($header, $linhas, $editar = "", $deletar = "", $visualizar = "") {
        $retorno = "<table  class='table table-striped table-bordered table-hover' id='dataTables-dados'>";
        $retorno .= "    <thead>";
        $retorno .= "        <tr>";

        //Adicionando o cabecalho da tabela
        foreach ($header as $item) {
            $retorno .= "<th>";
            $retorno .= $item["titulo"];
            $retorno .= "</th>";
        }
        $retorno .= "        </tr>";
        $retorno .= "    </thead>";
        $retorno .= "    <tbody>";
        //Adicionando as linhas da tabela
        foreach ($linhas as $registro) {
            $retorno .= "        <tr>";
            foreach ($registro["dados"] as $item) {
                $retorno .= "<td>";
                $retorno .= $item;
                $retorno .= "</td>";
            }
            $retorno .= "        </tr>";
        }
        $retorno .= "    </tbody>";

        $retorno .= "</table>";



        return $retorno;
    }

    /**
     * 
     * @param String $tabela Informe a tabela
     */
    function setTabela(String $tabela) {
        $this->tabela = $tabela;
        global $infoSessao;
        
        if(!array_key_exists($tabela, $this->session->userdata("filtros"))){
           $infoSessao["filtros"] = $this->session->userdata("filtros");
           $infoSessao["filtros"][$this->tabela] = array();
           $this->session->set_userdata($infoSessao);            
           
        }
        
        
        if($this->input->get("filtro")=="submit"){
            //Grava os filtros na sessão para ser reaproveitado
            $infoSessao["filtros"] = $this->session->userdata("filtros");
            foreach ($this->input->post() as $key=>$value){
                
               $infoSessao["filtros"][$this->tabela][$key] = $value;
               

            }
            $this->session->set_userdata($infoSessao);
        }
        
    }

    function setCabecalho($cabecalho) {
        $this->cabecalho = $cabecalho;
    }

    function setLinhas($linhas) {
        $this->linhas = $linhas;
    }

    function setTitulo(String $titulo) {
        $this->titulo = $titulo;
    }

    function index($indices = array()) {
        
        $this->autentica();
        $this->load->view("Includes/header");
              
        global $header_nav;
      
        $this->load->view("Includes/header_nav", $header_nav);

        $dados["tabela"] = $this->table($this->cabecalho, $this->linhas);
        $dados["titulo"] = $this->titulo;
        $dados["tabela_db"] = $this->tabela;
        $dados["uri_completa"] = $this->uri->uri_string();
        $dados["url_novo"] = $this->url_novo;
        $dados["botao_novo"] = $this->botao_novo;
        $dados["botao_voltar"] = $this->botao_voltar;
        $this->setFiltrosDados();
        $dados["filtros"] = $this->filtros;

        if($this->session->userdata("erro")==""){
           $dados["erro"] = $this->erro;
        }else{
           $dados["erro"] = $this->session->userdata("erro");
           $infoSessao["erro"] = "";
           $this->session->set_userdata($infoSessao);   
        }
        
        
        $dados["indices"] = $indices;

        $this->load->view($this->view_default_list, $dados);
        $this->load->view("Includes/footer");
        
    }
    function getUrl_novo(): type {
        return $this->url_novo;
    }

    
    function setUrl_novo(String $url_novo) {
        $this->url_novo = $url_novo;
    }

        /**
     * Define os dados
     * @param array $dados
     */
    function setDados(array $dados) {
        $this->dados = $dados;
    }

    function setAcao(String $acao) {
        $this->acao = $acao;
    }

    /*
     * Define Componentes no dataSource
     * @author Lucas Leandro de Moura
     */

    function setData(array $data) {
        $this->data = $data;
    }

    /**
     * Habilita o menu na tela
     * @author Lucas Leandro de Moura
     * @param bool $habilitado = true
     */
    function setMenu(bool $habilitado) {
        $this->menu = $habilitado;
    }

    /**
     * Tela padrão de cadastro
     * @author Lucas Leandro de Moura
     */
    function cadastro($indices=array()) {
        $this->autentica();
        $this->load->view("Includes/header");

        $dados["tabela"] = $this->table($this->cabecalho, $this->linhas);
        $dados["titulo"] = $this->titulo;
        $dados["acao"] = $this->acao;
        $dados["erro"] = $this->erro;

        //Monta o menu
        if ($this->menu) {
            global $header_nav;
            $this->load->view("Includes/header_nav", $header_nav);
            
        }

        $this->db->where("tabela", $this->tabela);
        $this->db->where("exibir_cadastro", true);
        $this->db->order_by("ordenacao ASC");
        $dados["campos"] = $this->db->get("estruturador")->result();

        //Define os dados do formulário, caso for edição
        if (sizeof($this->dados) == 1) {
            $dados["dados"] = (array) $this->dados[0];
        } else {
            $dados["dados"] = "";
        }
       
        $dados["indices"] = $indices;
       

        $this->load->view($this->view_cadastro_padrao, $dados);
        $this->load->view("Includes/footer");
    }
    
    private function converteTelatoBD($tabela,$campo,$valor){
        $this->db->where("tabela",$tabela);
        $this->db->where("coluna",$campo);
        
        $tipo_dado = $this->db->get("estruturador")->result()[0]->tipo_campo;
        
        if($tipo_dado==5){
    
            $valor = date("Y-m-d", strtotime(str_replace('/', '-', $valor)));
           
        }
        
        return $valor;
    }
    
   

    /**
     * Função que realiza um cadastro genérico
     * @author Lucas Leandro de Moura
     */
    function cadastrar() {
        $this->autentica();
        $infoTela = $this->input->post();
        $edicao = false;
        
        foreach ($infoTela as $key =>$valor) {
            if ($key != "btn_salvar") {
                if($valor==""){
                    $valor = null;
                }else {
                    $valor = $this->converteTelatoBD($this->tabela,$key,$valor);
                }
                //Se a chave não existe dai cria se não ele já foi criado pelo programador
                //Antes de chamar o cadastrar
                if(!array_key_exists($key, $this->data)){
                    $this->data[$key] = $valor;
                }
            }
        }
        
        if($this->input->get()){
            $indices = $this->input->get();
           
            foreach ($indices as $key => $valor) {
                $this->db->where($key,$valor);            
                $edicao = true;
            }
        }
        $retorno = "";
        $retorno["erro"] = "";
        
        
        //Se for edição
        if($edicao==true){
            $retorno["erro"] = $this->db->update($this->tabela, $this->data);
            $retorno["mensagem"] = $this->db->error()["message"];
            
        }else{
            $retorno["erro"] = $this->db->insert($this->tabela, $this->data);
            $retorno["mensagem"] = $this->db->error()["message"];
        }
        
        if($retorno["mensagem"]!=""){
            $infoSessao["erro"] = "Erro inesperado. <br> ".$retorno["mensagem"];
            $this->session->set_userdata($infoSessao);
        }
        
        return $retorno;
        
    }

    function getView_cadastro_padrao(): String {
        return $this->view_cadastro_padrao;
    }

    /**
     * Define uma nova view de cadastro padrão
     * @author Lucas Moura<lmoura@universo.univates.br>
     * @param String $view_cadastro_padrao
     */
    function setView_cadastro_padrao(String $view_cadastro_padrao) {
        $this->view_cadastro_padrao = $view_cadastro_padrao;
    }
    
    /**
     * Define se terá ou não filtros a tela de visualização
     * @param bool $filtros
     */
    function setFiltros(bool $filtros) {
        $this->filtros_enable = $filtros;
    }
    
    private function setFiltrosDados(){
        if($this->filtros_enable){
            $this->db->where("e_filtro",true);
            $this->db->where("tabela", $this->tabela);
            $this->filtros = $this->db->get("estruturador")->result();
            
        }
    }
    
    /**
     * Exclui o registro
     * @param type $indice
     */
    function excluir(){
        $this->autentica();
        $indices = $this->input->get();
        
 
        foreach ($indices as $key => $valor) {
            $this->db->where($key,$valor);            
        }
        
        $deletar = $this->db->delete($this->tabela);
        
        if(!$deletar){
            
            $this->erro = "<b>Houve um erro ao excluir este registro!</b><br>Mensagem do erro:".$this->db->error()["message"];
           
            $infoSessao["erro"] = $this->erro;
            $this->session->set_userdata($infoSessao);
      
        }
        return $this->erro;
        
    }
    
    
    function consultapadrao($campo){
    
        $this->autentica();
        $this->db->where("codigo_campo",$campo);
        $infoCampos = $this->db->get("estruturador")->result()[0];
        //print_r($this->session->userdata('indice_record'));
      
         foreach($this->session->userdata('indice_record') as $in=>$in_val){
            
               
                $infoCampos->where_dependencia = str_replace("{".$in."}", $in_val, $infoCampos->where_dependencia);
                
              
        }
      
        
        $this->db->select($infoCampos->coluna_dependencia." as codigo, ".$infoCampos->exibicao_dependencia." as descricao");
        if($infoCampos->where_dependencia){
            $this->db->where($infoCampos->where_dependencia);
        }
        
        $resultados = $this->db->get($infoCampos->tabela_dependencia)->result();                
        
        $cabecalho = array(
            array("titulo"=>"Código"),
            array("titulo"=>"Descrição"),
            array("titulo"=>"Ações")
        ); 
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);
      
        $dados = array();
        foreach($resultados as $item){
          
            
          $dados[]["dados"] = array(
              $item->codigo,
              $item->descricao, 
              '<a href="javascript:thisJanela.seleciona('.$item->codigo.','.$campo.');" class="btn btn-default"><i class="fa fa-folder-open"></i></a>'
              );  
        }
        
        //Defino os dados da tabela
        $this->setLinhas($dados);
        
        
        
        
        $this->load->view("Includes/header");
        $dados["tabela"] = $this->table($this->cabecalho, $this->linhas);
        $this->load->view("consultapadrao", $dados);
        $this->load->view("Includes/footer");
        
    }


}
