<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controlador responsável por construir o estruturador de dados
 * Com estas informações, é possível construir telas genéricas de cadastro
 */
class Estruturador extends MY_Controller {
    //Utilização padrão
    


    public function index() {
        //Defino o título da página
        $this->setTitulo("Estruturador");
        $this->setTabela("estruturador");
        
        //Habilitar filtros
        $this->setFiltros(true);
        
        $cabecalho = array(
            array("titulo"=>"Cód"),
            array("titulo"=>"Orden"),
            array("titulo"=>"Tab"),
            array("titulo"=>"Col"),
            array("titulo"=>"Rót"),
            array("titulo"=>"Tip Campo"),
            array("titulo"=>"Tip Valor"),
            array("titulo"=>"Ações")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        
        
        $dados = array();
        $this->db->join("estruturador_tipos_campo",
                "estruturador_tipos_campo.tipo_codigo_tipo = estruturador.tipo_campo",
                "inner");
        $this->db->join("estruturador_tipos_valor",
                "estruturador_tipos_valor.tipo_codigo_valor = estruturador.tipo_valor",
                "inner");
        //Aplica os filtros de tela
        
        foreach($this->session->userdata("filtros")[$this->tabela] as $key=>$value){
            if($value!=""){
                $this->db->where($this->tabela.".".$key,$value);
            }
        }
        
        $resultados = $this->db->get("estruturador")->result();

        foreach($resultados as $item){
          $indices = array(array("codigo_campo"=>$item->codigo_campo));  
            
          $dados[]["dados"] = array(
              $item->codigo_campo,
              $item->ordenacao,
              $item->tabela,
              $item->coluna,
              $item->rotulo,
              $item->tipo_campo_descricao,
              $item->tipo_valor_descricao,
              L_Deletar($indices, "estruturador/excluir")."".
              L_Editar($indices, "estruturador/editar")
              );  
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);
       
        
        parent::index();
    }
    
    public function cadastro() {
        $this->setTabela("estruturador");
        $this->setTitulo("Cadastro de tabelas");
        $this->setAcao("estruturador/cadastrar");
        
       
        parent::cadastro();

        
    }
    
    
    public function excluir() {
        $this->setTabela("estruturador");
        
        parent::excluir();
        redirect("estruturador");
        
    }
    
    public function cadastrar() {
        $this->setTabela("estruturador");
        
        parent::cadastrar();
        redirect("estruturador");
        
    }
    
    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->setTabela("estruturador");
        $this->setTitulo("Editar campo");
        $this->setAcao("estruturador/cadastrar?codigo_campo=".$this->input->get("codigo_campo"));
        
        $this->db->where("codigo_campo",$this->input->get("codigo_campo"));
        $this->setDados($this->db->get("estruturador")->result());
        
        parent::cadastro();
        
    }
    
}
            

        



