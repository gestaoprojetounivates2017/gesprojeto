<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_setores extends MY_Controller {

    //esta tela é inicial. index... quando acessar projetos.adevale.com.br/setor abrira este controlador
    public function index() {
        $this->autentica();
        // Definindo o título da janela
        $this->setTitulo("Setores");
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Empresa"),
            array("titulo" => "Descrição"),
            array("titulo" => "Chefe"),
            array("titulo" => "Ativo"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        $this->setFiltros(true);

        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela setor
        $this->db->select("usuario.nome,empresa_setores.*,empresa.nome_empresa");
        $this->db->join("usuario", "empresa_setores.cod_usuario_chefe = usuario.id_usuario", "inner");
        $this->db->join("empresa", "empresa_setores.codigo_empresa = empresa.cod_empresa", "inner");
        $this->db->where($this->getEmpresasComAcessos("empresa.cod_empresa"));

        foreach ($this->session->userdata("filtros")[$this->tabela] as $key => $value) {
            if ($value != "") {
                $this->db->where($this->tabela . "." . $key, $value);
            }
        }

        $resultados = $this->db->get("empresa_setores")->result();
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            $indices = array(array("id_setor" => $item->id_setor));

            $dados[]["dados"] = array(
                $item->id_setor,
                $item->nome_empresa,
                $item->descricao_resumida,
                $item->nome,
                L_boolean($item->ativo),
                L_Deletar($indices, "empresa_setores/excluir") . " " .
                L_Editar($indices, "empresa_setores/editar")
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::index($indices);
    }

    public function excluir() {
        $this->autentica();
        $this->setTabela("empresa_setores");

        parent::excluir();
        redirect("empresa_setores");
    }

    public function cadastro() {
        $this->autentica();
        $this->setTabela("empresa_setores");
        $this->setTitulo("Cadastro de setor");
        $this->setAcao("empresa_setores/cadastrar");

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

    public function cadastrar() {
        $this->autentica();
        $this->setTabela("empresa_setores");

        parent::cadastrar();
        redirect("empresa_setores");
    }

    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->autentica();
        $this->setTabela("empresa_setores");
        $this->setTitulo("Cadastro de setor");

        $this->db->where("id_setor", $this->input->get("id_setor"));
        $this->setDados($this->db->get("empresa_setores")->result());
        $this->setAcao("empresa_setores/cadastrar?id_setor=" . $this->input->get("id_setor"));

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

}
