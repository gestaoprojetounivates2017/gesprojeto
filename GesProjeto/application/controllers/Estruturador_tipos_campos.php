<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controlador responsável por construir o estruturador de dados
 * Com estas informações, é possível construir telas genéricas de cadastro
 */
class Estruturador_tipos_campos extends MY_Controller {
    //Utilização padrão
    


    public function index() {
        //Defino o título da página
        $this->setTitulo("Tipos de campos");
        $this->setTabela("estruturador_tipos_campos");
        
        //Habilitar filtros
        $this->setFiltros(true);
        
        $cabecalho = array(
            array("titulo"=>"Cód"),
            array("titulo"=>"Descrição")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        
        
        $dados = array();
        
        
        $resultados = $this->db->get("estruturador_tipos_campo")->result();

        foreach($resultados as $item){
          $indices = array(array("tipo_codigo_tipo"=>$item->tipo_codigo_tipo));  
            
          $dados[]["dados"] = array(
              $item->tipo_codigo_tipo,
              $item->tipo_campo_descricao
              );  
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);
       
        
        parent::index();
    }
    
    public function cadastro() {
        $this->setTabela("estruturador_tipos_campo");
        $this->setTitulo("Tipos de campos");
        $this->setAcao("estruturador_tipos_campos/cadastrar");
        
       
        parent::cadastro();

        
    }
    
    
    
    public function cadastrar() {
        $this->setTabela("estruturador_tipos_campo");
        
        parent::cadastrar();
        redirect("estruturador_tipos_campos");
        
    }
    
    
}
            

        



