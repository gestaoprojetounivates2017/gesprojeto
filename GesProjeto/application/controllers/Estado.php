<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estado extends MY_Controller {
    protected $tabela = "estado";
    
    public function index() {
        //Defino o título da página
        $this->setTitulo("Estados");
        $this->setTabela("estado");
        $this->setFiltros(true);
        
        $cabecalho = array(
            array("titulo"=>"Código"),
            array("titulo"=>"País"),
            array("titulo"=>"Sigla"),
            array("titulo"=>"Descrição"),
            array("titulo"=>"Ações")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        
        
        $dados = array();
        $this->db->select("estado.*,pais.descricao_resumida as pais");
        $this->db->join("pais","estado.codigo_pais=pais.id_pais","inner");
        $resultados = $this->db->get("estado")->result();
        foreach($resultados as $item){
          $indices = array(array("codigo_estado"=>$item->codigo_estado));  
            
          $dados[]["dados"] = array(
              $item->codigo_estado,
              $item->pais,
              $item->sgl_estado,
              $item->descricao,
              L_Deletar($indices, "estado/excluir")." ".
              L_Editar($indices, "estado/editar")." ".
              L_Botao($indices, "estado/cidades", "fa-globe", "Abrir o cadatro de cidades")
              );  
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);
       
        
        parent::index();
    }
    
    
     public function cidades(){
        $this->autentica();
         global $infoSessao;
         $infoSessao["filtros"]["cidade"]["cod_estado"] = $this->input->get("codigo_estado");
         $this->session->set_userdata($infoSessao);
         redirect("cidade");
        
    }
    
    public function cadastro() {
        $this->setTabela("estado");
        $this->setTitulo("Cadastro de estado");
        $this->setAcao("estado/cadastrar");
        parent::cadastro();
    }
    
    public function cadastrar() {
        $this->autentica();
        $this->setTabela("estado");
        parent::cadastrar();
        redirect("estado");
    }
    
    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->setTabela("estado");
        $this->setTitulo("Cadastro de estado");
       
        $this->db->where("codigo_estado",$this->input->get("codigo_estado"));
        $this->setDados($this->db->get("estado")->result());
        $this->setAcao("estado/cadastrar?codigo_estado=".$this->input->get("codigo_estado"));
        
        parent::cadastro();
    }
    
}
            

        



