<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Lucas Leandro de Moura
 */
class Justificativas extends MY_Controller {
    protected $tabela = "justificativas";
    
    public function index() {
        // Definindo o título da janela
        $this->setTitulo("Justificativa");
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Descrição"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);

        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
        $resultados = $this->db->get("justificativas")->result();
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            $indices = array(array("codigo_justificativa" => $item->codigo_justificativa));

            $dados[]["dados"] = array(
                $item->codigo_justificativa,
                $item->descricao_justificativa,
                L_Deletar($indices, "justificativas/excluir") . " " .
                L_Editar($indices, "justificativas/editar")
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);
       
        
        
        parent::index();
    }

    public function cadastro() {
        $this->setTabela("justificativas");
        $this->setTitulo("Cadastro de justificativa");
        $this->setAcao("justificativas/cadastrar");
        parent::cadastro();
    }

    public function cadastrar() {
        $this->autentica();
        $this->setTabela("justificativas");
        parent::cadastrar();
        redirect("justificativas");
    }

    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->setTabela("justificativas");
        $this->setTitulo("Cadastro de justificativa");

        $this->db->where("codigo_justificativa", $this->input->get("codigo_justificativa"));
        $this->setDados($this->db->get("justificativas")->result());
        $this->setAcao("justificativas/cadastrar?codigo_justificativa=".$this->input->get("codigo_justificativa"));

        parent::cadastro();
    }
}
