<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    //Utilização padrão
    function index(){
        $this->autentica();
        $this->load->view("Includes/header");
        
        global $header_nav;
        
        $this->load->view("Includes/header_nav",$header_nav);
        
        //Quantidade de tarefas
        $this->db->select("count(*) as contagem");
        $this->db->where("resolvido",false);
        $this->db->where("responsavel", $this->codigo_usuario);
        
        $data["numero_tarefas"] = $this->db->get('projeto_tarefa')->result()[0]->contagem;
        
        
        
        //Quantidade de tarefas
        $this->db->select("count(*) as contagem");
        $this->db->where("resolvido",false);
        $this->db->where("data_previsto_final > '".date("Y-m-d")."'"); 
        $this->db->where("responsavel", $this->codigo_usuario);
        
        $data["numero_tarefas_atrasadas"] = $this->db->get('projeto_tarefa')->result()[0]->contagem;
      
        
        
        
        
        $this->db->select("AVG((data_previsto_final - data_real_final)) as contagem");
        $this->db->where("resolvido",TRUE);
        $this->db->where("responsavel", $this->codigo_usuario);
        
        $data["n_tarefas_time_ok"] = number_format($this->db->get('projeto_tarefa')->result()[0]->contagem,0);
      
        
        
        $this->db->select("count(*) as contagem");
        $this->db->join("projeto","projeto.id_projeto = projeto_membros.cod_projeto","inner");
        $this->db->where("projeto.concluido",FALSE);
        $this->db->where("cod_usuario", $this->codigo_usuario);
        
        $data["numero_projeto"] = number_format($this->db->get('projeto_membros')->result()[0]->contagem,0);
      
        
        $dados = $this->db->query("select Extract('Month' From data_previsto_final) as mes, 
            count(projeto_tarefa.id_tarefa) as contagem from projeto_tarefa
        INNER join projeto_membros ON projeto_membros.cod_projeto = projeto_tarefa.cod_projeto
        inner join projeto on projeto.id_projeto = projeto_membros.cod_projeto
        where projeto_membros.cod_usuario = ".$this->codigo_usuario."
        and Extract('Year' From data_previsto_final) = Extract('Year' From now())
        group by Extract('Month' From data_previsto_final)")->result();
        
        $data["grafico_previsto_final"] = array("01"=>0,
            "02"=>0,
            "03"=>0,
            "04"=>0,
            "05"=>0,
            "06"=>0,
            "07"=>0,
            "08"=>0,
            "09"=>0,
            "10"=>0,
            "11"=>0,
            "12"=>0);
        
        foreach($dados as $i){
            $data["grafico_previsto_final"][$i->mes] = $i->contagem;
        }
        
        
        
        
        
        $dados = $this->db->query("select Extract('Month' From data_real_final) as mes, 
            count(projeto_tarefa.id_tarefa) as contagem from projeto_tarefa
        INNER join projeto_membros ON projeto_membros.cod_projeto = projeto_tarefa.cod_projeto
        inner join projeto on projeto.id_projeto = projeto_membros.cod_projeto
        where projeto_membros.cod_usuario = ".$this->codigo_usuario."
        and Extract('Year' From data_real_final) = Extract('Year' From now())
        group by Extract('Month' From data_real_final)")->result();
        
        $data["grafico_real_final"] = array("01"=>0,
            "02"=>0,
            "03"=>0,
            "04"=>0,
            "05"=>0,
            "06"=>0,
            "07"=>0,
            "08"=>0,
            "09"=>0,
            "10"=>0,
            "11"=>0,
            "12"=>0);
        
        foreach($dados as $i){
            $data["grafico_real_final"][$i->mes] = $i->contagem;
        }
        
        
        
        $this->load->view("home",$data);
        $this->load->view("Includes/footer");
    }
    
    
    function exemplo_lista(){
        $this->setTitulo("Tela de exemplo");
        $this->setTabela("tabela_no_banco");
        $this->setFiltros(true);
        
        $cabecalho = array(
            array("titulo"=>"Código"),
            array("titulo"=>"Descrição 1"),
            array("titulo"=>"Desc 2")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);
         //Defino os dados da tabela
        $this->setLinhas($dados);
        parent::index();
    }
    
    
    
    
    function test(){
        $this->geraPDF("etatea", "lçmteaçmatelçatmlçaet");
    }
    
    function teste(){
        $this->load->view("Includes/header");
        $this->load->view("Includes/header_nav");
        $this->load->view("home");
        $this->load->view("Includes/footer");
    }
    
}