<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Lucas Leandro de Moura
 * Controla tudo referente aos usuários
 * @date 2017-09-01
 */
class Usuarios extends MY_Controller {
  
            
    function index($erro="") {
        $this->nivel_acesso = 9;
        $this->autentica();
        $this->setTabela("usuario");
        $this->setTitulo("Usuários do sistema");
        $this->setBotao_novo(false);
        
         $cabecalho = array(
            array("titulo"=>"Código"),
            array("titulo"=>"Nome"),
            array("titulo"=>"Login"),
            array("titulo"=>"E-mail"),
            array("titulo"=>"Ativo"),
            array("titulo"=>"Ações")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        
        
        $dados = array();
        $resultados = $this->db->get("usuario")->result();
        foreach($resultados as $item){
          $indices = array(array("id_usuario"=>$item->id_usuario)); 
                  
        
          $dados[]["dados"] = array(
              $item->id_usuario,
              $item->nome,
              $item->login,
              $item->email,
              L_boolean($item->ativo),
              L_Editar($indices, "usuarios/editar")." ".
              L_Deletar($indices, "usuarios/excluir")
              );  
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);
        $this->erro = $erro;
        
        parent::index();
    }
    
    
    
    
     public function excluir() {
         $this->nivel_acesso = 9;
         $this->autentica();
        $this->setTabela("usuario");
        $excluir = parent::excluir();
        if($excluir!=""){
            $this->index($excluir);
        }else{
            redirect("usuarios");
        }
        
        
    }
    
    
    /**
     * @author Lucas Leandro de Moura
     */
    public function editar() {
        $this->nivel_acesso = 9;
        $this->autentica();
        $this->setTabela("usuario");
        $this->setTitulo("Editando usuário");

        $this->db->where("id_usuario", $this->input->get("id_usuario"));
        $this->setDados($this->db->get("usuario")->result());
        $this->setAcao("usuario/cadastrar?id_usuario=".$this->input->get("id_usuario"));

        parent::cadastro();
    }
    
    
    /**
     * Abre a tela de cadastro de usuários
     */
    function cadastro(){
        
        $this->load->view("Includes/header");
        $data["erro"] = "";
        $data["dados"] = null;
        $this->load->view("usuarios/cadastro",$data);
        $this->load->view("Includes/footer");
    }
    
    
    function cadastrar(){
        
        $data["nome"] = $this->input->post("nome");
        $data["login"] = $this->input->post("login");
        $data["senha"] = $this->input->post("senha");
        $data["email"] = $this->input->post("email");
        
        $dados["dados"]["nome"] = $this->input->post("nome");
        $dados["dados"]["login"] = $this->input->post("login");
        $dados["dados"]["senha"] = $this->input->post("senha");
        $dados["dados"]["email"] = $this->input->post("email");
        
        $dados["erro"] = "";
        
        //Avalia regras
        $this->db->select("COUNT(*) as contagem");
        $this->db->where("login",$data["login"]);
        if($this->db->get("usuario")->result()[0]->contagem>0){
            $dados["erro"] = 1;
        }
        
        $this->db->select("COUNT(*) as contagem");
        $this->db->where("email",$data["email"]);
        if($this->db->get("usuario")->result()[0]->contagem>0){
            $dados["erro"] = 2;
        }
        
        
        if($dados["erro"]!=""){
            $this->load->view("Includes/header");
            $this->load->view("usuarios/cadastro",$dados);
            $this->load->view("Includes/footer");
        }else{
            //Faz a inclusão
            $edicao = false;
            if($this->input->get()){
                $indices = $this->input->get();

                foreach ($indices as $key => $valor) {
                    $this->db->where($key,$valor);            
                    $edicao = true;
                }
            }
            if($edicao==false){
            $this->db->insert("usuario",$data);
            }
            else{
                $this->db->update("usuario",$data);
            }
            $mensagem = "Olá ".$data["nome"]." (".$data["login"].")! "
                    . "<br>Seu acesso ao site ". base_url()." foi criado com sucesso!<br> "
                    . "<br>Login: ".$data["login"].""
                    . "<br>Senha: ".$data["senha"];
            
            $this->enviar_email($data["email"], "Cadastro realizado com sucesso", $mensagem);
            redirect("Login/3");
        }
    } 
    
    
          
    /**
     * Responsável por editar o cadastro do usuário logado
     */
    function meus_dados(){
        $this->autentica();
        $this->setTabela("usuario");
        $this->setTitulo("Atualize o seu cadastro");
        $id_usuario = $this->session->userdata('id_usuario');
        $this->setAcao("usuarios/atualizar_dados?id_usuario=".$id_usuario);
        $this->setMenu(true);
        
       
        $this->db->where("id_usuario",$id_usuario);
        $this->setDados($this->db->get("usuario")->result());
        
        parent::cadastro();
    }
    
    
    public function atualizar_dados() {
        $this->autentica();
        $this->setTabela("usuario");
        parent::cadastrar();
        redirect("home");
    }
    
    
    
   
}
            

        

