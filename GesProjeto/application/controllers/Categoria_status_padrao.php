<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Lucas Leandro de Moura
 */
//Deve mudar o nome da classe para o nome do arquivo
class Categoria_status_padrao extends MY_Controller {

    protected $tabela = "categoria_status_padrao";
    private $nome_classe = "categoria_status_padrao";

    //Tela inicial
    public function index() {
        $this->autentica();
        // Definindo o título da janela
        $this->setTitulo("Categoria de Status");
        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Descrição"),
            array("titulo" => "Categoria"),
            array("titulo" => "% Concluído"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
        $this->db->select("categoria_projeto.descricao as categoria, categoria_projeto.codigo_empresa, categoria_status_padrao.*");
        $this->db->join("categoria_projeto", "categoria_status_padrao.codigo_categoria = categoria_projeto.id_categoria_projeto", "inner");
        $this->db->where($this->getEmpresasComAcessos("categoria_projeto.codigo_empresa"));

        //Aplica valores do filtro
        foreach ($this->session->userdata("filtros")[$this->tabela] as $key => $value) {
            if ($value != "") {
                $this->db->where($this->tabela . "." . $key, $value);
            }
        }


        $resultados = $this->db->get($this->tabela)->result();
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            //Cria um indice
            $indices = array(array("id_status_projeto" => $item->id_status_projeto));

            //Seta as permissões para a tabela
            $botoes = "";
            //if ($item->usuario_administrador == $this->codigo_usuario) {
            $botoes = L_Deletar($indices, $this->nome_classe . "/excluir") . " " .
                    L_Editar($indices, $this->nome_classe . "/editar");
            //}
            //Carrega as informações que irá na tabela
            $dados[]["dados"] = array(
                $item->id_status_projeto,
                $item->descricao,
                $item->categoria,
                $item->percentual_concluido,
                $botoes
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::index($indices);
    }

    /**
     * Tela de cadastro
     */
    public function cadastro() {
        $this->autentica();

        //Titulo da tela
        $this->setTitulo("Cadastro de Categoria de Status");
        //Controlar que esta a operação de gravar
        $this->setAcao($this->nome_classe . "/cadastrar");

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

    /**
     * Ação de cadastrar
     */
    public function cadastrar() {
        $this->autentica();

        parent::cadastrar();
        //Redireciona
        redirect($this->nome_classe);
    }

    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->autentica();
        $this->setTitulo("Cadastro de Categoria de Status");

        $this->db->where("id_status_projeto", $this->input->get("id_status_projeto"));
        $this->setDados($this->db->get($this->tabela)->result());
        $this->setAcao($this->nome_classe . "/cadastrar?id_status_projeto=" . $this->input->get("id_status_projeto"));

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

    public function excluir() {
        $this->autentica();
        $this->setTabela($this->tabela);
        parent::excluir();
        redirect($this->nome_classe);
    }

}
