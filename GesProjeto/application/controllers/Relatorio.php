<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio extends MY_Controller {

    /**
     * Teste
     * @author Lucas Moura <lmoura@universo.univates.br>
     * será acessado pelo seguinte endereço http://angra.me/Relatorio/nome_relatorio/1/2
     * @param type $indice
     */
    public function Nome_Relatorio($indice, $indice2) {
        /**
            //Valida sessão
            $this->autentica();

            //$SQL = "SUA QUERY WHERE coluna = ".$indice." and coluna2 = ".$indice2;
            //Executa o SQL e armazena na variavel $resultado
            //$resultados = $this->db->query($SQL)->result();
            //Percorre o resultado
            //foreach ($resultados as $registro){
            //print $registro->nome_campo;
            //}
            //$conteudo = "<html>";
            //Mesma coisa no java $conteudo+="";
            //$conteudo.= "<body>";
            //$conteudo.= "....teste taetaetaeate";
            //$conteudo.=" jkltaejklaet ".$resultados[0]->coluna;
            //Gera o PDF
            //$this->geraPDF("NomeRelatorio_".date("Ymd"), $conteudo);
         */
    }

    /**
     * Relatório: Termo de Abertura de Projeto
     * @author Sérgio Luis Saibel <sergio.saibel@universo.univates.br>
     * será acessado pelo seguinte endereço http://angra.me/Relatorio/termo_abertura_projeto/n
     * @param type $indice 0 = todos
     */
    public function termo_abertura_projeto($cod_projeto) {
        //Valida sessão
        $this->autentica();

        $SQL = "SELECT view_termo_abertura_projeto.cod_projeto,
                       view_termo_abertura_projeto.titulo, 
                       view_termo_abertura_projeto.gerente,
                       view_termo_abertura_projeto.empresa,
                       view_termo_abertura_projeto.setor,
                       view_termo_abertura_projeto.categoria,
                       view_termo_abertura_projeto.orcamento,
                       view_termo_abertura_projeto.cadastro,
                       view_termo_abertura_projeto.prev_ini,
                       view_termo_abertura_projeto.prev_fin,
                       view_termo_abertura_projeto.descricao, 
                       view_termo_abertura_projeto.observacoes,
                       view_termo_abertura_projeto.escopo
                  FROM view_termo_abertura_projeto 
                 WHERE view_termo_abertura_projeto.cod_projeto IS NOT NULL";

        // 0 = todos
        IF ($cod_projeto != 0) {
            $SQL .= " AND view_termo_abertura_projeto.cod_projeto = " . $cod_projeto;
        }

        //Executa o SQL e armazena na variavel $resultado
        $resultados = $this->db->query($SQL)->result();

        if (!empty($resultados)) {
            $conteudo = "<html>";
            $conteudo .= "  <!DOCTYPE html>";
            $conteudo .= "  <head>";
            $conteudo .= "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";
            $conteudo .= "    <title>Relatório de Custos do Projeto</title>";
            $conteudo .= "";
            $conteudo .= "    <!-- Estilos da página -->";
            $conteudo .= "    <style type=\"text/css\">";
            $conteudo .= "      /* Inicializando página de layout para impressão */";
            $conteudo .= "      * {";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      body {";
            $conteudo .= "        margin: auto;";
            $conteudo .= "        align: center;";
            $conteudo .= "        line-height: 1.4em;";
            $conteudo .= "        font: 10pt Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        color: #000;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #site_content{";
            $conteudo .= "        /* Definimos a dimansão da página */";
            $conteudo .= "        min-width: 700px;";
            $conteudo .= "        max-width: 2000px;";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 100%;";
            $conteudo .= "";
            $conteudo .= "        /* Posição da página centralizada */";
            $conteudo .= "        margin-top: 0;";
            $conteudo .= "        margin-left: auto;";
            $conteudo .= "        margin-bottom: 0;";
            $conteudo .= "        margin-right: auto;";
            $conteudo .= "";
            $conteudo .= "        text-align: center;";
            $conteudo .= "";
            $conteudo .= "        /* Limpando a página */";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 80%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content-both{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 33%;";
            $conteudo .= "        float: top;";
            $conteudo .= "        position: relative;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table{";
            $conteudo .= "        min-width: 100%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table tr td{";
            $conteudo .= "        min-width: 8.333333%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .title{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 10px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .info-right{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: right;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .label{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .field{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        margin-top: 5px;";
            $conteudo .= "        border-top: 0.5px solid #D9D5CF;";
            $conteudo .= "        border-bottom: 1px solid #000000;";
            $conteudo .= "        padding: 4px 4px;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator_ {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        border-bottom: 0.5px dashed #D9D5CF;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      p a {";
            $conteudo .= "        word-wrap: break-word;";
            $conteudo .= "      }";
            $conteudo .= "    </style>";
            $conteudo .= "  </head>";
            $conteudo .= "";
            $conteudo .= "  <body>";
            $conteudo .= "    <!-- site_content -->";
            $conteudo .= "    <div id=\"site_content\">";
            $conteudo .= "";
            $conteudo .= "      <!-- content -->";
            $conteudo .= "      <div id=\"content\">";
            $conteudo .= "        <!-- content-both -->";
            $conteudo .= "        <div id=\"content-both\">";
            $conteudo .= "          <!-- form_settings -->";
            $conteudo .= "          <div class=\"form_settings\">";
            $conteudo .= "            <!-- Cabeçalho da tabela -->";


            // Montando informação de cabeçalho do relatório
            IF ($cod_projeto != 0) {
                $conteudo .= "            <p class=\"info-right\">Relatório do Projeto: " . $cod_projeto . " - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            } ELSE {
                $conteudo .= "            <p class=\"info-right\">Relatório de Projetos - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            }

            // Percorrendo "resultados" e mostrando todos os registros
            foreach ($resultados as $item) {
                /**
                 * Abertura do relatório
                 */
                $conteudo .= "            <table id=\"layout\" class=\"separator\">";
                $conteudo .= "              <!-- Corpo da tabela -->";
                $conteudo .= "";
                $conteudo .= "              <tr>";
                $conteudo .= "                <td colspan=\"12\">";
                $conteudo .= "                  <!-- Tabela para informaçoes a respeito dos termos de abertura de projeto -->";
                $conteudo .= "                  <table id=\"layout_termo-abertura-projeto\" class=\"separator_\">";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Termo de Abertura do Projeto</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Projeto: " . $item->cod_projeto . "</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Título: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->titulo . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Gerente: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->gerente . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Empresa: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->empresa . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Setor: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item->setor . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Categoria: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item->categoria . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Orçamento Inicial: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item->orcamento . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Data de Cadastro: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item->cadastro . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Início Previsto: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item->prev_ini . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Término Previsto: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item->prev_fin . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Descrição do Projeto</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"field\" colspan=\"12\"><p>" . $item->descricao . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Observações</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"field\" colspan=\"12\"><p>" . $item->observacoes . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Escopo</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"field\" colspan=\"12\"><p>" . $item->escopo . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                  </table> <!-- Tabela para informaçoes a respeito dos termos de abertura de projeto -->";
                $conteudo .= "                </td>";
                $conteudo .= "              </tr>";
                $conteudo .= "";


                $this->db->select("usuario.nome || ' - ' || usuario.email AS usuario, projeto_membros.nivel");
                $this->db->join("usuario", "projeto_membros.cod_usuario=usuario.id_usuario", "inner");
                $this->db->where("projeto_membros.cod_projeto", $item->cod_projeto);
                $resultados_membros = $this->db->get("projeto_membros")->result();

                $conteudo .= "              <tr>";
                $conteudo .= "                <td colspan=\"12\">";
                $conteudo .= "                  <!-- Tabela para informaçoes a respeito dos termos de abertura de projeto -->";
                $conteudo .= "                  <table id=\"layout_termo-abertura-usuario-projeto\" class=\"separator_\">";

                // Percorrendo "resultados" e mostrando todos os registros
                foreach ($resultados_membros as $item_membro) {
                    $conteudo .= "                    <tr>";
                    $conteudo .= "                      <td class=\"label\" colspan=\"2\">Membro:</td>";
                    $conteudo .= "                      <td class=\"field\" colspan=\"8\">" . $item_membro->usuario . "</td>";
                    $conteudo .= "                      <td class=\"label\" colspan=\"1\">Nível no Projeto:</td>";
                    $conteudo .= "                      <td class=\"field\" colspan=\"1\">" . $item_membro->nivel . "</td>";
                    $conteudo .= "                    </tr>";
                }
                $conteudo .= "                  </table> <!-- Tabela para informaçoes a respeito dos termos de abertura de projeto -->";
                $conteudo .= "                </td>";
                $conteudo .= "              </tr>";
                $conteudo .= "";
                $conteudo .= "            </table> <!-- Tabela de layout -->";
            }

            /**
             * Fechamento do relatório
             */
            $conteudo .= "";
            $conteudo .= "            </table> <!-- Tabela de layout -->";
        }

        $conteudo .= "            <p class=\"info-right\">Fim do arquivo</p>";
        $conteudo .= "          </div> <!-- form_settings -->";
        $conteudo .= "        </div> <!-- content-both -->";
        $conteudo .= "      </div> <!-- content -->";
        $conteudo .= "";
        $conteudo .= "    </div> <!-- site_content -->";
        $conteudo .= "  </body>";
        $conteudo .= "</html>";

        //$conteudo.=" jkltaejklaet ".$resultados[0]->coluna;
        //Gera o PDF
        $this->geraPDF("termo_abertura_projeto_" . date("Ymd"), $conteudo);
    }

    /**
     * Relatório: Fases do Projeto
     * @author Sérgio Luis Saibel <sergio.saibel@universo.univates.br>
     * será acessado pelo seguinte endereço http://angra.me/Relatorio/fases_projeto/n/n/n
     * @param type $cod_projeto Código do projeto 0 = todos
     * @param type $ordem_fase Ordem da fase que se deseja imprimir 0 = todas
     * @param type $cod_situacao Código da situação das tarefas: 
     *  0 = todas, 1 = atasadas, 2 = adiantadas, 3 = no prazo, 4 = concluídas
     */
    public function fases_projeto($cod_projeto, $ordem_fase, $cod_situacao) {
        //Valida sessão
        $this->autentica();

        $SQL = "SELECT
                    view_fases_projeto.projeto1_id_projeto, 
                    view_fases_projeto.projeto_fases1_ordem_fase,
                    view_fases_projeto.projeto_tarefa_data_previsto_inicial, 
                    view_fases_projeto.projeto_tarefa_data_previsto_final, 
                    view_fases_projeto.projeto_tarefa_data_real_inicial, 
                    view_fases_projeto.projeto_tarefa_data_real_final
                  FROM view_fases_projeto
                 WHERE view_fases_projeto.projeto1_id_projeto IS NOT NULL";

        // 0 = todos
        IF ($cod_projeto != 0) {
            $SQL .= " AND view_fases_projeto.projeto1_id_projeto = " . $cod_projeto;
        }

        // 0 = todas
        IF ($ordem_fase != 0) {
            $SQL .= " AND view_fases_projeto.projeto_fases1_ordem_fase = " . $ordem_fase;
        }

        // O = todos
        IF ($cod_situacao != 0) {
            switch ($cod_situacao) {
                case 1: // 1 = atasadas
                    $SQL .= " AND (((view_fases_projeto.projeto_tarefa_data_real_final IS NULL) AND (view_fases_projeto.projeto_tarefa_data_previsto_final < now())) OR (view_fases_projeto.projeto_tarefa_data_real_final > view_fases_projeto.projeto_tarefa_data_previsto_final))";
                    break;
                case 2: // 2 = adiantada
                    $SQL .= " AND (((view_fases_projeto.projeto_tarefa_data_real_inicial <= view_fases_projeto.projeto_tarefa_data_previsto_inicial) AND (view_fases_projeto.projeto_tarefa_data_previsto_final > now())) OR (view_fases_projeto.projeto_tarefa_data_real_final < view_fases_projeto.projeto_tarefa_data_previsto_final))";
                    break;
                case 3: // 3 = no prazo
                    $SQL .= " AND ((view_fases_projeto.projeto_tarefa_data_real_inicial IS NOT NULL) AND (view_fases_projeto.projeto_tarefa_data_real_final IS NULL AND view_fases_projeto.projeto_tarefa_data_previsto_final > now()))";
                    break;
                case 4: // 4 = concluídas
                    $SQL .= " AND ((view_fases_projeto.projeto_tarefa_data_real_inicial IS NOT NULL) AND (view_fases_projeto.projeto_tarefa_data_real_final IS NOT NULL))";
                    break;
            }
        }

        //Executa o SQL e armazena na variavel $resultado
        $resultados = $this->db->query($SQL)->result();

        if (!empty($resultados)) {
            $conteudo = "<html>";
            $conteudo .= "  <!DOCTYPE html>";
            $conteudo .= "  <head>";
            $conteudo .= "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";
            $conteudo .= "    <title>Relatório de Fases do Projeto</title>";
            $conteudo .= "";
            $conteudo .= "    <!-- Estilos da página -->";
            $conteudo .= "    <style type=\"text/css\">";
            $conteudo .= "      /* Inicializando página de layout para impressão */";
            $conteudo .= "      * {";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      body {";
            $conteudo .= "        margin: auto;";
            $conteudo .= "        align: center;";
            $conteudo .= "        line-height: 1.4em;";
            $conteudo .= "        font: 10pt Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        color: #000;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #site_content{";
            $conteudo .= "        /* Definimos a dimansão da página */";
            $conteudo .= "        min-width: 700px;";
            $conteudo .= "        max-width: 2000px;";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 100%;";
            $conteudo .= "";
            $conteudo .= "        /* Posição da página centralizada */";
            $conteudo .= "        margin-top: 0;";
            $conteudo .= "        margin-left: auto;";
            $conteudo .= "        margin-bottom: 0;";
            $conteudo .= "        margin-right: auto;";
            $conteudo .= "";
            $conteudo .= "        text-align: center;";
            $conteudo .= "";
            $conteudo .= "        /* Limpando a página */";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 80%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content-both{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 33%;";
            $conteudo .= "        float: top;";
            $conteudo .= "        position: relative;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table{";
            $conteudo .= "        min-width: 100%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table tr td{";
            $conteudo .= "        min-width: 8.333333%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .title{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 10px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .info-right{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: right;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .label{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .field{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        margin-top: 5px;";
            $conteudo .= "        border-top: 0.5px solid #D9D5CF;";
            $conteudo .= "        border-bottom: 1px solid #000000;";
            $conteudo .= "        padding: 4px 4px;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator_ {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        border-bottom: 0.5px dashed #D9D5CF;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      p a {";
            $conteudo .= "        word-wrap: break-word;";
            $conteudo .= "      }";
            $conteudo .= "    </style>";
            $conteudo .= "  </head>";
            $conteudo .= "";
            $conteudo .= "  <body>";
            $conteudo .= "    <!-- site_content -->";
            $conteudo .= "    <div id=\"site_content\">";
            $conteudo .= "";
            $conteudo .= "      <!-- content -->";
            $conteudo .= "      <div id=\"content\">";
            $conteudo .= "        <!-- content-both -->";
            $conteudo .= "        <div id=\"content-both\">";
            $conteudo .= "          <!-- form_settings -->";
            $conteudo .= "          <div class=\"form_settings\">";
            $conteudo .= "            <!-- Cabeçalho da tabela -->";

            // Montando informação de cabeçalho do relatório
            IF ($cod_projeto != 0) {
                $conteudo .= "            <p class=\"info-right\">Relatório do Projeto: " . $cod_projeto . " - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            } ELSE {
                $conteudo .= "            <p class=\"info-right\">Relatório de Projetos - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            }

            // O = todos
            IF ($cod_situacao != 0) {
                switch ($cod_situacao) {
                    case 1: // 1 = atasadas
                        $conteudo .= "            <p class=\"info-right\">Fases de Projetos - EM ATRASO</p>";
                        break;
                    case 2: // 2 = adiantada
                        $conteudo .= "            <p class=\"info-right\">Fases de Projetos - ADIANTADAS</p>";
                        break;
                    case 3: // 3 = no prazo
                        $conteudo .= "            <p class=\"info-right\">Fases de Projetos - NO PRAZO</p>";
                        break;
                    case 4: // 4 = concluídas
                        $conteudo .= "            <p class=\"info-right\">Fases de Projetos - CONCLUÍDAS</p>";
                        break;
                }
            }

            // Percorrendo "resultados" e mostrando todos os registros
            foreach ($resultados as $item) {
                /**
                 * Abertura do relatório
                 */
                $conteudo .= "            <table id=\"layout\" class=\"separator\">";
                $conteudo .= "              <!-- Corpo da tabela -->";
                $conteudo .= "              <tr>";
                $conteudo .= "                <td class=\"title\" colspan=\"12\">Fase: " . $item->projeto_fases1_ordem_fase . " - Projeto: " . $item->projeto1_id_projeto . "</td>";
                $conteudo .= "              </tr>";
                $conteudo .= "";


                /**
                 * Custos do Projeto
                 */
                $SQL_FASES = "SELECT view_fases_projeto.projeto1_id_projeto || ' - ' || view_fases_projeto.projeto1_descricao_resumida AS projeto, 
                                      view_fases_projeto.projeto_fases1_ordem_fase || ' - ' || view_fases_projeto.projeto_fases1_descricao AS fase,
                                      view_fases_projeto.projeto_tarefa_id_tarefa || ' - ' || view_fases_projeto.projeto_tarefa_titulo AS tarefa, 
                                      view_fases_projeto.projeto_tarefa_progresso, 
                                      view_fases_projeto.projeto_tarefa_resolvido, 
                                      view_fases_projeto.projeto_tarefa_descricao, 
                                      view_fases_projeto.projeto_tarefa_data_inclusao, 
                                      view_fases_projeto.usuario1_nome || ' - ' || view_fases_projeto.usuario1_email || ', ' || view_fases_projeto.usuario1_nivel_usuario AS responsavel
                                 FROM view_fases_projeto
                                WHERE view_fases_projeto.projeto1_id_projeto = " . $item->projeto1_id_projeto .
                        "         AND view_fases_projeto.projeto_fases1_ordem_fase = " . $item->projeto_fases1_ordem_fase .
                        "       ORDER BY view_fases_projeto.projeto1_id_projeto, view_fases_projeto.projeto_fases1_ordem_fase";

                //Executa o SQL e armazena na variavel $resultados_membros
                $resultados_fase = $this->db->query($SQL_FASES)->result();

                if (!empty($resultados_fase)) {
                    $conteudo .= "              <tr>";
                    $conteudo .= "                <td colspan=\"12\">";
                    $conteudo .= "                  <!-- Tabela para informaçoes complementares a respeito das fases do projeto -->";
                    $conteudo .= "                  <table id=\"layout_fase-projeto\" class=\"separator_\">";

                    // Percorrendo "resultados" e mostrando todos os registros
                    foreach ($resultados_fase as $item_fase) {
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"12\">Tarefa: " . $item_fase->tarefa . "</td>";
                        $conteudo .= "                    </tr>";                       
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Data Ini. Prev.: </td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item->projeto_tarefa_data_previsto_inicial . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Data Fin. Prev.: </td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item->projeto_tarefa_data_previsto_final . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Data Ini. Real: </td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item->projeto_tarefa_data_real_inicial . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Data Fin. Real: </td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item->projeto_tarefa_data_real_final . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"separator_\" colspan=\"12\"></td>";
                        $conteudo .= "                    </tr>";  
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Data Inclusão: </td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item_fase->projeto_tarefa_data_inclusao . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Progresso: </td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_fase->projeto_tarefa_progresso . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Resolvido: </td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_fase->projeto_tarefa_resolvido . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Responsável</td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item_fase->responsavel . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\">Descrição:</td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item_fase->projeto_tarefa_descricao . "</p></td>";
                        $conteudo .= "                    </tr>";
                    }

                    $conteudo .= "                  </table> <!-- Tabela para informaçoes complementares a respeito dos custos do projeto -->";
                    $conteudo .= "                </td>";
                    $conteudo .= "              </tr>";
                    $conteudo .= "";
                }

                /**
                 * Fechamento do relatório
                 */
                $conteudo .= "";
                $conteudo .= "            </table> <!-- Tabela de layout -->";
            }

            $conteudo .= "            <p class=\"info-right\">Fim do arquivo</p>";
            $conteudo .= "          </div> <!-- form_settings -->";
            $conteudo .= "        </div> <!-- content-both -->";
            $conteudo .= "      </div> <!-- content -->";
            $conteudo .= "";
            $conteudo .= "    </div> <!-- site_content -->";
            $conteudo .= "  </body>";
            $conteudo .= "</html>";

            //$conteudo.=" jkltaejklaet ".$resultados[0]->coluna;
            //Gera o PDF
            $this->geraPDF("fases_projeto_" . date("Ymd"), $conteudo);
        }
    }

    /**
     * Relatório: Custos do Projeto
     * @author Sérgio Luis Saibel <sergio.saibel@universo.univates.br>
     * será acessado pelo seguinte endereço http://angra.me/Relatorio/custos_projeto/n/n/n
     * @param type $cod_projeto Código do projeto 0 = todos
     * @param type $mes Mês que ocorreu o lançamento 0 = todos
     * @param type $ano Ano que ocorreu o lançamento 0 = todos
     */
    public function custos_projeto($cod_projeto, $mes, $ano) {
        //Valida sessão
        $this->autentica();

        // Seleciona os projetos
        $SQL = "SELECT view_custos.mes,
                       view_custos.ano,
                       view_custos.cod_projeto,
                       view_custos.projeto,
                       view_custos.id_tarefa
                  FROM view_custos
                 WHERE view_custos.cod_projeto IS NOT NULL";

        // 0 = todos
        IF ($cod_projeto != 0) {
            $SQL .= " AND view_custos.cod_projeto = " . $cod_projeto;
        }

        // 0 = todos
        IF ($mes != 0) {
            $SQL .= " AND view_custos.mes = " . $mes;
        }

        // 0 = todos
        IF ($ano != 0) {
            $SQL .= " AND view_custos.ano = " . $ano;
        }

        $SQL .= " ORDER BY view_custos.cod_projeto, view_custos.mes DESC, view_custos.ano DESC";

        //Executa o SQL e armazena na variavel $resultado
        $resultados = $this->db->query($SQL)->result();

        if (!empty($resultados)) {
            $conteudo = "<html>";
            $conteudo .= "  <!DOCTYPE html>";
            $conteudo .= "  <head>";
            $conteudo .= "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";
            $conteudo .= "    <title>Relatório de Custos do Projeto</title>";
            $conteudo .= "";
            $conteudo .= "    <!-- Estilos da página -->";
            $conteudo .= "    <style type=\"text/css\">";
            $conteudo .= "      /* Inicializando página de layout para impressão */";
            $conteudo .= "      * {";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      body {";
            $conteudo .= "        margin: auto;";
            $conteudo .= "        align: center;";
            $conteudo .= "        line-height: 1.4em;";
            $conteudo .= "        font: 10pt Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        color: #000;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #site_content{";
            $conteudo .= "        /* Definimos a dimansão da página */";
            $conteudo .= "        min-width: 700px;";
            $conteudo .= "        max-width: 2000px;";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 100%;";
            $conteudo .= "";
            $conteudo .= "        /* Posição da página centralizada */";
            $conteudo .= "        margin-top: 0;";
            $conteudo .= "        margin-left: auto;";
            $conteudo .= "        margin-bottom: 0;";
            $conteudo .= "        margin-right: auto;";
            $conteudo .= "";
            $conteudo .= "        text-align: center;";
            $conteudo .= "";
            $conteudo .= "        /* Limpando a página */";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 80%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content-both{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 33%;";
            $conteudo .= "        float: top;";
            $conteudo .= "        position: relative;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table{";
            $conteudo .= "        min-width: 100%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table tr td{";
            $conteudo .= "        min-width: 8.333333%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .title{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 10px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .info-right{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: right;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .label{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .field{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        margin-top: 5px;";
            $conteudo .= "        border-top: 0.5px solid #D9D5CF;";
            $conteudo .= "        border-bottom: 1px solid #000000;";
            $conteudo .= "        padding: 4px 4px;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator_ {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        border-bottom: 0.5px dashed #D9D5CF;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      p a {";
            $conteudo .= "        word-wrap: break-word;";
            $conteudo .= "      }";
            $conteudo .= "    </style>";
            $conteudo .= "  </head>";
            $conteudo .= "";
            $conteudo .= "  <body>";
            $conteudo .= "    <!-- site_content -->";
            $conteudo .= "    <div id=\"site_content\">";
            $conteudo .= "";
            $conteudo .= "      <!-- content -->";
            $conteudo .= "      <div id=\"content\">";
            $conteudo .= "        <!-- content-both -->";
            $conteudo .= "        <div id=\"content-both\">";
            $conteudo .= "          <!-- form_settings -->";
            $conteudo .= "          <div class=\"form_settings\">";
            $conteudo .= "            <!-- Cabeçalho da tabela -->";


            // Montando informação de cabeçalho do relatório
            IF ($cod_projeto != 0) {
                $conteudo .= "            <p class=\"info-right\">Relatório do Projeto: " . $cod_projeto . " - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            } ELSE {
                $conteudo .= "            <p class=\"info-right\">Relatório de Projetos - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            }

            // 0 = todos
            IF ($mes != 0) {
                $conteudo .= "            <p class=\"info-right\">Relatório de Custos MENSAL</p>";
            }

            // 0 = todos
            IF ($ano != 0) {
                $conteudo .= "            <p class=\"info-right\">Relatório de Custos ANUAL</p>";
            }

            // Percorrendo "resultados" e mostrando todos os registros
            foreach ($resultados as $item) {
                /**
                 * Abertura do relatório
                 */
                $conteudo .= "            <table id=\"layout\" class=\"separator\">";
                $conteudo .= "              <!-- Corpo da tabela -->";
                $conteudo .= "";


                /**
                 * Custos do Projeto
                 */
                $SQL_CUSTOS = "SELECT view_custos.data_lancamento,
                                      view_custos.descricao,
                                      view_custos.valor_lancamento,
                                      view_custos.id_tarefa|| ' - ' || view_custos.nome_tarefa AS tarefa,
                                      view_custos.responsavel
                                 FROM view_custos
                                WHERE view_custos.cod_projeto = " . $item->cod_projeto .
                        "         AND view_custos.id_tarefa = " . $item->id_tarefa .
                        "         AND view_custos.mes = '" . $item->mes . "'" .
                        "         AND view_custos.ano = '" . $item->ano . "'" .
                        "       ORDER BY view_custos.cod_projeto, view_custos.mes DESC, view_custos.ano DESC";

                //Executa o SQL e armazena na variavel $resultados_membros
                $resultados_custo = $this->db->query($SQL_CUSTOS)->result();

                if (!empty($resultados_custo)) {
                    $conteudo .= "              <tr>";
                    $conteudo .= "                <td colspan=\"12\">";
                    $conteudo .= "                  <!-- Tabela para informaçoes complementares a respeito dos custos do projeto -->";
                    $conteudo .= "                  <table id=\"layout_custo-tarefa-projeto\" class=\"separator_\">";
                    $conteudo .= "                    <tr>";
                    $conteudo .= "                      <td class=\"title\" colspan=\"12\">Custos - Projeto: " . $item->cod_projeto . "</td>";
                    $conteudo .= "                    </tr>";

                    // Percorrendo "resultados" e mostrando todos os registros
                    foreach ($resultados_custo as $item_custos) {
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"3\">Lançamento</td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"9\"><p>" . $item_custos->data_lancamento . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"3\">Vlr. Lançamento</td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"9\"><p>" . $item_custos->valor_lancamento . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"3\">Descrição</td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"9\"><p>" . $item_custos->descricao . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"3\">Tarefa</td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"9\"><p>" . $item_custos->tarefa . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"3\">Responsável</td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"9\"><p>" . $item_custos->responsavel . "</p></td>";
                        $conteudo .= "                    </tr>";
                    }

                    $conteudo .= "                  </table> <!-- Tabela para informaçoes complementares a respeito dos custos do projeto -->";
                    $conteudo .= "                </td>";
                    $conteudo .= "              </tr>";
                    $conteudo .= "";
                }

                /**
                 * Fechamento do relatório
                 */
                $conteudo .= "";
                $conteudo .= "            </table> <!-- Tabela de layout -->";
            }

            $conteudo .= "            <p class=\"info-right\">Fim do arquivo</p>";
            $conteudo .= "          </div> <!-- form_settings -->";
            $conteudo .= "        </div> <!-- content-both -->";
            $conteudo .= "      </div> <!-- content -->";
            $conteudo .= "";
            $conteudo .= "    </div> <!-- site_content -->";
            $conteudo .= "  </body>";
            $conteudo .= "</html>";

            //$conteudo.=" jkltaejklaet ".$resultados[0]->coluna;
            //Gera o PDF
            $this->geraPDF("custos_projeto_" . date("Ymd"), $conteudo);
        }
    }
    
    /**
     * Relatório que exibirá todas as tarefas e seus status
     * @author Lucas Leandro de Moura <lmoura@universo.univates.br
     * @param type $cod_projeto
     */
    public function tarefas_projeto($indice){
        $this->autentica();
        
        
           //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Titulo"),
            array("titulo" => "Responsável"),
            array("titulo" => "Fase"),
            array("titulo" => "Progresso"),
            array("titulo" => "Resolvido")
        );
        
        
                $dados = array();
        //Seleciona todos os registros da tabela
       $this->db->select("projeto_tarefa.*,usuario.nome as nome_responsavel,projeto_fases.descricao as descricao_fase");
       $this->db->where("cod_projeto",$indice);
       $this->db->join("usuario","usuario.id_usuario = projeto_tarefa.responsavel","inner");
       $this->db->join("projeto_fases","projeto_fases.id_fase_projeto = projeto_tarefa.fase_atual","inner");
       
       $resultados = $this->db->get("projeto_tarefa")->result();
       
       
       
        foreach ($resultados as $item) {
            
            $dados[]["dados"] = array(
                $item->id_tarefa,
                $item->titulo,
                $item->nome_responsavel,
                $item->descricao_fase,
                $item->progresso."%",
                L_boolean($item->resolvido)
            );
        }
        $this->db->where("id_projeto",$indice);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;
        
        $conteudo.= $this->cabecalho("Relação de tarefas do projeto ".$descricao_resumida);
              
        $conteudo.=$this->table($cabecalho, $dados);
        $conteudo.= $this->rodape();
        $this->geraPDF("relatorio_tarefas.pdf", $conteudo);
    }
    
    private function cabecalho($titulo){
        $conteudo = "<b>".$titulo."</b>";
          $conteudo.='<style>
#dataTables-dados {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#dataTables-dados td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#dataTables-dados tr:nth-child(even){background-color: #c0c0c0;}

#dataTables-dados tr:hover {background-color: #ddd;}

#dataTables-dados th {
    padding-top: 9px;
    padding-bottom: 9px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>'; 
    
        return $conteudo;
    }
    
    
    
    private function rodape(){
        $conteudo = "<p style='color: #000000;
                        font-family: arial;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 10px;'><b>Emitido por: ".$this->session->userdata("nome")." às ".date("d/m/Y H:i:s")." | ". base_url()."</b></p>";
         
    
        return $conteudo;
    }

    /**
     * Relatório: Projeto
     * @author Sérgio Luis Saibel <sergio.saibel@universo.univates.br>
     * será acessado pelo seguinte endereço http://angra.me/Relatorio/projetos/n
     * @param type $cod_projeto Código do projeto 0 = todos
     */
    public function projetos($cod_projeto) {
        // Valida sessão
        $this->autentica();

        $SQL = "SELECT view_projetos.projeto_id_projeto, 
                       view_projetos.projeto_id_projeto ||' - '|| view_projetos.projeto_descricao_resumida AS projeto,
                       view_projetos.projeto_data_inclusao, 
                       view_projetos.projeto_descricao,
                       view_projetos.projeto_observacoes, 
                       view_projetos.projeto_custo_previsto,
                       view_projetos.projeto_concluido,
                       view_projetos.projeto_cod_usuario_chefe ||' - '|| view_projetos.usuario1_nome ||', '|| view_projetos.usuario1_email AS usuario_projeto, 
                       view_projetos.usuario1_nivel_usuario AS nivel_usuario_projeto,
                       view_projetos.projeto_cod_setor ||' - '|| view_projetos.empresa_setores1_descricao_resumida AS projeto_setor, 
                       view_projetos.empresa_setores1_cod_usuario_chefe ||' - '|| view_projetos.usuario2_nome ||', '|| view_projetos.usuario2_email AS chefe_setor, 
                       view_projetos.usuario2_nivel_usuario AS nivel_chefe_setor,
                       view_projetos.empresa_setores1_codigo_empresa ||' - '|| view_projetos.empresa1_nome_empresa ||', CNPJ: '|| view_projetos.empresa1_cnpj AS empresa_cliente, 
                       view_projetos.empresa1_usuario_administrador ||' - '|| view_projetos.usuario3_nome ||', '|| view_projetos.usuario3_email AS administrador_cliente,
                       view_projetos.usuario3_nivel_usuario AS nivel_administrador_cliente,
                       view_projetos.empresa1_codigo_cidade ||' - '|| view_projetos.cidade1_descricao_resumida ||', '|| view_projetos.estado1_descricao ||' - '|| view_projetos.estado1_sgl_estado ||', '|| view_projetos.pais1_descricao_resumida ||' - '|| view_projetos.pais1_sigla AS endereco_cliente,
                       view_projetos.projeto_cod_categoria_projeto ||' - '|| view_projetos.categoria_projeto1_descricao AS categoria_projeto, 
                       view_projetos.categoria_projeto1_observacoes AS observacoes_categoria, 
                       view_projetos.projeto_cod_status_projeto ||' - '|| view_projetos.projeto_status1_descricao AS status_projeto,
                       view_projetos.projeto_status1_percentual_concluido AS concluido
                  FROM view_projetos
                 WHERE view_projetos.projeto_id_projeto IS NOT NULL";

        // 0 = todos
        IF ($cod_projeto != 0) {
            $SQL .= " AND view_projetos.projeto_id_projeto = " . $cod_projeto;
        }

        //Executa o SQL e armazena na variavel $resultado
        $resultados = $this->db->query($SQL)->result();

        if (!empty($resultados)) {
            $conteudo = "<html>";
            $conteudo .= "  <!DOCTYPE html>";
            $conteudo .= "  <head>";
            $conteudo .= "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>";
            $conteudo .= "    <title>Relatório de Projetos</title>";
            $conteudo .= "";
            $conteudo .= "    <!-- Estilos da página -->";
            $conteudo .= "    <style type=\"text/css\">";
            $conteudo .= "      /* Inicializando página de layout para impressão */";
            $conteudo .= "      * {";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      body {";
            $conteudo .= "        margin: auto;";
            $conteudo .= "        align: center;";
            $conteudo .= "        line-height: 1.4em;";
            $conteudo .= "        font: 10pt Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        color: #000;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #site_content{";
            $conteudo .= "        /* Definimos a dimansão da página */";
            $conteudo .= "        min-width: 700px;";
            $conteudo .= "        max-width: 2000px;";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 100%;";
            $conteudo .= "";
            $conteudo .= "        /* Posição da página centralizada */";
            $conteudo .= "        margin-top: 0;";
            $conteudo .= "        margin-left: auto;";
            $conteudo .= "        margin-bottom: 0;";
            $conteudo .= "        margin-right: auto;";
            $conteudo .= "";
            $conteudo .= "        text-align: center;";
            $conteudo .= "";
            $conteudo .= "        /* Limpando a página */";
            $conteudo .= "        background: transparent !important;";
            $conteudo .= "        color: #000 !important;";
            $conteudo .= "        text-shadow: none !important;";
            $conteudo .= "        filter: none !important;";
            $conteudo .= "        -ms-filter: none !important;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 80%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      #content-both{";
            $conteudo .= "        /* Definindo a dimansão da div */";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 33%;";
            $conteudo .= "        float: top;";
            $conteudo .= "        position: relative;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table{";
            $conteudo .= "        min-width: 100%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      table tr td{";
            $conteudo .= "        min-width: 8.333333%;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .title{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 10px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .info-right{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: right;";
            $conteudo .= "        color: #696969;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .label{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "        font-weight: bold;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .field{";
            $conteudo .= "        text-shadow: none;";
            $conteudo .= "        text-align: left;";
            $conteudo .= "        font: 8px Georgia, \"Times New Roman\", Times, serif;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        margin-top: 5px;";
            $conteudo .= "        border-top: 0.5px solid #D9D5CF;";
            $conteudo .= "        border-bottom: 1px solid #000000;";
            $conteudo .= "        padding: 4px 4px;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      .separator_ {";
            $conteudo .= "        width: 100%;";
            $conteudo .= "        height: 0;";
            $conteudo .= "        border-bottom: 0.5px dashed #D9D5CF;";
            $conteudo .= "      }";
            $conteudo .= "";
            $conteudo .= "      p a {";
            $conteudo .= "        word-wrap: break-word;";
            $conteudo .= "      }";
            $conteudo .= "    </style>";
            $conteudo .= "  </head>";
            $conteudo .= "";
            $conteudo .= "  <body>";
            $conteudo .= "    <!-- site_content -->";
            $conteudo .= "    <div id=\"site_content\">";
            $conteudo .= "";
            $conteudo .= "      <!-- content -->";
            $conteudo .= "      <div id=\"content\">";
            $conteudo .= "        <!-- content-both -->";
            $conteudo .= "        <div id=\"content-both\">";
            $conteudo .= "          <!-- form_settings -->";
            $conteudo .= "          <div class=\"form_settings\">";
            $conteudo .= "            <!-- Cabeçalho da tabela -->";


            // Montando informação de cabeçalho do relatório
            IF ($cod_projeto != 0) {
                $conteudo .= "            <p class=\"info-right\">Relatório do Projeto: " . $cod_projeto . " - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            } ELSE {
                $conteudo .= "            <p class=\"info-right\">Relatório de Projetos - " . date("l jS \of F Y \- h:i:s A") . "</p>";
            }

            // Percorrendo "resultados" e mostrando todos os registros
            foreach ($resultados as $item) {
                /**
                 * Abertura do relatório
                 */
                $conteudo .= "            <table id=\"layout\" class=\"separator\">";
                $conteudo .= "              <!-- Corpo da tabela -->";


                /**
                 * Cliente
                 *      view_projetos.projeto_cod_setor ||' - '|| view_projetos.empresa_setores1_descricao_resumida AS projeto_setor, 
                 *      view_projetos.empresa_setores1_cod_usuario_chefe ||' - '|| view_projetos.usuario2_nome ||', '|| view_projetos.usuario2_email AS chefe_setor, 
                 *      view_projetos.usuario2_nivel_usuario AS nivel_chefe_setor,
                 *      view_projetos.empresa_setores1_codigo_empresa ||' - '|| view_projetos.empresa1_nome_empresa ||', CNPJ: '|| view_projetos.empresa1_cnpj AS empresa_cliente, 
                 *      view_projetos.empresa1_usuario_administrador ||' - '|| view_projetos.usuario3_nome ||', '|| view_projetos.usuario3_email AS administrador_cliente,
                 *      view_projetos.usuario3_nivel_usuario AS nivel_administrador_cliente,
                 *      view_projetos.empresa1_codigo_cidade ||' - '|| view_projetos.cidade1_descricao_resumida ||', '|| view_projetos.estado1_descricao ||' - '|| view_projetos.estado1_sgl_estado ||', '|| view_projetos.pais1_descricao_resumida ||' - '|| view_projetos.pais1_sigla AS endereco_cliente,
                 */
                $conteudo .= "              <tr>";
                $conteudo .= "                <td colspan=\"12\">";
                $conteudo .= "                  <!-- Tabela para informaçoes a respeito do cliente do projeto -->";
                $conteudo .= "                  <table id=\"layout_cliente-projeto\" class=\"separator_\">";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Cliente</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Nome: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->empresa_cliente . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Endereço: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->endereco_cliente . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Administrador: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item->administrador_cliente . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Nível: </p></td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item->nivel_administrador_cliente . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Setor:</td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->projeto_setor . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Chefe setor: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item->chefe_setor . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"1\">Nível: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item->nivel_chefe_setor . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "";
                $conteudo .= "                  </table> <!-- Tabela para informaçoes a respeito do cliente do projeto -->";
                $conteudo .= "                </td>";
                $conteudo .= "              </tr>";
                $conteudo .= "";


                /**
                 * Dados do Projeto
                 *      view_projetos.projeto_id_projeto, 
                 *      view_projetos.projeto_id_projeto ||' - '|| view_projetos.projeto_descricao_resumida AS projeto,
                 *      view_projetos.projeto_data_inclusao, 
                 *      view_projetos.projeto_descricao,
                 *      view_projetos.projeto_observacoes, 
                 *      view_projetos.projeto_custo_previsto,
                 *      view_projetos.projeto_concluido,
                 *      view_projetos.projeto_cod_usuario_chefe ||' - '|| view_projetos.usuario1_nome ||', '|| view_projetos.usuario1_email AS usuario_projeto, 
                 *      view_projetos.usuario1_nivel_usuario AS nivel_usuario_projeto,
                 *      view_projetos.projeto_cod_status_projeto ||' - '|| view_projetos.projeto_status1_descricao AS status_projeto
                 *      view_projetos.projeto_status1_percentual_concluido AS concluido
                 */
                $conteudo .= "              <tr>";
                $conteudo .= "                <td colspan=\"12\">";
                $conteudo .= "                  <!-- Tabela para informaçoes a respeito do projeto -->";
                $conteudo .= "                  <table id=\"layout_projeto\" class=\"separator_\">";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Projeto</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Título: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->projeto . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Chefe projeto: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item->usuario_projeto . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"1\">Nível: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item->nivel_usuario_projeto . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Descrição: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->projeto_descricao . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Observações: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->projeto_observacoes . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"1\">Status: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"5\"><p>" . $item->status_projeto . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"3\">Percentual Concluído: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"3\"><p>" . $item->concluido . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"1\">Data inclusão: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"3\"><p>" . $item->projeto_data_inclusao . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"1\">Concluído: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"2\"><p>" . $item->projeto_concluido . "</p></td>";
                $conteudo .= "                      <td class=\"label\" colspan=\"1\">Custo estimado: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item->projeto_custo_previsto . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                  </table> <!-- Tabela para informaçoes a respeito do projeto -->";
                $conteudo .= "                </td>";
                $conteudo .= "              </tr>";
                $conteudo .= "";


                /**
                 * Categoria do Projeto
                 *      view_projetos.projeto_cod_categoria_projeto ||' - '|| view_projetos.categoria_projeto1_descricao AS categoria_projeto, 
                 *      view_projetos.categoria_projeto1_observacoes AS observacoes_categoria, 
                 */
                $conteudo .= "              <tr>";
                $conteudo .= "                <td colspan=\"12\">";
                $conteudo .= "                  <!-- Tabela para informaçoes a respeito da categoria de projeto -->";
                $conteudo .= "                  <table id=\"layout_categoria-projeto\" class=\"separator_\">";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"title\" colspan=\"12\">Categoria</td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Descrição: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item->categoria_projeto . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                    <tr>";
                $conteudo .= "                      <td class=\"label\" colspan=\"2\">Observações: </td>";
                $conteudo .= "                      <td class=\"field\" colspan=\"12\"><p>" . $item->observacoes_categoria . "</p></td>";
                $conteudo .= "                    </tr>";
                $conteudo .= "                  </table> <!-- Tabela para informaçoes a respeito da categoria de projeto -->";
                $conteudo .= "                </td>";
                $conteudo .= "              </tr>";
                $conteudo .= "";



                /**
                 * Membros do Projeto
                 */
                $SQL_MEMBROS = "SELECT view_membros.projeto_membros_cod_projeto, 
                                       view_membros.projeto_membros_cod_usuario || ' - ' || usuario1_nome || ' - ' || usuario1_email AS membro,
                                       view_membros.usuario1_nivel_usuario AS nivel_usuario,
                                       view_membros.projeto_membros_nivel AS nivel_membro
                                  FROM view_membros
                                 WHERE view_membros.projeto_membros_cod_projeto = " . $item->projeto_id_projeto .
                        "        ORDER BY nivel_membro DESC";

                //Executa o SQL e armazena na variavel $resultados_membros
                $resultados_membros = $this->db->query($SQL_MEMBROS)->result();

                if (!empty($resultados_membros)) {
                    $conteudo .= "              <tr>";
                    $conteudo .= "                <td colspan=\"12\">";
                    $conteudo .= "                  <!-- Tabela para informaçoes complementares a respeito dos membros do projeto -->";
                    $conteudo .= "                  <table id=\"layout_membro-projeto\" class=\"separator_\">";
                    $conteudo .= "                    <tr>";
                    $conteudo .= "                      <td class=\"title\" colspan=\"12\">Membros</td>";
                    $conteudo .= "                    </tr>";

                    // Percorrendo "resultados" e mostrando todos os registros
                    foreach ($resultados_membros as $item_membro) {
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item_membro->membro . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Nível: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_membro->nivel_usuario . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Nível no Projeto: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_membro->nivel_membro . "</p></td>";
                        $conteudo .= "                    </tr>";
                    }

                    $conteudo .= "                  </table> <!-- Tabela para informaçoes complementares a respeito dos membros do projeto -->";
                    $conteudo .= "                </td>";
                    $conteudo .= "              </tr>";
                    $conteudo .= "";
                }


                /**
                 * Tarefas do Projeto 
                 */
                $SQL_TAREFAS = "SELECT view_tarefas.projeto_tarefa_id_tarefa, 
                                       view_tarefas.projeto_tarefa_titulo, 
                                       view_tarefas.projeto_tarefa_progresso, 
                                       view_tarefas.projeto_tarefa_resolvido, 
                                       view_tarefas.projeto_tarefa_descricao, 
                                       view_tarefas.projeto_tarefa_data_inclusao, 
                                       view_tarefas.projeto_tarefa_data_previsto_inicial, 
                                       view_tarefas.projeto_tarefa_data_previsto_final, 
                                       view_tarefas.projeto_tarefa_data_real_inicial, 
                                       view_tarefas.projeto_tarefa_data_real_final,
                                       view_tarefas.projeto_tarefa_cod_projeto,
                                       view_tarefas.projeto_tarefa_fase_atual || ' - ' || view_tarefas.projeto_fases1_descricao AS fase, 
                                       view_tarefas.projeto_fases1_ordem_fase,
                                       view_tarefas.projeto_tarefa_responsavel || ' - ' || view_tarefas.usuario1_nome || ', ' || view_tarefas.usuario1_email AS responsavel,
                                       view_tarefas.usuario1_nivel_usuario
                                  FROM view_tarefas 
                                 WHERE view_tarefas.projeto_tarefa_cod_projeto = " . $item->projeto_id_projeto .
                        "        ORDER BY view_tarefas.projeto_fases1_ordem_fase,
                                       view_tarefas.projeto_tarefa_data_previsto_inicial,
                                       view_tarefas.projeto_tarefa_data_previsto_final,
                                       view_tarefas.projeto_tarefa_data_real_inicial,
                                       view_tarefas.projeto_tarefa_data_real_final";

                //Executa o SQL e armazena na variavel $resultados_tarefas
                $resultados_tarefas = $this->db->query($SQL_TAREFAS)->result();

                if (!empty($resultados_tarefas)) {
                    $conteudo .= "              <tr>";
                    $conteudo .= "                <td class=\"title\" colspan=\"12\">Tarefas";

                    // Percorrendo "resultados" e mostrando todos os registros
                    foreach ($resultados_tarefas as $item_tarefa) {
                        $conteudo .= "                  <!-- Tabela para informaçoes das tarefas do projeto -->";
                        $conteudo .= "                  <table id=\"layout_tarefas-projeto\" class=\"separator_\">";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Inclusão: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"4\"><p>" . $item_tarefa->projeto_tarefa_data_inclusao . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Fase: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"3\"><p>" . $item_tarefa->fase . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Ordem: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"2\"><p>" . $item_tarefa->projeto_fases1_ordem_fase . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Ini. Prev.: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"2\"><p>" . $item_tarefa->projeto_tarefa_data_previsto_inicial . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Fin. Prev.: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"2\"><p>" . $item_tarefa->projeto_tarefa_data_previsto_final . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Ini. Real: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"2\"><p>" . $item_tarefa->projeto_tarefa_data_real_inicial . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Fin. Real: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"2\"><p>" . $item_tarefa->projeto_tarefa_data_real_final . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Responsável: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item_tarefa->responsavel . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Nível: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_tarefa->usuario1_nivel_usuario . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Título: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"7\"><p>" . $item_tarefa->projeto_tarefa_titulo . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Progesso: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_tarefa->projeto_tarefa_progresso . "</p></td>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Resolvido: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_tarefa->projeto_tarefa_resolvido . "</p></td>";
                        $conteudo .= "                    </tr>";
                        $conteudo .= "                    <tr>";
                        $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Descrição: </p></td>";
                        $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item_tarefa->projeto_tarefa_descricao . "</p></td>";
                        $conteudo .= "                    </tr>";


                        /**
                         * Atividades da Tarefa do Projeto
                         */
                        $SQL_TAREFA_ATIVIDADE = "SELECT view_atividades.projeto_tarefa_atividades_codigo_atividade, 
                                                        view_atividades.projeto_tarefa_atividades_codigo_tarefa, 
                                                        view_atividades.projeto_tarefa_atividades_codigo_usuario || ' - ' || usuario1_nome || ', ' || usuario1_email AS executor, 
                                                        view_atividades.usuario1_nivel_usuario,
                                                        view_atividades.projeto_tarefa_atividades_tempo_atividade, 
                                                        view_atividades.projeto_tarefa_atividades_descricao_atividade     
                                                   FROM view_atividades
                                                  WHERE view_atividades.projeto_tarefa_atividades_codigo_tarefa = " . $item_tarefa->projeto_tarefa_id_tarefa .
                                "                 ORDER BY view_atividades.projeto_tarefa_atividades_codigo_tarefa,
                                                        view_atividades.projeto_tarefa_atividades_codigo_atividade";

                        //Executa o SQL e armazena na variavel $resultados_tarefa_atividades
                        $resultados_tarefa_atividades = $this->db->query($SQL_TAREFA_ATIVIDADE)->result();

                        if (!empty($resultados_tarefa_atividades)) {
                            $conteudo .= "              <tr>";
                            $conteudo .= "                <td class=\"title\" colspan=\"12\">Atividades da Tarefa";

                            // Percorrendo "resultados" e mostrando todos os registros
                            foreach ($resultados_tarefa_atividades as $item_tarefa_atividade) {
                                $conteudo .= "                  <!-- Tabela para informaçoes das atividades da tarefa do projeto -->";
                                $conteudo .= "                  <table id=\"layout_tarefas-projeto\" class=\"separator_\">";
                                $conteudo .= "                    <tr>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Atividade: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item_tarefa_atividade->projeto_tarefa_atividades_descricao_atividade . "</p></td>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Tempo: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_tarefa_atividade->projeto_tarefa_atividades_tempo_atividade . "</p></td>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                    <tr>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Executor: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item_tarefa_atividade->executor . "</p></td>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Nível: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_tarefa_atividade->usuario1_nivel_usuario . "</p></td>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                  </table> <!-- Tabela para informaçoes das atividades da tarefa do projeto -->";
                            }

                            $conteudo .= "                </td>";
                            $conteudo .= "              </tr>";
                            $conteudo .= "";
                        }


                        /**
                         * Custos da Tarefa do Projeto
                         */
                        $SQL_TAREFA_CUSTO = "SELECT view_custos.data_lancamento,
                                                    view_custos.mes,
                                                    view_custos.ano,
                                                    view_custos.descricao,
                                                    view_custos.valor_lancamento,
                                                    view_custos.cod_projeto,
                                                    view_custos.id_tarefa,
                                                    view_custos.nome_tarefa,
                                                    view_custos.responsavel,
                                                    view_custos.projeto
                                               FROM view_custos
                                              WHERE view_custos.id_tarefa = " . $item_tarefa->projeto_tarefa_id_tarefa .
                                "             ORDER BY view_custos.id_tarefa,
                                                    view_custos.valor_lancamento";


                        //Executa o SQL e armazena na variavel $resultados_tarefa_custos
                        $resultados_tarefa_custos = $this->db->query($SQL_TAREFA_CUSTO)->result();

                        if (!empty($resultados_tarefa_custos)) {
                            $conteudo .= "              <tr>";
                            $conteudo .= "                <td class=\"title\" colspan=\"12\">Custos da Tarefa";

                            // Percorrendo "resultados" e mostrando todos os registros
                            foreach ($resultados_tarefa_custos as $item_tarefa_custo) {
                                $conteudo .= "                  <!-- Tabela para informaçoes dos custos da tarefa do projeto -->";
                                $conteudo .= "                  <table id=\"layout_tarefas-projeto\" class=\"separator_\">";
                                $conteudo .= "                    <tr>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Descrição: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item_tarefa_custo->descricao . "</p></td>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                    <tr>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Responsável: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item_tarefa_custo->responsavel . "</p></td>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Valor: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"2\"><p>" . $item_tarefa_custo->valor_lancamento . "</p></td>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                  </table> <!-- Tabela para informaçoes dos custos da tarefa do projeto -->";
                            }

                            $conteudo .= "                </td>";
                            $conteudo .= "              </tr>";
                            $conteudo .= "";
                        }


                        /**
                         * Arquivos da Tarefa do Projeto
                         */
                        $SQL_TAREFA_ARQUIVO = "SELECT view_arquivos.projeto_tarefa_arquivos_codigo_arquivo, 
                                                      view_arquivos.projeto_tarefa_arquivos_nome_arquivo, 
                                                      view_arquivos.projeto_tarefa_arquivos_descricao, 
                                                      view_arquivos.projeto_tarefa_arquivos_codigo_usuario || ' - ' || usuario1_nome || ', ' || usuario1_email AS responsavel, 
                                                      view_arquivos.usuario1_nivel_usuario,
                                                      view_arquivos.projeto_tarefa_arquivos_codigo_tarefa
                                                 FROM view_arquivos
                                                WHERE view_arquivos.projeto_tarefa_arquivos_codigo_tarefa = " . $item_tarefa->projeto_tarefa_id_tarefa .
                                "               ORDER BY view_arquivos.projeto_tarefa_arquivos_codigo_tarefa,
                                                      view_arquivos.projeto_tarefa_arquivos_codigo_arquivo";

                        //Executa o SQL e armazena na variavel $resultados_tarefa_arquivo
                        $resultados_tarefa_arquivo = $this->db->query($SQL_TAREFA_ARQUIVO)->result();

                        if (!empty($resultados_tarefa_arquivo)) {
                            $conteudo .= "              <tr>";
                            $conteudo .= "                <td class=\"title\" colspan=\"12\">Arquivos da Tarefa";

                            // Percorrendo "resultados" e mostrando todos os registros
                            foreach ($resultados_tarefa_arquivo as $item_tarefa_arquivo) {
                                $conteudo .= "                  <!-- Tabela para informaçoes dos arquivos da tarefa do projeto -->";
                                $conteudo .= "                  <table id=\"layout_tarefas-projeto\" class=\"separator_\">";
                                $conteudo .= "                    <tr>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Arquivo: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item_tarefa_arquivo->projeto_tarefa_arquivos_nome_arquivo . "</p></td>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                    <tr>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Descrição: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"10\"><p>" . $item_tarefa_arquivo->projeto_tarefa_arquivos_descricao . "</p></td>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Responsável: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item_tarefa_arquivo->responsavel . "</p></td>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Nível: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_tarefa_arquivo->usuario1_nivel_usuario . "</p></td>";
                                $conteudo .= "                    </tr>";
                                $conteudo .= "                  </table> <!-- Tabela para informaçoes dos arquivos da tarefa do projeto -->";
                            }

                            $conteudo .= "                </td>";
                            $conteudo .= "              </tr>";
                            $conteudo .= "";
                        }


                        /**
                         * Envolvidos na tarefa Projeto
                         */
                        $SQL_ENVOLVIDOS = "SELECT view_envolvidos.projeto_tarefa_envolvidos_codigo_tarefa, 
                                                  view_envolvidos.projeto_tarefa_envolvidos_codigo_usuario || ' - ' || usuario1_nome || ', ' || usuario1_email AS envolvido, 
                                                  view_envolvidos.usuario1_nivel_usuario,
                                                  view_envolvidos.projeto_tarefa_envolvidos_nivel
                                             FROM view_envolvidos
                                            WHERE view_envolvidos.projeto_tarefa_envolvidos_codigo_tarefa = " . $item_tarefa->projeto_tarefa_id_tarefa .
                                "        ORDER BY view_envolvidos.projeto_tarefa_envolvidos_codigo_tarefa,
                                                  view_envolvidos.projeto_tarefa_envolvidos_nivel DESC";

                        //Executa o SQL e armazena na variavel $resultados_envolvidos
                        $resultados_envolvidos = $this->db->query($SQL_ENVOLVIDOS)->result();

                        if (!empty($resultados_envolvidos)) {
                            $conteudo .= "              <tr>";
                            $conteudo .= "                <td colspan=\"12\">";
                            $conteudo .= "                  <!-- Tabela para informaçoes complementares a respeito dos envolvidos no projeto -->";
                            $conteudo .= "                  <table id=\"layout_envolvidos-projeto\" class=\"separator_\">";
                            $conteudo .= "                    <tr>";
                            $conteudo .= "                      <td class=\"title\" colspan=\"12\">Envolvidos na Tarefa</td>";
                            $conteudo .= "                    </tr>";

                            // Percorrendo "resultados" e mostrando todos os registros
                            foreach ($resultados_envolvidos as $item_envolvido) {
                                $conteudo .= "                    <tr>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"8\"><p>" . $item_envolvido->envolvido . "</p></td>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"1\"><p>Nível: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_envolvido->usuario1_nivel_usuario . "</p></td>";
                                $conteudo .= "                      <td class=\"label\" colspan=\"2\"><p>Nível no Projeto: </p></td>";
                                $conteudo .= "                      <td class=\"field\" colspan=\"1\"><p>" . $item_envolvido->projeto_tarefa_envolvidos_nivel . "</p></td>";
                                $conteudo .= "                    </tr>";
                            }

                            $conteudo .= "                  </table> <!-- Tabela para informaçoes complementares a respeito dos envolvidos no projeto -->";
                            $conteudo .= "                </td>";
                            $conteudo .= "              </tr>";
                            $conteudo .= "";
                        }
                        $conteudo .= "                  </table> <!-- Tabela para informaçoes das tarefas do projeto -->";
                    }

                    $conteudo .= "                </td>";
                    $conteudo .= "              </tr>";
                    $conteudo .= "";
                }

                /**
                 * Fechamento do relatório
                 */
                $conteudo .= "";
                $conteudo .= "            </table> <!-- Tabela de layout -->";
            }

            $conteudo .= "            <p class=\"info-right\">Fim do arquivo</p>";
            $conteudo .= "          </div> <!-- form_settings -->";
            $conteudo .= "        </div> <!-- content-both -->";
            $conteudo .= "      </div> <!-- content -->";
            $conteudo .= "";
            $conteudo .= "    </div> <!-- site_content -->";
            $conteudo .= "  </body>";
            $conteudo .= "</html>";

            //$conteudo.=" jkltaejklaet ".$resultados[0]->coluna;
            //Gera o PDF
            $this->geraPDF("projetos_" . date("Ymd"), $conteudo);
        }
    }

}
