<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Lucas Leandro de Moura
 */
//Deve mudar o nome da classe para o nome do arquivo
class Categoria_projeto extends MY_Controller {

    protected $tabela = "categoria_projeto";
    private $nome_classe = "categoria_projeto";

    //Tela inicial
    function index($erro = "") {
        $this->autentica();
        // Definindo o título da janela
        $this->setTitulo("Categoria de Projeto");
        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Descrição"),
            array("titulo" => "Empresa"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
        $this->db->select("categoria_projeto.*,empresa.nome_empresa as empresa");
        $this->db->join("empresa", "empresa.cod_empresa = categoria_projeto.codigo_empresa", "inner");
        $this->db->where($this->getEmpresasComAcessos("empresa.cod_empresa"));


        //Aplica valores do filtro
        foreach ($this->session->userdata("filtros")[$this->tabela] as $key => $value) {
            if ($value != "") {
                $this->db->where($this->tabela . "." . $key, $value);
            }
        }

        $resultados = $this->db->get($this->tabela)->result();
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            //Cria um indice
            $indices = array(array("id_categoria_projeto" => $item->id_categoria_projeto));

            //Seta as permissões para a tabela
            $botoes = "";
            $botoes = L_Deletar($indices, $this->nome_classe . "/excluir") . " " .
                    L_Editar($indices, $this->nome_classe . "/editar");

            //Carrega as informações que irá na tabela
            $dados[]["dados"] = array(
                $item->id_categoria_projeto,
                $item->descricao,
                $item->empresa,
                $botoes
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);
        $this->erro = $erro;

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::index($indices);
    }

    /**
     * Tela de cadastro
     */
    public function cadastro() {
        $this->autentica();

        //Titulo da tela
        $this->setTitulo("Cadastro de Categoria de Projeto");
        //Controlar que esta a operação de gravar
        $this->setAcao($this->nome_classe . "/cadastrar");

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

    /**
     * Ação de cadastrar
     */
    public function cadastrar() {
        $this->autentica();
        //$this->setTabela("categoria_projeto");
        parent::cadastrar();
        //Redireciona
        redirect($this->nome_classe);
    }

    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->autentica();
        //$this->setTabela("categoria_projeto");
        $this->setTitulo("Cadastro de Categoria de Projeto");

        $this->db->where("id_categoria_projeto", $this->input->get("id_categoria_projeto"));
        $this->setDados($this->db->get($this->tabela)->result());
        $this->setAcao($this->nome_classe. "/cadastrar?id_categoria_projeto=" . $this->input->get("id_categoria_projeto"));

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

    public function excluir() {
        $this->autentica();
        $this->setTabela($nome_classe);
        $excluir = parent::excluir();
        if ($excluir != "") {
            $this->index($excluir);
        } else {
            redirect($nome_classe);
        }
    }

}
