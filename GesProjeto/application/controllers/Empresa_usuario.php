<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Lucas Leandro de Moura
 */
//Deve mudar o nome da classe para o nome do arquivo
class Empresa_usuario extends MY_Controller {
    protected $tabela = "empresa_usuarios";


    //Tela inicial
    public function index() {
        $this->autentica();
        // Definindo o título da janela
        $this->setTitulo("Usuários da empresa");
        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Empresa"),
            array("titulo" => "Usuário"),
            array("titulo" => "E-mail"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
        $this->db->select("empresa_usuarios.cod_empresa,"
                . "empresa_usuarios.cod_usuario,"
                . "empresa.nome_empresa,"
                . "usuario.nome,"
                . "usuario.email");
        $this->db->join("cidade","cidade.id_cidade = empresa.codigo_cidade","inner");
        $this->db->join("empresa_usuarios","empresa.cod_empresa = empresa_usuarios.cod_empresa","inner");
        $this->db->join("usuario","usuario.id_usuario = empresa_usuarios.cod_usuario","inner");
        foreach($this->session->userdata("filtros")[$this->tabela] as $key=>$value){
            if($value!=""){
                $this->db->where($this->tabela.".".$key,$value);
            }
        }
        
        //Define quais empresas deverão aparecer
        $this->db->where("empresa.usuario_administrador", $this->codigo_usuario);        
        
        $resultados = $this->db->get("empresa")->result();
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            $indices = array(array("cod_empresa" => $item->cod_empresa),
                             array("cod_usuario"=>$item->cod_usuario));
            //Carrega as informações que irá na tabela
                $botoes = L_Deletar($indices, "empresa_usuario/excluir");
            
            
            $dados[]["dados"] = array(
                $item->nome_empresa,
                $item->nome,
                $item->email,
                $botoes
            );
        }
        
        //Definindo os dados da tabela
        $this->setLinhas($dados);
        $indices = array("cod_usuario"=> $this->codigo_usuario);
        parent::index($indices);
    }
    
    
    public function excluir() {

        parent::excluir();
        redirect("empresa_usuario");
    }


    /**
     * Tela de cadastro
     */
    public function cadastro() {
        $this->autentica();

        //Titulo da tela
        $this->setTitulo("Liberação de acesso a empresa");
        //Controlar que esta a operação de gravar
        $this->setAcao("empresa_usuario/cadastrar");
        $indices = array("cod_usuario"=> $this->codigo_usuario);
        parent::cadastro($indices);
    }
    /**
     * Ação de cadastrar
     */
    public function cadastrar() {
        $this->autentica();
  
        parent::cadastrar();
        //Redireciona
        redirect("empresa_usuario");
    }

}
