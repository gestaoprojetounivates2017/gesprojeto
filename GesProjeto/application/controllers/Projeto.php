<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Projeto extends MY_Controller {

    //Utilização padrão
    protected $tabela = "projeto";

    public function index() {
        //Defino o título da página
        $this->autentica();
        $this->setTitulo("Projetos");

        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Nome"),
            array("titulo" => "Previsão de Conclusão"),
            array("titulo" => "% Concluído"),
            array("titulo" => "Ações")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);


        $dados = array();
        $this->db->select("ROUND((select AVG(projeto_tarefa.progresso) as media from projeto_tarefa where cod_projeto = projeto.id_projeto),2) as p_concluido,projeto.*, empresa_setores.codigo_empresa");
        $this->db->join("empresa_setores", "empresa_setores.id_setor = projeto.cod_setor", "inner");
        $this->db->where("projeto.cod_usuario_chefe = " . $this->codigo_usuario . " OR"
                . "(" . $this->codigo_usuario . " IN (SELECT cod_usuario FROM projeto_membros WHERE projeto_membros.cod_projeto = projeto.id_projeto))");

        //Aplica valores do filtro
        foreach ($this->session->userdata("filtros")[$this->tabela] as $key => $value) {
            if ($value != "") {
                $this->db->where($this->tabela . "." . $key, $value);
            }
        }

        $resultados = $this->db->get("projeto")->result();

        foreach ($resultados as $item) {
            $indices = array(array("id_projeto" => $item->id_projeto));
            $botoes = L_Abrir($indices, "Projeto/abrir");

            $nivel_usuario_logado = $this->retorna_nivel_no_projeto($item->id_projeto);

            if ($nivel_usuario_logado == 2) {
                $botoes .= " " . L_Deletar($indices, "Projeto/excluir") . " " .
                        L_Editar($indices, "Projeto/editar");
            }
            if($item->p_concluido < 100){
                $cor = "progress-bar-danger";
            }else {
                $cor = "progress-bar-success";
            }
            $dados[]["dados"] = array(
                $item->id_projeto,
                $item->descricao_resumida,
                L_label_data($item->data_inclusao, "d/m/Y"),
                L_progress_bar($item->p_concluido,$cor),
                $botoes
            );
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::index($indices);
    }

    public function fases($indice, $erro = "") {
        //Defino o título da página
        $this->autentica();
        $this->setTitulo("Fases do projeto");

        $cabecalho = array(
            array("titulo" => "Ordem da fase"),
            array("titulo" => "Descrição"),
            array("titulo" => "Ações")
        );
        //Defino o cabecalho da tabelas
        $this->setCabecalho($cabecalho);


        $dados = array();

        $this->db->where("codigo_projeto", $indice);
        $this->db->order_by("ordem_fase");
        $resultados = $this->db->get("projeto_fases")->result();

        foreach ($resultados as $item) {
            $indices = array(array("id_fase_projeto" => $item->id_fase_projeto), array("codigo_projeto" => $indice));
            $botoes = "";

            $nivel_usuario_logado = $this->retorna_nivel_no_projeto($item->codigo_projeto);

            if ($nivel_usuario_logado == 2) {
                $botoes .= " " . L_Deletar($indices, "projeto/fases_excluir");
            }
            $dados[]["dados"] = array(
                $item->ordem_fase,
                $item->descricao,
                $botoes
            );
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);
        $this->setBotao_voltar(base_url() . "projeto/abrir/" . $indice);
        $this->setBotao_novo(true);
        $this->setUrl_novo(base_url() . "projeto/fases_cadastro/" . $indice);
        $this->erro = $erro;

        parent::index();
    }

    public function fases_excluir() {
        $this->autentica();
        $this->setTabela("projeto_fases");
        $excluir = parent::excluir();
        $indice = $this->input->get("codigo_projeto");

        if ($excluir != "") {
            $this->fases($indice, $excluir);
        } else {
            redirect("projeto/fases/" . $indice);
        }
    }

    public function fases_cadastro($indice) {
        $this->autentica();
        $this->setTabela("projeto_fases");
        $this->setTitulo("Cadastro de fases no projeto");
        $this->setAcao("projeto/fases_cadastrar/" . $indice);
        parent::cadastro();
    }

    public function fases_cadastrar($indice) {
        $this->autentica();
        $this->setTabela("projeto_fases");
        $data["codigo_projeto"] = $indice;
        $this->setData($data);
        $cadastrar = parent::cadastrar();

        redirect("projeto/fases/" . $indice);
    }

    public function status($indice, $erro = "") {
        //Defino o título da página
        $this->autentica();
        $this->setTitulo("Status deste projeto");

        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Descrição"),
            array("titulo" => "% Automático"),
            array("titulo" => "Ações")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);


        $dados = array();

        $this->db->where("codigo_projeto", $indice);
        $resultados = $this->db->get("projeto_status")->result();

        foreach ($resultados as $item) {
            $indices = array(array("id_status_projeto" => $item->id_status_projeto), array("codigo_projeto" => $indice));
            $botoes = "";

            $nivel_usuario_logado = $this->retorna_nivel_no_projeto($item->codigo_projeto);

            if ($nivel_usuario_logado == 2) {
                $botoes .= " " . L_Deletar($indices, "projeto/status_excluir");
            }
            $dados[]["dados"] = array(
                $item->id_status_projeto,
                $item->descricao,
                L_progress_bar($item->percentual_concluido),
                $botoes
            );
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);
        $this->setBotao_voltar(base_url() . "projeto/abrir/" . $indice);
        $this->setBotao_novo(true);
        $this->setUrl_novo(base_url() . "projeto/status_cadastro/" . $indice);

        parent::index();
    }

    public function status_excluir() {
        $this->autentica();
        $this->setTabela("projeto_fases");
        $excluir = parent::excluir();
        $indice = $this->input->get("codigo_projeto");

        if ($excluir != "") {
            $this->status($indice, $excluir);
        } else {
            redirect("projeto/status/" . $indice);
        }
    }

    public function status_cadastro($indice) {
        $this->autentica();
        $this->setTabela("projeto_status");
        $this->setTitulo("Cadastro de status no projeto");
        $this->setAcao("projeto/status_cadastrar/" . $indice);
        parent::cadastro();
    }

    public function status_cadastrar($indice) {
        $this->autentica();
        $this->setTabela("projeto_status");
        $data["codigo_projeto"] = $indice;
        $this->setData($data);
        $cadastrar = parent::cadastrar();

        redirect("projeto/status/" . $indice);
    }

    public function custos($indice) {
        $this->autentica();

        $CI = &get_instance();
        $CI->load->library('dynacore');
        $CI->load->library('dynatables');


        $CI->dynatables->setDatabaseName('default');
        $CI->dynatables->setTable('view_custos');
        $CI->dynatables->addClause("cod_projeto", "=", $indice);
        $CI->dynatables->setTitle('Custos do projeto');
        $CI->dynatables->setSubTitle('');

        $CI->dynatables->setfilterTitle('Filtros');
        $CI->dynatables->setfilterSubtitle('');


        // Filtro de linhas
        // NIVEL 1
        $rows = array();
        $rows[1] = array();
        $rows[1]['nome_tarefa'] = new stdClass();
        $rows[1]['nome_tarefa']->name = 'Tarefa';
        $rows[1]['ano'] = new stdClass();
        $rows[1]['ano']->name = 'Ano';
        $rows[1]['responsavel'] = new stdClass();
        $rows[1]['responsavel']->name = 'Responsável';
        $rows[1]['descricao'] = new stdClass();
        $rows[1]['descricao']->name = 'Descrição do Serviço';


        $rows[2] = array();
        $rows[2]['responsavel'] = new stdClass();
        $rows[2]['responsavel']->name = 'Responsável';
        $rows[2]['ano'] = new stdClass();
        $rows[2]['ano']->name = 'Ano';
        $rows[2]['nome_tarefa'] = new stdClass();
        $rows[2]['nome_tarefa']->name = 'Tarefa';
        $rows[2]['descricao'] = new stdClass();
        $rows[2]['descricao']->name = 'Descrição do Serviço';



        $rows[3] = array();
        $rows[3]['ano'] = new stdClass();
        $rows[3]['ano']->name = 'Ano';
        $rows[3]['nome_tarefa'] = new stdClass();
        $rows[3]['nome_tarefa']->name = 'Tarefa';
        $rows[3]['responsavel'] = new stdClass();
        $rows[3]['responsavel']->name = 'Responsável';
        $rows[3]['descricao'] = new stdClass();
        $rows[3]['descricao']->name = 'Descrição do Serviço';

        $rows[4] = array();
        $rows[4]['ano'] = new stdClass();
        $rows[4]['ano']->name = 'Ano';
        $rows[4]['nome_tarefa'] = new stdClass();
        $rows[4]['nome_tarefa']->name = 'Tarefa';
        $rows[4]['responsavel'] = new stdClass();
        $rows[4]['responsavel']->name = 'Responsável';
        $rows[4]['descricao'] = new stdClass();
        $rows[4]['descricao']->name = 'Descrição do Serviço';






        $CI->dynatables->setFilterRows($rows);
        $CI->dynatables->totalRows(true);
        $CI->dynatables->totalCols(false);
        $CI->dynatables->subtotalRows(false);


        $cols = array();
        $cols['mes'] = new stdClass();
        $cols['mes']->name = 'mes';
        $cols['mes']->align = 'right';
        $cols['mes']->aliases = array('01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro');
        $cols['mes']->default = "0.00";

        $CI->dynatables->setCols($cols);

        // Cálculo
        $calculus = array();
        $calculus['valor_lancamento'] = new stdClass();
        $calculus['valor_lancamento']->name = "Soma";
        $calculus['valor_lancamento']->calculus = 'sum';
        $calculus['valor_lancamento']->format = array('type' => 'number', 'dec' => '2', 'sepd' => ',', 'sepm' => '.');


        $CI->dynatables->setFilterCalculus($calculus);


        //Define os filtros
        $filters = array();
        //$filters['ano'] = new stdClass();
        //$filters['ano']->title = 'Ano de análise';
        //$filters['ano']->key = 'ano';
        //$filters['ano']->table = 'view_custos';


        $CI->dynatables->addMainFilter($filters);




        $this->db->where("id_projeto", $indice);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;
        $data["titulo"] = "Custos do projeto " . $descricao_resumida;

        $data["botao_novo"] = FALSE;
        $data["voltar"] = base_url() . "projeto/abrir?id_projeto=" . $indice;


        global $header_nav;
        $this->load->view("Includes/header");
        $this->load->view("Includes/header_nav", $header_nav);
        $this->load->view("tabela_cruzada", $data);
        $this->load->view('Includes/footer');
    }

    public function cadastro() {
        $this->autentica();

        $this->setTitulo("Cadastro de projetos");
        $this->setAcao("projeto/cadastrar");

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

    public function tarefas_editar() {
        $this->autentica();
        $this->setTabela("projeto_tarefa");
        $this->setTitulo("Editar tarefa");
        $this->setAcao("projeto/cadastrar/" . $this->input->get("id_tarefa"));

        $this->db->where("id_tarefa", $this->input->get("id_tarefa"));
        $this->setDados($this->db->get("projeto_tarefa")->result());
        $indices = array("indice" => $this->dados[0]->cod_projeto);

        $this->tarefas_cadastro($this->dados[0]->cod_projeto, $this->input->get("id_tarefa"));
    }

    /**
     * @author Lucas Leandro de Moura
     */
    public function tarefas_cadastro($indice, $id_tarefa = "") {
        $this->autentica();
        //Tabela do banco
        $this->setTabela("projeto_tarefa");
        //Titulo da tela
        $this->db->where("id_projeto", $indice);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;

        $this->setTitulo("Tarefas do projeto " . $descricao_resumida);

        //Controlar que esta a operação de gravar
        if ($id_tarefa == "") {
            $this->setAcao("projeto/tarefas_cadastrar/" . $indice . "/" . $id_tarefa);
        } else {
            $this->setAcao("projeto/tarefas_cadastrar/" . $indice . "/?id_tarefa=" . $id_tarefa);
        }
        $indices = array("indice" => $indice);
        parent::cadastro($indices);
    }

    public function tarefas_cadastrar($indice) {
        $this->autentica();
        $this->setTabela("projeto_tarefa");
        $data["cod_projeto"] = $indice;

        if ($this->input->post("resolvido") == "t") {
            $data["progresso"] = "100";
        }


        $this->setData($data);




        $cadastrar = parent::cadastrar();
        print $cadastrar["mensagem"];
        redirect("projeto/tarefas/" . $indice);
    }

    /**
     * @author Lucas Leandro de Moura
     */
    public function arquivos_cadastro($indice, $erro = "") {
        $this->autentica();
        //Tabela do banco
        $this->setTabela("projeto_tarefa_arquivos");
        //Titulo da tela
        $this->db->where("id_projeto", $indice);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;

        $this->setTitulo("Cadastro de arquivos do " . $descricao_resumida);
        $this->erro = $erro;
        //Controlar que esta a operação de gravar
        $this->setAcao("projeto/arquivos_cadastrar/" . $indice);
        $indices = array("indice" => $indice);
        parent::cadastro($indices);
    }

    public function arquivos_cadastrar($indice, $tarefa = "") {
        $this->autentica();
        $this->setTabela("projeto_tarefa_arquivos");
        $data["codigo_projeto"] = $indice;
        if ($tarefa != "") {
            $data["codigo_tarefa"] = $tarefa;
        }
        $data["codigo_usuario"] = $this->codigo_usuario;



        $config['upload_path'] = '/var/www/gestaoprojetos/upload/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|doc';
        $config['max_size'] = 1000000;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('nome_arquivo')) {

            $erro = $this->upload->display_errors();
            $this->arquivos_cadastro($indice, $erro);
        } else {
            $novo_nome = $this->upload->data()["file_name"];
            $data["nome_arquivo"] = $novo_nome;

            $this->setData($data);
            $cadastrar = parent::cadastrar();
            redirect("projeto/arquivos/" . $indice);
        }
    }

    /**
     * Excluir arquivo
     * @author Lucas Leandro de Moura
     */
    public function arquivo_excluir() {
        $this->autentica();
        $this->setTabela("projeto_tarefa_arquivos");
        $erro = parent::excluir();
        if ($erro) {
            $this->db->where("codigo_arquivo", $this->input->get("codigo_arquivo"));
            $arquivos_deletar = $this->db->get(projeto_tarefa_arquivos)->result();
            foreach ($arquivos_deletar as $arquivos) {
                unlink("/var/www/gestaoprojetos/upload/" . $arquivos->nome_arquivo);
            }
        }
        redirect("projeto/arquivos/" . $this->input->get("codigo_projeto"));
    }

    public function arquivo_download() {
        $this->autentica();

        $resultados = $this->db->get("projeto_tarefa_arquivos")->result();
        foreach ($resultados as $item) {

            if (md5($item->codigo_arquivo) == $this->input->get("file")) {
                $pth = file_get_contents("/var/www/gestaoprojetos/upload/" . $item->nome_arquivo);
                $nme = $item->nome_arquivo;
                force_download($nme, $pth);
            }
        }
    }

    public function arquivos($indice, $codigo_tarefa = null) {

        $this->autentica();
        // Definindo o título da janela
        $this->db->where("id_projeto", $indice);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;
        $this->setTitulo("Arquivos do projeto " . $descricao_resumida);
        $this->setUrl_novo(base_url() . "projeto/arquivos_cadastro/" . $indice);

        $this->tabela = "projeto_tarefa_arquivos";
        $this->setFiltros(true);

        $nivel_no_projeto = $this->retorna_nivel_no_projeto($indice);
        if ($nivel_no_projeto == 0) {
            $this->setBotao_novo(false);
        }
        $this->setBotao_voltar(base_url() . "projeto/abrir?id_projeto=" . $indice);


        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Nome do arquivo"),
            array("titulo" => "Título"),
            array("titulo" => "Usuário"),
            array("titulo" => "Fase"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
        $this->db->select("projeto_tarefa_arquivos.*,usuario.nome,projeto_fases.descricao as nome_fase");

        $this->db->join("usuario", "projeto_tarefa_arquivos.codigo_usuario=usuario.id_usuario", "inner");
        $this->db->join("projeto_fases", "projeto_tarefa_arquivos.fase=projeto_fases.id_fase_projeto", "left");
        $this->db->where("projeto_tarefa_arquivos.codigo_projeto", $indice);

        if ($codigo_tarefa != null) {
            $this->db->where("projeto_tarefa_arquivos.codigo_tarefa", $codigo_tarefa);
        }

        foreach ($this->session->userdata("filtros")[$this->tabela] as $key => $value) {
            if ($value != "") {
                $this->db->where($this->tabela . "." . $key, $value);
            }
        }

        $resultados = $this->db->get("projeto_tarefa_arquivos")->result();
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            $indices = array(array("codigo_arquivo" => $item->codigo_arquivo), array("codigo_projeto" => $item->codigo_projeto));
            $keysmd5 = array(array("file" => md5($item->codigo_arquivo)));
            //Carrega as informações que irá na tabela
            $botoes = L_Botao($keysmd5, "projeto/arquivo_download", "fa-download", "Download");
            if ($item->codigo_usuario == $this->codigo_usuario || $nivel_no_projeto == 2) {
                $botoes .= " " . L_Deletar($indices, "projeto/arquivo_excluir");
            }

            $dados[]["dados"] = array(
                $item->codigo_arquivo,
                $item->nome_arquivo,
                $item->descricao,
                $item->nome,
                $item->nome_fase,
                $botoes
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);

        parent::index();
    }
    
    
    public function tarefas_abrir(){
         $this->autentica();
         global $header_nav;
         $dados = array();
         
         $dados["id_tarefa"] = $this->input->get("id_tarefa");
         $dados["cod_projeto"] = $this->input->get("cod_projeto");
         
         
         $this->load->view("Includes/header");
         $this->load->view("Includes/header_nav", $header_nav);
         
         
         $this->db->where("id_tarefa",$dados["id_tarefa"]);
         $descricao_resumida = $this->db->get("projeto_tarefa")->result()[0]->titulo;
         $dados["cod_projeto"] = $this->db->get("projeto_tarefa")->result()[0]->cod_projeto;
         $dados["titulo"] = "Ações da tarefa ".$descricao_resumida;
         
         
        $this->load->view("tarefa/index",$dados);
        $this->load->view("Includes/footer");
         
     }
     
     
      public function tarefas_custos_cadastro($indice) {
        $this->autentica();
        //Tabela do banco
        $this->setTabela("projeto_tarefa_custos");
        //Titulo da tela
        $this->db->where("id_projeto",$indice);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;
        
        $this->setTitulo("Cadastro de custos da tarefa ".$descricao_resumida);
        
        //Controlar que esta a operação de gravar
        if($id_tarefa==""){
            $this->setAcao("projeto/tarefas_custos_cadastrar/".$indice);
        }
        $indices = array("codigo_tarefa"=>$indice);
        parent::cadastro($indices);
    }
    
      public function tarefas_custos_excluir() {
        $this->autentica();
        $this->setTabela("projeto_tarefa_custos");
        parent::excluir();
        redirect("projeto/tarefas_custos/".$this->input->get("codigo_custo"));
    }
    
    public function tarefas_custos_cadastrar($indice){
        $this->autentica();
        $this->setTabela("projeto_tarefa_custos");
        $data["codigo_tarefa"] = $indice;
        $data["codigo_usuario"] = $this->codigo_usuario;
        $data["data_lancamento"] = date("Y-m-d");
   
        $this->setData($data);
        
        $cadastrar = parent::cadastrar();
        
        redirect("projeto/tarefas_custos/".$indice);
    }
    
    
    public function projeto_prazos($indice){
       
        $this->autentica();
        $this->setTabela("alteracoes_prazos");
        // Definindo o título da janela
        $this->db->where("id_projeto",$indice);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao;
        $this->setTitulo("Alterações de prazos - ".$descricao_resumida);
        //$this->setUrl_novo(base_url()."projeto/tarefas_custos_cadastro/".$indice);
        $this->setBotao_novo(false);
        
        
        $this->setBotao_voltar(base_url()."projeto/abrir?id_projeto=".$indice);
        
        
        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Alterado em"),
            array("titulo" => "Titulo"),
            array("titulo" => "DT. Prev. Ini. Ant."),
            array("titulo" => "DT. Prev. Ini. Nov."),
            array("titulo" => "DT. Prev. Fim Ant."),
            array("titulo" => "DT. Prev. Fim Nov."),
            array("titulo" => "DT. Real. Ini. Ant."),
            array("titulo" => "DT. Real. Ini. Nov."),
            array("titulo" => "DT. Real. Fim. Ant."),
            array("titulo" => "DT. Real. Fim. Nov.")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
       
  
       
       $resultados = $this->db->query("select 
projeto_tarefa.titulo,
data_alteracao,
data_ini_prev_old,
data_ini_prev_new,
data_fin_prev_old,
data_fin_prev_new,
data_ini_real_old,
data_ini_real_new,
data_fin_real_old,
data_fin_real_new
from alteracoes_prazos 
INNER JOIN projeto_tarefa ON projeto_tarefa.id_tarefa = alteracoes_prazos.codigo_tarefa
where codigo_projeto = ".$indice."
order by data_alteracao")->result();
       
       
       
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
         
            //Carrega as informações que irá na tabela
         
            $dados[]["dados"] = array(
                L_label_data($item->data_alteracao, "d/m/Y"),
                $item->titulo,
                L_label_data($item->data_ini_prev_old, "d/m/Y"),
                L_label_data($item->data_ini_prev_new, "d/m/Y"),
                L_label_data($item->data_fin_prev_old, "d/m/Y"),
                L_label_data($item->data_fin_prev_new, "d/m/Y"),
                L_label_data($item->data_ini_real_old, "d/m/Y"),
                L_label_data($item->data_ini_real_new, "d/m/Y"),
                L_label_data($item->data_fin_real_old, "d/m/Y"),
                L_label_data($item->data_fin_real_new, "d/m/Y")
               
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);

        parent::index();
    }
    
     
     public function tarefas_custos($indice){
       
        $this->autentica();
        $this->setTabela("projeto_tarefa_custos");
        // Definindo o título da janela
        $this->db->where("id_tarefa",$indice);
        $descricao_resumida = $this->db->get("projeto_tarefa")->result()[0]->titulo;
        $this->setTitulo("Custos da tarefa ".$descricao_resumida);
        $this->setUrl_novo(base_url()."projeto/tarefas_custos_cadastro/".$indice);
        
        $nivel_no_projeto = $this->retorna_nivel_na_tarefa($indice);
        if($nivel_no_projeto==0){
            $this->setBotao_novo(false);
        }
        $this->setBotao_voltar(base_url()."projeto/tarefas_abrir?id_tarefa=".$indice);
        
        
        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Data"),
            array("titulo" => "Descrição"),
            array("titulo" => "R$ Lançado"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
       
       $this->db->where("codigo_tarefa",$indice);
      
       
       $resultados = $this->db->get("projeto_tarefa_custos")->result();
       
       
       
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            $indices = array(array("codigo_custo"=>$item->codigo_custo)); 
            //Carrega as informações que irá na tabela
            if($item->codigo_usuario==$this->codigo_usuario){
                $botoes=" ".L_Deletar($indices, "projeto/tarefas_custos_excluir");
            }

            $dados[]["dados"] = array(
                $item->codigo_custo,
                L_label_data($item->data_lancamento, "d/m/Y"),
                $item->descricao,
                number_format($item->valor_lancamento,2),
                $botoes
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);

        parent::index();
    }

    /**
     * Exclui a tarefa
     * @author Lucas Leandro de Moura
     */
    public function tarefas_excluir() {
        $this->autentica();
        $this->setTabela("projeto_tarefa");
        parent::excluir();
        redirect("projeto/tarefas/" . $this->input->get("cod_projeto"));
    }

    public function tarefas($indice) {

        $this->autentica();
        // Definindo o título da janela
        $this->db->where("id_projeto", $indice);
        $this->setTabela("projeto_tarefa");
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;
        $this->setTitulo("Tarefas do projeto " . $descricao_resumida);
        $this->setUrl_novo(base_url() . "projeto/tarefas_cadastro/" . $indice);

        $nivel_no_projeto = $this->retorna_nivel_no_projeto($indice);
        if ($nivel_no_projeto == 0) {
            $this->setBotao_novo(false);
        }
        $this->setBotao_voltar(base_url() . "projeto/abrir?id_projeto=" . $indice);


        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Titulo"),
            array("titulo" => "Responsável"),
            array("titulo" => "Prev. Termin."),
            array("titulo" => "Progresso"),
            array("titulo" => "Resolvido"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
        $this->db->select("projeto_tarefa.*,usuario.nome as nome_responsavel,projeto_fases.descricao as descricao_fase");
        $this->db->where("cod_projeto", $indice);
        $this->db->join("usuario", "usuario.id_usuario = projeto_tarefa.responsavel", "inner");
        $this->db->join("projeto_fases", "projeto_fases.id_fase_projeto = projeto_tarefa.fase_atual", "inner");

        //Aplica valores do filtro
        foreach ($this->session->userdata("filtros")[$this->tabela] as $key => $value) {
            if ($value != "") {
                $this->db->where($this->tabela . "." . $key, $value);
            }
        }



        $resultados = $this->db->get("projeto_tarefa")->result();



        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            $indices = array(array("cod_projeto" => $item->cod_projeto), array("id_tarefa" => $item->id_tarefa));
            //Carrega as informações que irá na tabela
            $botoes = L_Botao($indices, "projeto/tarefas_abrir", "fa fa-folder-open", "Ver informações desta tarefa");
            $this->db->select("count(*) as contagem");
            $this->db->where("cod_usuario_chefe", $this->codigo_usuario);
            $this->db->where("id_projeto",$item->cod_projeto);
            $contagem = $this->db->get("projeto")->result()[0]->contagem;
            if ($item->responsavel == $this->codigo_usuario || $contagem>0) {
                $botoes .= " " . L_Deletar($indices, "projeto/tarefas_excluir") . " " .
                        L_Editar($indices, "projeto/tarefas_editar");
            }
            if($item->progresso < 100){
                $cor = "progress-bar-danger";
            }else {
                $cor = "progress-bar-success";
            }
            $dados[]["dados"] = array(
                $item->id_tarefa,
                $item->titulo,
                $item->nome_responsavel,
                L_label_data($item->data_previsto_final, "d/m/Y"),
                L_progress_bar($item->progresso,$cor),
                L_boolean($item->resolvido),
                $botoes
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);

        parent::index();
    }

    public function gantt($indice) {
        $this->autentica();
        global $header_nav;
        $this->load->view("Includes/header");
        $this->load->view("Includes/header_nav", $header_nav);

        $dados = array();
        $dados["indice"] = $indice;
        $this->db->select("projeto_tarefa.*,usuario.nome as nome_responsavel");
        $this->db->where("cod_projeto", $indice);
        $this->db->join("usuario", "projeto_tarefa.responsavel=usuario.id_usuario", "inner");
        $this->db->order_by("data_previsto_inicial,data_previsto_final ASC");
        $dados["gantt"] = $this->db->get("projeto_tarefa")->result();
        
        $this->db->where("id_projeto",$indice);
        $this->db->join("usuario", "projeto.cod_usuario_chefe=usuario.id_usuario", "inner");
        $dados["projeto"] = $this->db->get("projeto")->result()[0];

        $this->load->view("projeto/gantt", $dados);
        $this->load->view("Includes/footer");
    }

    /**
     * @author Lucas Leandro de Moura
     * @global type $header_nav
     * @param type $indice
     */
    public function forum($indice) {
        $this->autentica();
        global $header_nav;
        $this->load->view("Includes/header");
        $this->load->view("Includes/header_nav", $header_nav);

        $dados = array();
        $dados["indice"] = $indice;
        $dados["projeto"] = $this->get_projeto($indice)[0];
        $this->db->select("projeto_forum.*,usuario.nome");
        $this->db->join("usuario", "projeto_forum.codigo_usuario = usuario.id_usuario", "inner");
        $this->db->where("codigo_projeto", $indice);
        $this->db->order_by("codigo_conversa asc");
        $dados["conversa"] = $this->db->get("projeto_forum")->result();

        $this->load->view("projeto/forum", $dados);
        $this->load->view("Includes/footer");
    }

    /**
     * Retorna todos os envolvidos em um projeto
     * @param type $codigo_projeto
     * @return type
     */
    private function get_envolvidos_projeto($codigo_projeto) {
        $this->db->where("cod_projeto", $codigo_projeto);
        $this->db->join("projeto", "cod_projeto=id_projeto", "inner");
        $this->db->join("usuario", "cod_usuario=id_usuario", "inner");
        $retorno = $this->db->get("projeto_membros")->result();
        return $retorno;
    }

    private function get_projeto($codigo_projeto) {
        $this->db->where("id_projeto", $codigo_projeto);
        $retorno = $this->db->get("projeto")->result();
        return $retorno;
    }

    /**
     * @author Lucas Leandro de Moura
     * @param type $codigo_projeto
     */
    public function forum_gravar($codigo_projeto) {
        $this->autentica();
        $this->setTabela("projeto_forum");
        $data["codigo_usuario"] = $this->codigo_usuario;
        $data["mensagem"] = $this->input->post("mensagem");
        $data["codigo_projeto"] = $codigo_projeto;
        $this->setData($data);

        parent::cadastrar();

        //Envia e-mails para os envolvidos do fórum
        foreach ($this->get_envolvidos_projeto($codigo_projeto) as $item) {
            $assunto = "Projeto " . $item->descricao_resumida . " | Nova mensagem no fórum";
            $mensagem = $item->nome . ", " . $this->session->userdata('nome') . " adicionou uma nova mensagem no fórum do projeto <b>" . $item->descricao_resumida . "</b>.";
            $mensagem .= "<br>Segue endereço para visualizar a publicação: ";
            $mensagem .= "<br> ";
            $mensagem .= $data["mensagem"];
            $mensagem .= "<br> ";
            $mensagem .= "<br> " . base_url() . "projeto/forum/" . $codigo_projeto;
            $mensagem .= "<br> ";
            $mensagem .= "<br> E-mail automático, não responder";
            $this->enviar_email($item->email, $assunto, $mensagem);
        }
        redirect("projeto/forum/" . $codigo_projeto);
    }

    public function cadastrar() {
        $this->autentica();
        $this->setTabela("projeto");

        $cadastrar = parent::cadastrar();

        if (!$this->input->get() && $cadastrar["erro"] == 1) {
            $this->db->select_max('id_projeto');
            $novo_id = $this->db->get("projeto")->result()[0]->id_projeto;
            $data = array();
            $data["cod_usuario"] = $this->codigo_usuario;
            $data["cod_projeto"] = $novo_id;
            $data["nivel"] = 2;

            $this->db->insert("projeto_membros", $data);

            redirect("projeto/abrir/" . $novo_id);
        } else {
            redirect("Projeto");
        }
    }

    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->autentica();

        $this->setTitulo("Editar projeto");
        $this->setAcao("projeto/cadastrar?id_projeto=" . $this->input->get("id_projeto"));

        $this->db->where("id_projeto", $this->input->get("id_projeto"));
        $this->setDados($this->db->get("projeto")->result());

        $indices = array("codigo_empresa" => $this->getEmpresasComAcessos(""));
        parent::cadastro($indices);
    }

    /**
     * Tela assim que se abre o projeto
     * @author Lucas Leandro de Moura
     * @global type $header_nav
     */
    public function abrir($indice = "") {
        $this->autentica();
        global $header_nav;
        $dados = array();
        if ($indice != "") {
            $dados["id_projeto"] = $indice;
        } else {
            $dados["id_projeto"] = $this->input->get("id_projeto");
        }

        $this->load->view("Includes/header");
        $this->load->view("Includes/header_nav", $header_nav);


        $this->db->where("id_projeto", $dados["id_projeto"]);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;
        $dados["titulo"] = "Ações do projeto " . $descricao_resumida;


        $this->load->view("projeto/index", $dados);
        $this->load->view("Includes/footer");
    }

    private function retorna_nivel_no_projeto($id_projeto) {
        $this->autentica();
        $this->db->where("cod_projeto", $id_projeto);
        $this->db->where("cod_usuario", $this->codigo_usuario);
        $nivel_usuario_logado = 1;
        foreach ($this->db->get("projeto_membros")->result() as $infoUser) {
            $nivel_usuario_logado = $infoUser->nivel;
        }

        $this->db->where("id_projeto", $id_projeto);
        $this->db->where("cod_usuario_chefe", $this->codigo_usuario);
        foreach ($this->db->get("projeto")->result() as $infoUser) {
            $nivel_usuario_logado = 2;
        }
        return $nivel_usuario_logado;
    }

    private function retorna_nivel_na_tarefa($id_tarefa) {
        $this->autentica();
        $this->db->where("codigo_tarefa", $id_tarefa);
        $this->db->where("codigo_usuario", $this->codigo_usuario);
        $nivel_usuario_logado = 1;
        foreach ($this->db->get("projeto_tarefa_envolvidos")->result() as $infoUser) {
            $nivel_usuario_logado = $infoUser->nivel;
        }

        $this->db->where("id_tarefa", $id_tarefa);
        $this->db->where("responsavel", $this->codigo_usuario);
        foreach ($this->db->get("projeto_tarefa")->result() as $infoUser) {
            $nivel_usuario_logado = 2;
        }
        return $nivel_usuario_logado;
    }

    public function equipe_tarefa($id_tarefa, $codigo_membro = "") {
        $this->autentica();
        global $header_nav;
        $this->load->view("Includes/header");
        $this->load->view("Includes/header_nav", $header_nav);
        $dados = array();
        $dados["id_tarefa"] = $id_tarefa;

        $nivel_usuario_logado = $this->retorna_nivel_na_tarefa($id_tarefa);

        $dados["botao_novo"] = false;
        if ($nivel_usuario_logado == 2) {
            $dados["botao_novo"] = true;
        }

        $this->db->where("id_tarefa", $id_tarefa);
        $descricao_resumida = $this->db->get("projeto_tarefa")->result()[0]->titulo;
        $dados["titulo"] = $descricao_resumida;
        $dados["codigo_projeto"] = $this->db->get("projeto_tarefa")->result()[0]->cod_projeto;



        $cabecalho = array(
            array("titulo" => "Username"),
            array("titulo" => "Nome"),
            array("titulo" => "E-mail"),
            array("titulo" => "Nível"),
            array("titulo" => "Ações")
        );

        $dados_tabela = array();

        $this->db->select("*,CASE projeto_tarefa_envolvidos.nivel WHEN 0 THEN 'Visualizador' WHEN 1 THEN 'Colaborador' WHEN 2 THEN 'Administrador' END as nivel_descricao");
        $this->db->join("usuario", "codigo_usuario=id_usuario", "inner");
        $this->db->where("codigo_tarefa", $id_tarefa);

        $membros = $this->db->get("projeto_tarefa_envolvidos")->result();
        foreach ($membros as $item) {
            $indices = array(array("codigo_tarefa" => $item->codigo_tarefa), array("codigo_usuario" => $item->codigo_usuario));
            $botoes = "";
            //Se for administrador
            if ($nivel_usuario_logado == 2) {
                $botoes = L_Deletar($indices, "projeto/excluir_equipe_tarefa/");
            }
            $dados_tabela[]["dados"] = array(
                $item->login,
                $item->nome,
                $item->email,
                $item->nivel_descricao,
                $botoes
            );
        }

        $dados["tabela"] = $this->table($cabecalho, $dados_tabela);
        $sql_membros = "select concat(usuario.nome,' - ',usuario.email) as nome,
            cod_usuario 
            from projeto_membros
            inner join usuario on usuario.id_usuario = projeto_membros.cod_usuario 
            and projeto_membros.cod_projeto = " . $dados["codigo_projeto"] . "
            where nivel = 1 or nivel = 2
            order by concat(usuario.nome,' - ',usuario.email)";
        $dados["membros"] = $this->db->query($sql_membros)->result();


        $this->load->view("tarefa/equipe", $dados);
        $this->load->view("Includes/footer");
    }

    public function equipe($id_projeto, $codigo_membro = "") {
        $this->autentica();
        global $header_nav;
        $this->load->view("Includes/header");
        $this->load->view("Includes/header_nav", $header_nav);
        $dados = array();
        $dados["id_projeto"] = $id_projeto;

        $nivel_usuario_logado = $this->retorna_nivel_no_projeto($id_projeto);

        $dados["botao_novo"] = false;
        if ($nivel_usuario_logado == 2) {
            $dados["botao_novo"] = true;
        }

        $this->db->where("id_projeto", $id_projeto);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;
        $dados["titulo"] = $descricao_resumida;



        $cabecalho = array(
            array("titulo" => "Username"),
            array("titulo" => "Nome"),
            array("titulo" => "E-mail"),
            array("titulo" => "Nível"),
            array("titulo" => "Ações")
        );

        $dados_tabela = array();

        $this->db->select("*,CASE projeto_membros.nivel WHEN 0 THEN 'Visualizador' WHEN 1 THEN 'Colaborador' WHEN 2 THEN 'Administrador' END as nivel_descricao");
        $this->db->join("usuario", "cod_usuario=id_usuario", "inner");
        $this->db->where("cod_projeto", $id_projeto);

        $membros = $this->db->get("projeto_membros")->result();
        foreach ($membros as $item) {
            $indices = array(array("cod_projeto" => $item->cod_projeto), array("cod_usuario" => $item->cod_usuario));
            $botoes = "";
            //Se for administrador
            if ($nivel_usuario_logado == 2) {
                $botoes = L_Deletar($indices, "projeto/excluir_equipe/");
            }
            $dados_tabela[]["dados"] = array(
                $item->login,
                $item->nome,
                $item->email,
                $item->nivel_descricao,
                $botoes
            );
        }

        $dados["tabela"] = $this->table($cabecalho, $dados_tabela);




        $this->load->view("projeto/equipe", $dados);
        $this->load->view("Includes/footer");
    }

    public function equipe_tarefa_adicionar($id_tarefa) {
        $this->autentica();
        $this->load->helper('string');

        $codigo_usuario = $this->input->post("codigo_usuario");
        $nivel = $this->input->post("nivel");

        $this->db->where("id_tarefa", $id_tarefa);
        $descricao_resumida = $this->db->get("projeto_tarefa")->result()[0]->titulo;

        $data["codigo_usuario"] = $codigo_usuario;
        $data["codigo_tarefa"] = $id_tarefa;
        $data["nivel"] = $nivel;

        $this->db->insert("projeto_tarefa_envolvidos", $data);

        redirect("projeto/equipe_tarefa/" . $id_tarefa);
    }

    public function equipe_adicionar($cod_projeto) {
        $this->autentica();
        $this->load->helper('string');

        $email = $this->input->post("email");
        $nivel = $this->input->post("nivel");
        $this->db->where("id_projeto", $cod_projeto);
        $descricao_resumida = $this->db->get("projeto")->result()[0]->descricao_resumida;

        do {
            $this->db->where("email", $email);
            $usuario = $this->db->get("usuario")->result();
            $cont = 0;
            foreach ($usuario as $item) {
                //Valida se o usuário já não se encontra neste projeto, se ele se encontra deleta o mesmo primeiro
                $this->db->where("cod_usuario", $item->id_usuario);
                $this->db->where("cod_projeto", $item->cod_projeto);
                $this->db->delete("projeto_membros");

                $data["cod_usuario"] = $item->id_usuario;
                $data["cod_projeto"] = $cod_projeto;
                $data["nivel"] = $nivel;

                $this->db->insert("projeto_membros", $data);



                $mensagem = "Olá! Você foi adicionado a um projeto. <br>"
                        . "<b>Projeto:</b> " . $descricao_resumida . "<br>"
                        . "<b>Link:</b> " . base_url() . "projeto/abrir?id_projeto=" . $cod_projeto . "<br>";

                $this->enviar_email($item->email, "Projeto: " . $descricao_resumida, $mensagem);

                $cont++;
            }

            //Se não encontrou ninguém cria um usuário
            if ($cont == 0) {
                $dataUser = array();
                $dataUser["email"] = $email;
                $dataUser["login"] = $email;
                $dataUser["nome"] = " ";
                $dataUser["senha"] = random_string('alnum', 7);
                $this->db->insert("usuario", $dataUser);
                //Disparo um e-mail para o usuário com a nova senha


                $mensagem = "Olá! Você foi adicionado a um projeto. <br>"
                        . "<b>Usuario:</b> " . $dataUser["login"] . "<br>"
                        . "<b>Senha:</b> " . $dataUser["senha"] . "<br>"
                        . "<b>Projeto:</b> " . $descricao_resumida . "<br>"
                        . "<b>Link:</b> " . base_url() . "projeto/abrir?id_projeto=" . $cod_projeto . "<br>";

                $this->enviar_email($dataUser["email"], "Projeto: " . $descricao_resumida, $mensagem);
            }
        } while ($cont == 0);

        redirect("projeto/equipe/" . $cod_projeto);
    }

    public function excluir() {
        $this->setTabela("projeto");
        parent::excluir();
        redirect("projeto");
    }

    public function excluir_equipe() {
        $this->setTabela("projeto_membros");
        parent::excluir();
        redirect("projeto/equipe/" . $this->input->get("cod_projeto"));
    }

    public function excluir_equipe_tarefa() {
        $this->setTabela("projeto_tarefa_envolvidos");
        $erro = parent::excluir();

        redirect("projeto/equipe_tarefa/" . $this->input->get("codigo_tarefa"));
    }

}
