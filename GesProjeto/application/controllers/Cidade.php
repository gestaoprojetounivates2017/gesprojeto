<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cidade extends MY_Controller {
    protected $tabela = "cidade";
    
    public function index() {
        //Defino o título da página
        $this->setTitulo("Cidades");
        $this->setTabela("cidade");
        $this->setFiltros(true);
        $cabecalho = array(
            array("titulo"=>"Código"),
            array("titulo"=>"Estado"),
       
            array("titulo"=>"Descrição"),
            array("titulo"=>"Ações")
        );
        //Defino o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        
        
        $dados = array();
        $this->db->select("cidade.*,estado.sgl_estado as estado");
        $this->db->join("estado","cidade.cod_estado=estado.codigo_estado","inner");
        foreach($this->session->userdata("filtros")[$this->tabela] as $key=>$value){
            if($value!=""){
                $this->db->where($this->tabela.".".$key,$value);
            }
        }
        $resultados = $this->db->get("cidade")->result();
        foreach($resultados as $item){
          $indices = array(array("id_cidade"=>$item->id_cidade));  
            
          $dados[]["dados"] = array(
              $item->id_cidade,
              $item->estado,
           
              $item->descricao_resumida,
              L_Deletar($indices, "cidade/excluir")." ".
              L_Editar($indices, "cidade/editar")." ".
              L_Abrir($indices, "cidade/abrir")
              );  
        }
        //Defino os dados da tabela
        $this->setLinhas($dados);
       
        
        parent::index();
    }
    
    public function cadastro() {
        $this->setTabela("cidade");
        $this->setTitulo("Cadastro de cidade");
        $this->setAcao("cidade/cadastrar");
        parent::cadastro();
    }
    
    public function cadastrar() {
        $this->autentica();
        $this->setTabela("cidade");
        parent::cadastrar();
        redirect("cidade");
    }
    
    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->setTabela("cidade");
        $this->setTitulo("Cadastro de cidade");
        
        $this->db->where("id_cidade",$this->input->get("id_cidade"));
        $this->setDados($this->db->get("cidade")->result());
        
        parent::cadastro();
    }
    
}
            

        



