<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Erro404 extends MY_Controller {
    //Utilização padrão
    function index(){
        $this->autentica();
        $this->load->view("Includes/header");
        

        global $header_nav;
        
        $this->load->view("Includes/header_nav",$header_nav);
        $this->load->view("errors/erro_404");
        $this->load->view("Includes/footer");
    }
    
    
    
}