<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Lucas Leandro de Moura
 */
//Deve mudar o nome da classe para o nome do arquivo
class Empresa extends MY_Controller {
    protected $tabela = "empresa";


    //Tela inicial
    public function index() {
        $this->autentica();
        // Definindo o título da janela
        $this->setTitulo("Empresa");
        //Define o cabecalho da lista
        $cabecalho = array(
            array("titulo" => "Código"),
            array("titulo" => "Empresa"),
            array("titulo" => "CNPJ"),
            array("titulo" => "Cidade"),
            array("titulo" => "Ações")
        );

        // Definindo o cabecalho da tabela
        $this->setCabecalho($cabecalho);
        // Defino os dados da lista
        $dados = array();
        //Seleciona todos os registros da tabela
        $this->db->join("cidade","cidade.id_cidade = empresa.codigo_cidade","inner");
        //Define quais empresas deverão aparecer
        $this->db->where($this->getEmpresasComAcessos("empresa.cod_empresa"));   
        foreach($this->session->userdata("filtros")[$this->tabela] as $key=>$value){
            if($value!=""){
                $this->db->where($this->tabela.".".$key,$value);
            }
        }
        
        $resultados = $this->db->get("empresa")->result();
        // Percorrendo "resultados" e mostrando todos os registros
        foreach ($resultados as $item) {
            $indices = array(array("cod_empresa" => $item->cod_empresa));
            //Carrega as informações que irá na tabela
            $botoes = "";
            if($item->usuario_administrador==$this->codigo_usuario){
                $botoes = L_Deletar($indices, "empresa/excluir") . " " .
                L_Editar($indices, "empresa/editar");
            }
            
            $dados[]["dados"] = array(
                $item->cod_empresa,
                $item->nome_empresa,
                $item->cnpj,
                $item->descricao_resumida,
                $botoes ." ".
                L_Botao($indices, "empresa/empresa_setores", "fa-puzzle-piece ", "Setores")
            );
        }
        //Definindo os dados da tabela
        $this->setLinhas($dados);

        parent::index();
    }
    
    
    public function empresa_setores(){
        $this->autentica();
         global $infoSessao;
         $infoSessao["filtros"]["empresa_setores"]["codigo_empresa"] = $this->input->get("cod_empresa");
         $this->session->set_userdata($infoSessao);
         redirect("empresa_setores");        
    }
    
    
    public function excluir() {
        $this->setTabela("empresa");
        parent::excluir();
        redirect("empresa");
    }


    /**
     * Tela de cadastro
     */
    public function cadastro() {
        $this->autentica();
        //Tabela do banco
        $this->setTabela("empresa");
        //Titulo da tela
        $this->setTitulo("Cadastro de empresa");
        //Controlar que esta a operação de gravar
        $this->setAcao("empresa/cadastrar");
        parent::cadastro();
    }
    /**
     * Ação de cadastrar
     */
    public function cadastrar() {
        $this->autentica();
        $this->setTabela("empresa");
        //Adiciono valores ao insert
        $data["usuario_administrador"] = $this->codigo_usuario;
        $this->setData($data);
        
        
        parent::cadastrar();
        //Redireciona
        redirect("empresa");
    }

    /**
     * Realiza a edição do cadastro
     * @author Lucas Moura <lmoura@universo.univates.br>
     * @param type $indice
     */
    public function editar() {
        $this->setTabela("empresa");
        $this->setTitulo("Cadastro de empresa");

        $this->db->where("cod_empresa", $this->input->get("cod_empresa"));
        $this->setDados($this->db->get("empresa")->result());
        $this->setAcao("empresa/cadastrar?cod_empresa=".$this->input->get("cod_empresa"));

        parent::cadastro();
    }
}
