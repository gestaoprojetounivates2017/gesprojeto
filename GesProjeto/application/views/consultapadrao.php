<style type="text/css">
    #page-wrapper {
    position: inherit;
    margin: 0 0 0 0;
    padding: 0 30px;
    border-left: 1px solid #e7e7e7;
  }
    </style>
<div id="page-wrapper">

    <div class="row">

        <div class="panel panel-default">
            
            <div class="panel-body">

                <div class="col-lg-12 table-responsive">
                    <?php if ($tabela) { ?>
                        <?= $tabela ?>
                    <?php } ?>
                </div>     

                
                    <div class="col-sm-offset-2 col-sm-10 text-right">
                        <a id="link_sair" title="Fechar" href="javascript:top.window.janela.close();" class="btn btn-info"><i class="fa fa-close"></i>Fechar</a>
                    </div>
                
            </div>
        </div>

    </div>
</div>
