
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h3 class="header_title"><?= $titulo ?></h3>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>


    <!-- guia !-->
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                        <i class="fa fa-edit"></i>Cadastros
                    </a>
                </li>
            
            </ul>
        </div>
    </div>
    <!-- end guia !-->

    <div class="row">
        <div class="col-lg-12">
            <div class="tab-content">
                <div class="tab-pane active" id="home" role="tabpanel">
                    <div class="row">


                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-user"></i>Equipe da tarefa</h3>
                                    <p>Cadastre aqui a equipe desta tarefa</p>
                                    <a href="<?= base_url() ?>projeto/equipe_tarefa/<?= $id_tarefa ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-paperclip"></i>Anexos</h3>
                                    <p>Documentos compartilhados relacionados ao projeto</p>
                                    <a href="<?= base_url() ?>projeto/arquivos/<?= $cod_projeto ?>/<?= $id_tarefa ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>


                       <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="fa fa-money"></i>Custos</h3>
                                    <p>Gerencie os custos envolvidos nesta tarefa</p>
                                    <a href="<?= base_url() ?>projeto/tarefas_custos/<?= $id_tarefa ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>



                    </div>


                    <div class="row">

                        


                    </div>


                </div>

              
            </div>
        </div>
    </div>




    <div class="col-sm-offset-2 col-sm-10 text-right">
        <a id="link_sair" title="Voltar" href="<?= base_url() ?>projeto/tarefas/<?=$cod_projeto?>" class="btn btn-info">Voltar</a>
    </div>
</div>


</div>
