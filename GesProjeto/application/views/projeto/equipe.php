
 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                    <h1>Equipe do projeto <?=$titulo?></h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
     
     
    
     <div class="row">
         <?php if($botao_novo==true) { ?>
                <div class="col-sm-offset-2 col-sm-10 text-right">
                    <button type="button" class="btn btn-circle btn-success" data-toggle="modal" data-target="#myModal">+</button>
                </div>
         <?php } ?>
        
         <div class="col-lg-12 table-responsive">
                        <?php if($tabela){?>
                         <?=$tabela?>
                       <?php } ?>
        </div>    
         
         
         
         
        <div class="col-sm-offset-2 col-sm-10 text-right">
            <a id="link_sair" title="Voltar" href="<?=base_url()?>projeto/abrir?id_projeto=<?=$id_projeto?>" class="btn btn-info">Voltar</a>
        </div>
     </div>
     
        
 </div>







<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Adicionar membro ao projeto <?=$titulo?></h4>
      </div>
      <div class="modal-body">
          <p>

              <?php 
                print form_open(base_url()."projeto/equipe_adicionar/".$id_projeto);
                $extra = array(
                    "class"=>"form-control input-sm",
                    "name"=>"email",
                    "id"=>"email",
                    "required"=>"required",
                    "type"=>"email",
                    "placeholder"=>"Adicione um usuário através do e-mail",
                    "value"=>""
                );
                print form_label("E-mail do usuário")." ".form_input($extra);
                
                
                 $extra = array(
                    "class"=>"form-control input-sm",
                    "name"=>"nivel",
                    "id"=>"nivel",
                    "required"=>"required",
                    "type"=>"number",
                    "autocomplete"=>"off",
                    "placeholder"=>"",
                    "value"=>""
                );
                $opcoes = array();
                $opcoes[0] = "Visulizador";
                $opcoes[1] = "Colaborador";
                $opcoes[2] = "Administrador";
                print form_label("Nível")." ". form_dropdown($extra,$opcoes);
                
                
                $extra = array(
                    "class"=>"btn btn-success"
                );
                print "<div class='text-right'>";
                    print form_submit("btn_salvar","Salvar",$extra);
                print "</div>";
                print form_close();
              ?> 
              
          </p>
      </div>
      
    </div>

  </div>
</div>