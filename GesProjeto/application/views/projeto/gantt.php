<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Gantt projeto <?=$projeto->descricao_resumida?></h1>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>



    <div class="row">
        <div class="col-sm-12">

            <div style="position:relative" class="gantt" id="GanttChartDIV"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-10 text-right">
            <a id="link_sair" title="Voltar" href="<?= base_url() ?>projeto/abrir?id_projeto=<?= $indice ?>" class="btn btn-info">Voltar</a>
        </div>
    </div>




    <script type="text/javascript">

        var g = new JSGantt.GanttChart('g', document.getElementById('GanttChartDIV'), 'day');
        g.setShowRes(0); // Show/Hide Responsible (0/1)
        g.setShowDur(0); // Show/Hide Duration (0/1)
        g.setShowComp(1); // Show/Hide % Complete(0/1)
        g.setCaptionType('Resource');  // Set to Show Caption
        g.setShowStartDate(0); // Show/Hide Start Date(0/1)
        g.setShowEndDate(0); // Show/Hide End Date(0/1)
        g.setDateInputFormat('dd/mm/yyyy')  // Set format of input dates ('mm/dd/yyyy', 'dd/mm/yyyy', 'yyyy-mm-dd')
        g.setDateDisplayFormat('dd/mm/yyyy') // Set format to display dates ('mm/dd/yyyy', 'dd/mm/yyyy', 'yyyy-mm-dd')

        if (g) {
  g.AddTaskItem(new JSGantt.TaskItem(1,   '<?=$projeto->descricao_resumida?>',     '',           '',          'ggroupblack',  '',                 0, '<?=$projeto->nome?>',    0,     1,      0,       1,     '',      '',      '', g ));

<?php

$chaves = array(""=>"");
$contador = 1;
foreach($gantt as $c){
    $chaves[$c->id_tarefa] = "1".$contador;
}
$contador=1;

foreach ($gantt as $item) {
    $resolvido = $item->resolvido;
    if ($resolvido) {
        $resolvido = 1;
    } else {
        $resolvido = 0;
    }
    
    ?>
     g.AddTaskItem(new JSGantt.TaskItem(1<?=$contador?>,  
     '<?= $item->titulo ?>',         
     '<?= date("d/m/Y", strtotime($item->data_previsto_inicial)) ?>',
     '<?= date("d/m/Y", strtotime($item->data_previsto_final)) ?>', 
     '#31d853',   
     '<?= base_url()?>projeto/tarefas_abrir?cod_projeto=<?=$item->cod_projeto?>&id_tarefa=<?=$item->id_tarefa?>',
     0, 
     '<?= $item->nome_responsavel ?>',   
     <?= $item->progresso ?>,   
     0,      
     1,       
     1,     
     '<?=$chaves[$item->dependencia]?>',           
     g));


    <?php

    $contador++;
}
?>


            g.Draw();
            g.DrawDependencies();


        } else
        {
            alert("not defined");
        }
    </script>