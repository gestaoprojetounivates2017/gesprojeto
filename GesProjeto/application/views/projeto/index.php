
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h3 class="header_title"><?= $titulo ?></h3>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>


    <!-- guia !-->
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                        <i class="fa fa-edit"></i>Cadastros
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#report" role="tab">
                        <i class="fa fa-bar-chart-o"></i>Análises
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#config" role="tab">
                        <i class="fa fa-gears fa-fw"></i>Configurações
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- end guia !-->

    <div class="row">
        <div class="col-lg-12">
            <div class="tab-content">
                <div class="tab-pane active" id="home" role="tabpanel">
                    <div class="row">


                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-user"></i>Equipe do projeto</h3>
                                    <p>Cadastre aqui a equipe pertencente a este projeto</p>
                                    <a href="<?= base_url() ?>projeto/equipe/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-paperclip"></i>Anexos</h3>
                                    <p>Documentos compartilhados relacionados ao projeto</p>
                                    <a href="<?= base_url() ?>projeto/arquivos/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-tasks"></i>Tarefas</h3>
                                    <p>Gerêncie todas as tarefas envolvidas neste projeto</p>
                                    <a href="<?= base_url() ?>projeto/tarefas/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>



                    </div>


                    <div class="row">

                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-comment"></i>Fórum</h3>
                                    <p>Debata assuntos relacionados a este projeto de forma geral</p>
                                    <a href="<?= base_url() ?>projeto/forum/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
                <div class="tab-pane" id="report" role="tabpanel">

                    <div class="row">


                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-plus"></i>Termo Abertura de Projeto</h3>
                                    <p>Gere o seu termo de abertura de projeto</p>
                                    <a href="<?= base_url() ?>relatorio/Termo_Abertura_Projeto/<?= $id_projeto ?>" target="_blank" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="fa fa-tasks"></i>Relação de tarefas</h3>
                                    <p>Imprima uma relação das tarefas deste projeto</p>
                                    <a href="<?= base_url() ?>relatorio/tarefas_projeto/<?= $id_projeto ?>" target="_blank" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class=" glyphicon glyphicon-object-align-top"></i>Gantt</h3>
                                    <p>Visualize de forma gráfica todas as tarefas deste projeto</p>
                                    <a href="<?= base_url() ?>projeto/gantt/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>

</div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="fa fa-money"></i>Custos</h3>
                                    <p>Gerencie os custos envolvidos neste projeto</p>
                                    <a href="<?= base_url() ?>projeto/custos/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="fa fa-clock-o"></i>Alterações de prazos</h3>
                                    <p>Visualize o histórico de alterações de prazos deste projeto</p>
                                    <a href="<?= base_url() ?>projeto/projeto_prazos/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>
                        </div>




                    


                </div>
                <div class="tab-pane" id="config" role="tabpanel">
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-flag"></i>Status do projeto</h3>
                                    <p>Os Status que este projeto terá</p>
                                    <a href="<?= base_url() ?>projeto/status/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3><i class="glyphicon glyphicon-edit"></i>Fases do projeto</h3>
                                    <p>Etapas deste projeto</p>
                                    <a href="<?= base_url() ?>projeto/fases/<?= $id_projeto ?>" class="btn btn-primary">Abrir</a>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>




    <div class="col-sm-offset-2 col-sm-10 text-right">
        <a id="link_sair" title="Voltar" href="<?= base_url() ?>projeto" class="btn btn-info">Voltar</a>
    </div>
</div>


</div>
