<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <h1>Fórum do projeto <?=$projeto->descricao_resumida?></h1>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-sm-offset-2 col-sm-10 text-right">
            <a id="link_sair" title="Voltar" href="<?= base_url() ?>projeto/abrir?id_projeto=<?= $indice ?>" class="btn btn-info">Voltar</a>
        </div>
    </div>

    <div class="row">
       
     
    </div>
    
    
    <div class="row">
        <div class="col-sm-12" style="height: 450px; overflow-y: auto;">
            <?php
                $cont = 0;
                $cor = "";
                foreach($conversa as $item) {
                    if($cont%2==0){
                        $cor = "bg-info";
                    }
                    $cont++;
            ?>
            
            <div class="media <?=$cor?>">
                <div class="media-body">
                    <div class="media-heading">
                        <h5><b><?=$item->nome?></b>
                            <span style="COLOR: #c2c2c2; font-size: 12px;"> respondeu às <?= date("d/m/Y H:i:s",strtotime($item->data_mensagem)) ?></span>
                        </h5>
                        <div style="font-size: 12px; min-height: 50px;">
                            <?=$item->mensagem?>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <?php
                }
                
              
            ?>
            
        </div>
        
        <hr>
        
         </div>
    
    <div class="row">
        <div class="media">
            <?= form_open(base_url()."projeto/forum_gravar/".$indice) ?>
            <div class="media-body">
                <div class="col-md-10 col-sm-9 col-lg-10 col-xs-9">
                    <?php 
                        $estilo = array(
                        'class' => 'form-control input-sm col-md-12 col-sm-12 col-lg-12 col-xs-12',
                        'style'=>'HEIGHT: 85px;',
                        'rows'=>'3',
                        'placeholder'=>'Responder ao fórum',
                        'cols'=>'50',
                        'name' => 'mensagem'
                        );
                    
                    ?>
                    <?= form_textarea($estilo) ?>
                </div>
                <div class="col-md-2 col-sm-3 col-lg-2 col-xs-3">
                    <?php 
                        $estilo = array(
                        'class' => 'btn btn-primary btn-lg col-md-12 col-sm-12 col-lg-12 col-xs-12',
                        'value' => 'Gravar',
                        'style'=>'HEIGHT: 85px;'
                        );
                    
                    ?>
                    <?= form_submit($estilo) ?>
                </div>
            </div>
            <?= form_close() ?>
        </div>
        </div>
   
    



