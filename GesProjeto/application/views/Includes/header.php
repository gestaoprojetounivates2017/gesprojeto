<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Angra - Software Planejamento</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>Includes/TemplateSB/sb_admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=base_url()?>Includes/TemplateSB/sb_admin/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=base_url()?>Includes/TemplateSB/sb_admin/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=base_url()?>Includes/TemplateSB/sb_admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="<?=base_url()?>Includes/TemplateSB/sb_admin/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?=base_url()?>Includes/TemplateSB/sb_admin/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    
    <!-- DataPicker Responsive CSS -->
    <link href="<?=base_url()?>Includes/datapicker/css/bootstrap-datepicker3.css" rel="stylesheet">
    
     <!-- Gantt -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>Includes/jsgantt/jsgantt.css" />
    <script language="javascript" src="<?=base_url()?>Includes/jsgantt/jsgantt.js"></script>
    
    <link rel="stylesheet" href="<?=base_url()?>Includes/Janelas/Janelas.css"> 
    <link rel="stylesheet" href="<?=base_url()?>Includes/thema.css">  
    
    <script type="text/javascript">
    /**
     * Abre a tela de um novo cadastro
     * @returns {undefined}
     */
    function Novo(url = ""){
        if(url==""){
            url = "<?=base_url()?><?=uri_string()?>/cadastro";
        }
        location.href = url;
    }
    
    
    function Excluir(url){
        if(confirm("Você deseja excluir este registro?")){
            location.href = url;
        }
    }
    
    
    function Consulta(codigo,nome_campo){
       
       var url = "<?=base_url()?>/consulta/abrir/"+codigo;
        
        
        top.window.janela = new Janela("novo", url, "Consulta Padrão",700,340);
        top.window.janela.seleciona = function (valor,campo) {
             
            document.getElementById(campo).value = valor;
              
                top.window.janela.close();
        }
        //top.window.janela.onCloseRefresh(window);
        
        //top.window.janela.autoSize();
        top.window.janela.show();
    }
   
    
    
    
   
    </script>
  
</head>
<body>
    
  
 
                
                
    
        
                

  
    
  