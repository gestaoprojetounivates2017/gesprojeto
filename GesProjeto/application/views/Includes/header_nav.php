<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url(); ?>">Angra</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?=base_url()?>Usuarios/meus_dados"><i class="fa fa-user fa-fw"></i> Meus dados</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li><a href="<?= base_url(); ?>Login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                
                </li>
            </ul>
        

            
            
                <!--Menus -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                         
                        <!-- /.Cadastros -->
                        <?php foreach ($categorias as $key=>$cat){ ?>
                            <li>
                                <a href="#"><i class="fa <?=$cat[0]->simbolo?>"></i><?=$cat[0]->descricao?><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                <?php
                                    foreach ($menus[$cat[0]->codigo_menu] as $item_menu){ 
                                ?>
                                        <li>
                                            <a href="<?= base_url()."".$item_menu->link; ?>" id="menu_<?=$item_menu->codigo_menu?>"><i class="fa <?=$item_menu->simbolo?> fa-fw"></i><?=$item_menu->descricao?></a>
                                        </li>
                                <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                                         
                        
                        
                    </ul>
                </div>
            </div>
        </nav>