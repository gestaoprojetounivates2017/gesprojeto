
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header-table">
                <div class="pull-left">
                    <h4 class="header_title"><?= $titulo ?></h4>
                </div>
                <div class="pull-right">
                    <?php
                    if($botao_novo){
                        $data = array(
                            "name" => "botao_novo",
                            "class" => "btn btn-success btn-circle",
                            "onclick" => "javascript:Novo('".$url_novo."');"
                        );
                        print form_button($data, "+");
                    }
                    ?>
                </div>
                
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>
    <div class="row">

        <?php
        if(sizeof($filtros)>0){
            print form_open($uri_completa . "?filtro=submit");
            ?>
        <div class="panel panel-default">
            <div class="panel-body">
    
    
 
        <?php
        
        
        foreach ($filtros as $valor_filtro) {
            ?>
            <div class="col-lg-4">
                <?php 
                
                print form_label($valor_filtro->rotulo);
                
                //Aplica os valores dos filtros
                $valor = "";
                
                if (array_key_exists(trim($valor_filtro->coluna), $this->session->userdata("filtros")[$tabela_db])) {
                    $valor = $this->session->userdata("filtros")[$tabela_db][trim($valor_filtro->coluna)];
                }

                $propriedades_filtro = array(
                        'class' => 'form-control input-sm',
                        'name' => trim($valor_filtro->coluna),
                        'id' => $valor_filtro->codigo_campo,
                        'type' => $valor_filtro->tipo_valor,
                        'placeholder' => $valor_filtro->ajuda,
                        'title' => $valor_filtro->ajuda,
                        'maxlength'=>$valor_filtro->tamanho_maximo_campo,
                        'value' => $valor
                );
                if ($valor_filtro->tipo_campo == 1 || $valor_filtro->tipo_campo == 2) {
                    print form_input($propriedades_filtro);
                }
                else if($valor_filtro->tipo_campo==3){
                    if ($valor_filtro->tipo_valor == "boolean") {
                        $lista = array('' => "",'t' => "Sim", 'f' => "Não");
                    } else {
                        $lista[""] = "";
                        
                        $this->db->select($valor_filtro->coluna_dependencia . " as id," . $valor_filtro->exibicao_dependencia . " as label");
                        $this->db->order_by($valor_filtro->exibicao_dependencia);
                        
                        if($valor_filtro->where_dependencia!=""){
                            foreach($indices as $in=>$in_val){
                               
                                if(strpos($valor_filtro->where_dependencia,"{".$in."}")){
                                    $valor_filtro->where_dependencia = str_replace("{".$in."}", $in_val, $valor_filtro->where_dependencia);
                                }
                               
                            }
                            $this->db->where($valor_filtro->where_dependencia);
                        }
                        
                        foreach ($this->db->get($valor_filtro->tabela_dependencia)->result() as $valores) {
                            $lista[trim($valores->id)] = trim($valores->label);
                        }
                    }
                    print form_dropdown($propriedades_filtro,$lista,$valor);
                }
                else if ($item->tipo_campo == 5) {
                                    
                                    if ($dados != "") {
                                        
                                        $data["value"] = L_label_data(trim($dados[trim($valor_filtro->coluna)]), "d/m/Y");
                                    }
                                                                        
                                    print "<div class='input-group date'>";
                                        print "<div class='input-group-addon'>";
                                            print "<i class='fa fa-calendar'></i>";
                                        print "</div>";
                                        print "<div id='sandbox-container'>";
                                            print form_input($data);
                                        print "</div>";
                                    print "</div>";
                                }

                ?>
            </div>
               
         
                
            
        <?php
        
        }
        
        ?>
                </div>
            <div class="panel-footer text-right">
                <?php

                print form_submit("", "Procurar","class='btn btn-default btn-sm'");
                print form_close();
                ?>
             </div>  
</div>
        
        <?php
        }
        ?>

    </div>
    
    
    
    <div class="row">
       
        <div class="panel panel-default">
            <?php if($erro!="") { ?>
                <div class="bg bg-danger text-center text-danger"><?=$erro?></div>
            <?php } ?>
            <div class="panel-body">
                
    
        <div class="col-lg-12 table-responsive">
            <?php if ($tabela) { ?>
                <?= $tabela ?>
            <?php } ?>
        </div>    
                
             <?php if ($botao_voltar!="") { ?>   
                <div class="col-sm-offset-2 col-sm-10 text-right">
                    <a id="link_sair" title="Voltar" href="<?=$botao_voltar?>" class="btn btn-info">Voltar</a>
                </div>
             <?php } ?>
</div>
            </div>

    </div>
</div>
