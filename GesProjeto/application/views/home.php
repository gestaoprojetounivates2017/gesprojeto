
 <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Seja bem vindo <?= $this->session->userdata("nome") ?></h3>
                    </div>
                 
                </div>
                
                <div class="row">
                    
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-thumbs-up fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?=$numero_tarefas?></div>
                                        <div>Tarefas em aberto sobre a sua responsabilidade</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-thumbs-down fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?=$numero_tarefas_atrasadas?></div>
                                        <div>Tarefas atrasadas sobre sua responsabilidade</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-clock-o fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?=$n_tarefas_time_ok?> dias</div>
                                        <div> de média concluídos depois do prazo</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?=$numero_projeto?></div>
                                        <div>projetos ativos em que você é membro</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                         <!-- BAR CHART -->
                            <div class="box box-success">
                              <div class="box-header with-border">
                                <h3 class="box-title">Tarefas por mês</h3>

                              </div>
                              <div class="box-body">
                                <div class="chart">
                                  <canvas id="barChart" style="height:230px"></canvas>
                                </div>
                              </div>
                              <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                    </div>
                </div>
            
            </div>
            
        </div>
 
 
 
 
 <!-- jQuery 3.1.1 -->
<script src="<?=base_url()?>Includes/plugins/jQuery/jquery-3.1.1.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?=base_url()?>Includes/plugins/chartjs/Chart.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>Includes/plugins/fastclick/fastclick.js"></script>

 <script>
  $(function () {
      
    var areaChartData = {
      labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
      datasets: [
        {
          label: "Previsão de conclusão",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [
        <?php
             $contagem = 1;                   
            foreach($grafico_previsto_final as $item) 
                     { 
                         print $item;
                         if($contagem<12){
                             print ",";
                         }
                         $contagem++;
                     }
             ?>
                                 ]
        },
        {
          label: "Concluídos",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php
             $contagem = 1;                   
            foreach($grafico_real_final as $item) 
                     { 
                         print $item;
                         if($contagem<12){
                             print ",";
                         }
                         $contagem++;
                     }
             ?>]
        }
      ]
    };  
      
     
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
  });
  </script>

 
  
