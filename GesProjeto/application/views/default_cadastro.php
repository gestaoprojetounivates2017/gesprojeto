<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            
            <div class="panel">
                <div class="panel panel-heading">
                   <h3><?= $titulo ?></h3> 
                </div>
                <div class="panel panel-body">
                    <?php if($erro!="") { ?>
                        <div class="bg bg-danger panel-danger text-center text-danger"><?=$erro?></div>
                    <?php } ?>
                    <?php
                    print form_open_multipart($acao, array("class" => "form"));


                    foreach ($campos as $item) {
                        $lista = array();
                        print ' <div class="row">';
                            print '<div class="col-md-12 col-sm-12 col-xs-12">';
                                print "<div class='form-group'>";
                                //Define a label
                                print form_label($item->rotulo, "label_" . $item->codigo_campo);
                                //Caracteristicas do campo
                                $data = array(
                                    'class' => 'form-control',
                                    'name' => trim($item->coluna),
                                    'id' => $item->codigo_campo,
                                    'type' => $item->tipo_valor,
                                    'placeholder' => $item->ajuda,
                                    'title' => $item->ajuda,
                                    'maxlength'=>$item->tamanho_maximo_campo,
                                    'value' => ''
                                );
                                if ($dados != "") {
                                    $data["value"] = trim($dados[trim($item->coluna)]);
                                }
                                if ($item->obrigatorio == "t") {
                                    $data["required"] = "required";
                                }


                                if ($item->ao_sair != "") {
                                    $data["onblur"] = $item->ao_sair;
                                }


                                if ($item->ao_carregar != "") {
                                    $data["onload"] = $item->ao_carregar;
                                }

                                //Desenha o campo
                                if ($item->tipo_campo == 1) {
                                    print form_input($data);
                                } else if ($item->tipo_campo == 2) {
                                    print form_textarea($data);
                                } else if ($item->tipo_campo == 3) {
                                    //Se for um campo booleano o mesmo acaba montando um campo Sim ou Não
                                    if ($item->tipo_valor == "boolean") {
                                        $lista = array('' => "",'t' => "Sim", 'f' => "Não");
                                    } else {

                                        $this->db->select($item->coluna_dependencia . " as id," . $item->exibicao_dependencia . " as label");
                                        $this->db->order_by($item->exibicao_dependencia);
                                        if($item->where_dependencia!=""){
                                            //Avalia se existe algum indice passado a ser substituído na cláusula
                                            foreach($indices as $in=>$in_val){
                                                if(strpos($item->where_dependencia,"{".$in."}")){
                                                    $item->where_dependencia = str_replace("{".$in."}", $in_val, $item->where_dependencia);
                                                }
                                            }
                                            $this->db->where($item->where_dependencia);
                                        }
                                        $lista[""] = "";
                                        foreach ($this->db->get($item->tabela_dependencia)->result() as $valores) {
                                            $lista[trim($valores->id)] = trim($valores->label);
                                        }
                                    }

                                    print form_dropdown($data, $lista, $data["value"]);
                                }
                                else if ($item->tipo_campo == 4) {
                                    print form_upload($data);
                                }
                                else if ($item->tipo_campo == 5) {
                                    
                                    if ($dados != "") {
                                        
                                        $data["value"] = L_label_data(trim($dados[trim($item->coluna)]), "d/m/Y");
                                    }
                                                                        
                                    print "<div class='input-group date'>";
                                        print "<div class='input-group-addon'>";
                                            print "<i class='fa fa-calendar'></i>";
                                        print "</div>";
                                        print "<div id='sandbox-container'>";
                                            print form_input($data);
                                        print "</div>";
                                    print "</div>";
                                }
                                else if ($item->tipo_campo == 6) {
                                  
                                    $infoSessao["indice_record"] = $indices;
                                    $this->session->set_userdata($infoSessao);            
       
                                     print "<div class='input-group'>";
                                        print "<div class='input-group-addon'>";
                                        print "<a href=\"javascript:Consulta(".$item->codigo_campo.",'".rtrim($item->coluna)."');\">";
                                        //print "<a href='javascript:testar();'>";
                                            print "<i class='fa fa-search-minus'></i>";
                                        print "</div>";
                                        print "</a>";
                                        print "<div id='container'>";
                                             $data["readonly"] = "readonly";
                                            print form_input($data);
                                        print "</div>";
                                    print "</div>";
                                }




                                print '</div>';
                            print "</div>";
                        print "</div>";
                    }



                    print '<div class="row">';
                        print '<div class="col-md-12 col-sm-12 col-xs-12">';
                            print form_submit("btn_salvar", "Salvar", "class='btn btn-success'");
                       
                            print form_button("btn_salvar", "Voltar", "class='btn btn-info' onclick='javascript:window.history.back();'");
                        print "</div>";
                    print "</div>";

                    print form_close();
                    ?>
                </div>
            </div>
           
        </div>
    </div>
</div>