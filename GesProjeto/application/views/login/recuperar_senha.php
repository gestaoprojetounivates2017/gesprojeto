<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
               
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <img src="<?=base_url()?>imagens/logomarca.jpg" width="100%" id="logomarca">
                        </h3>
                    </div>
                    
                    <div class="panel-body">
                         <?php echo form_open("login/recuperar_confirmar"); ?>
            <div class="form-signin">

                    <?php if($erro=="1") { ?>
                        <h4 class="form-signin-heading bg-danger">Pelo menos um dos campos deve ser preenchido</h4>
                    <?php } ?>
                    <?php if($erro=="2") { ?>
                        <h4 class="form-signin-heading bg-danger">Tentativa de acessos esgotadas. Por gentileza, solicite uma nova senha.</h4>
                    <?php } ?>    
                    <label for="inputEmail">Login</label>
                    <input type="text" id="inputEmail" name="login" class="form-control" placeholder="Login" autofocus>
                    <label for="inputPassword">ou E-mail</label>
                    <input type="text" id="inputEmail" name="email" class="form-control" placeholder="E-mail">
                    <button class="btn btn-lg btn-warning btn-block" type="submit">Recuperar</button>
                    <a href="<?= base_url() ?>login" style="text-align: center">Cancelar</a>
            </div>
            <?php echo form_close(); ?>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>


