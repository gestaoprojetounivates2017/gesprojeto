<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header-table">
                <div class="pull-left">
                    <h4 class="header_title"><?= $titulo ?></h4>
                
                </div>
                <div class="pull-right">
                    <?php
                    if ($voltar!="") {
                        $data = array(
                            "name" => "botao_voltar",
                            "class" => "btn btn-info",
                            "id"=>"Voltar",
                            "onclick" => "javascript:Novo('" . $voltar . "');"
                        );
                        print form_button($data, "Voltar");
                    }
                    ?>
                        </div>
                <div class="pull-right">
                    <?php
                    if ($botao_novo) {
                        $data = array(
                            "name" => "botao_novo",
                            "class" => "btn btn-success btn-circle",
                            "onclick" => "javascript:Novo('" . $url_novo . "');"
                        );
                        print form_button($data, "+");
                    }
                    ?>
                </div>

            </div>
        </div>


    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                $CI = & get_instance();


                $formName = $CI->dynacore->getFormName();

                $CI->dynatables->setFormName($formName);
                echo $CI->dynacore->open_form("");
                ?>
                <p><?php echo $CI->dynatables->generate(); ?></p>

            </div>

        </div>

    </div>


</form>
<!-- /page content -->
</div>



