<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
               
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <img src="<?=base_url()?>imagens/logomarca.jpg" width="100%" id="logomarca">
                            
                        </h3>
                        <h1>Cadastre-se</h1>
                    </div>
                    
                    <div class="panel-body">
                    <?php 
       
                        echo form_open("usuarios/cadastrar"); 
       
                    ?>
                        
            <div class="form-signin">

                   <?php if($erro==1) { ?>
                <h4 class="form-signin-heading bg-danger">Usuário já cadastrado!</h4>
            <?php } ?>
            <?php if($erro==2) { ?>
                <h4 class="form-signin-heading bg-warning">E-mail já utilizado!</h4>
            <?php } ?>   
                        <div class="row">
                    <div class="form-group col-md-12">
                    <label for="inputNome">Nome</label>
                    <input type="text" id="inputNome" autocomplete="off" name="nome" value="<?php if($dados) { print $dados["nome"]; } ?>" class="form-control" placeholder="Nome" required>
                </div>
                
                <div class="form-group col-md-12">
                    <label for="inputLogin">Usuário</label>
                    <input type="text" id="inputLogin" autocomplete="off" name="login" value="<?php if($dados) { print $dados["login"]; } ?>" class="form-control" placeholder="Login" required>
                </div>
                
                <div class="form-group col-md-12">
                    <label for="inputEmail">E-mail</label>
                    <input type="email" id="inputEmail" autocomplete="off" name="email" value="<?php if($dados) { print $dados["email"]; } ?>" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group col-md-12">
                    <label for="inputPassword">Senha</label>
                    <input type="password" id="inputPassword" autocomplete="off" name="senha" value="<?php if($dados) { print $dados["senha"]; } ?>" class="form-control" placeholder="Senha" required>
                </div>
                    <div class="form-group col-md-6 text-left">
                        <a href="<?= base_url() ?>login" class="btn btn-default" style="text-align: center">Cancelar</a>
                    </div>
                    <div class="form-group col-md-6 text-right">
                        <input type="submit" name="btn_salvar" id="btn_salvar" value="Salvar" class="btn btn-success">
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>


