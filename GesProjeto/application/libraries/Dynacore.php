<?php

class Dynacore
{
	protected $name;
	protected $tabs;
 
	public function __construct()
	{
		$this->setFormName('dynacore');
		$this->tabs = array();
	}

	public function setFormName($name)
	{
		$this->name = $name;
	}

	public function setActive($tabName)
	{
		if (!isset($this->tabs[$tabName]))
		{
			$this->tabs[$tabname] = new stdClass();
		}
		$this->tabs[$tabName]->active = true;
	}

	public function open_form($hidden)
	{
		if (!is_array($hidden))
		{
			$hidden = array();
		}
		$hidden['downloadType'] = '';

		$CI = &get_instance();
		$hidden['_call'] = $CI->input->post('_call');

		return  form_open('', ['name'=>$this->name, 'class'=>'form-inline', 'method'=>'post'], $hidden);
	}

	public function getFormName()
	{
		return $this->name;
	}


	public function getCallInput()
	{
		$CI = &get_instance();
		return $CI->input->post('_call');
	}

	

	

	
}