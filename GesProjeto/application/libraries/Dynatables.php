<?php

//
// Classe dynatables
//
class Dynatables
{
	protected $databaseName;
	protected $database;
	protected $table;
	protected $rows;
	protected $cols;
	protected $rowValues;
	protected $colValues;
	protected $calculus;
	protected $filterRows;
	protected $filterCalculus;
	protected $filterCols;
	protected $mainFilters;
	protected $filtertitle;
	protected $filtersubtitle;
	protected $totalRows = false;
	protected $subtotalRows = false;
	protected $totalCols = false;
	protected $mainTitle;
	protected $mainSubTitle;
	protected $call;
	protected $formname;
	protected $column;
	protected $condition;
	protected $filter;

	public function __construct()
	{
		$this->call = 'dynatables';
	}

	public function addClause($column, $condition, $filter)
	{
		$this->column = $column;
		$this->condition = $condition;
		$this->filter = $filter;
	}

	public function setFormName($formName)
	{
		$this->formname = $formName;
	}

	public function getCall()
	{
		return $this->call;
	}

	public function setfilterTitle($title)
	{
		$this->filtertitle = $title;
	}

	public function setfilterSubtitle($subtitle)
	{
		$this->filtersubtitle = $subtitle;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function setSubTitle($subTitle)
	{
		$this->subTitle = $subTitle;
	}

	// Set a database name group connection 
	// @param $databaseName (string): Connection config name
	// @returns void
	//
	public function setDatabaseName($databaseName)
	{
		$this->databaseName = $databaseName;
	}

	// Set the table name
	// @param @table (string): Table nae
	// @returns void
	//
	public function setTable($table)
	{
		$this->table = $table;
	}

	// Set the filter rows
	// @param $rows (array) Array of objects
	// @returns void
	public function setFilterRows(Array $rows)
	{
		$this->filterRows = $rows;
	}

	private function setRows($rows)
	{
		$this->rows = $rows;
	}

	// Set the filter Calculus
	// @param $rows (array) Array of objects
	// @returns void
	public function setFilterCalculus(Array $calculus)
	{
		$this->filterCalculus = $calculus;
	}

	public function setCalculus($calculus)
	{
		$this->calculus = $calculus;
	}

	public function setFilterCols(Array $cols)
	{
		$this->filterCols = $cols;
	}

	// Set the rows configurations
	// @param $rows (array) Array of objects
	// @returns void
	public function setCols(Array $cols)
	{
		$this->cols = $cols;
	}

	public function totalRows($option = false, $type = 'sum')
	{
		$this->totalRows = $option;
		$this->calcRows = 'sum';
	}

	public function subTotalRows($option = false, $type = 'sum')
	{
		$this->subtotalRows = $option;
		$this->subcalcRows = 'sum';
	}

	public function totalCols($option = false, $type = 'sum')
	{
		$this->totalCols = $option;
		$this->calcCols = $type;
	}

	protected function query()
	{
		$CI = &get_instance();
		if (!empty($this->filterCalculus))
		{
			$this->setCalculus($this->filterCalculus);
		}
		if (!empty($this->filterRows))
		{
			$levControl = array();
			$data = array();
			$groupers = array();
			$levels = array_keys($this->filterRows);
			foreach ($levels as $level)
			{
				// Tenta buscar no post
				$levelsData[$level] = $CI->input->post('rowLevel'.$level);
				if (!$levelsData[$level])
				{
					// Não tem, pega valor padrão
					$obj = reset($this->filterRows[$level]);
					$fil = key($this->filterRows[$level]);
					$levControl[] = $level;
					$data[$fil] = $obj;
				}
				else
				{
					// Tem, alimenta corretamente
					$levControl[] = $level;
					$data[$levelsData[$level]] = $this->filterRows[$level][$levelsData[$level]];
					$data[$levelsData[$level]]->selected = true;
				}

				$this->setRows($data);
			}

		}
		unset($data);

		$dataC = array();
		$calcA = array();
		$calcK = array();
		// TODO: Compatibilizar com mais níveis
		$cSelected = $CI->input->post('calculus_dynatables');
                 
                        
		if ($cSelected == false)
		{      
			$cSelected = key($this->calculus);

		}
                        
 		$calcA[] = $this->calculus[$cSelected]->calculus.'('.$cSelected.') AS '.$cSelected;
                
		$calcK[] = $cSelected;
		$this->calculus[$cSelected]->selected = true;
		// TODO: Compatibilizar com mais níveis
		$calcFormat = $this->calculus[$cSelected]->format;
		$calcs = implode(', ', $calcA);
                

		// Verifica colunas
		if (empty($this->cols))
		{
			throw new Exception('Colunas não definidas');
		}
		else
		{
			$colsA = array();
			$colsK = array();
			foreach ($this->cols as $key => $col)
			{
				$colsA[] = $key.' AS '.$key;
				$colsK[] = $key;
				$colsT[] = '\'TOTAL\' AS '.$key;

				if ($this->totalCols)
				{
					if (!empty($this->cols[$key]->aliases))
					{
						$this->cols[$key]->aliases['TOTAL'] = 'TOTAL';
					}
				}
				$colsAlias = isset($col->aliases) ? $col->aliases : null;
				$colsDefValue = isset($col->default) ? $col->default : '-';
			}
			$cols = implode(', ', $colsA);
			$colst = implode(', ', $colsT);
		}
		// Verifica linhas
		if (empty($this->rows))
		{
			throw new Exception('Linhas não definidas');
		}
		else
		{
			$rowsA = array();
			$rowsK = array();
			foreach ($this->rows as $key => $row)
			{
				$rowsA[] = $key.' AS '.$key;
				$rowsK[] = $key;
				$rowsT[] = isset($row->total) ? $row->total : null;				
			}
			$rows = implode(', ', $rowsA);
			$md = array();
			foreach ($rowsK as $ra)
			{
				$md[] = $ra;
			}
			$firstOrder = reset($rowsK);
			$rowmd = 'MD5(CONCAT('.implode(', ', $md).'))';
			$rowsGroup = implode(', ', $rowsK).', rowmd, '.implode(', ', $colsK);
		}

		$nColsT = count($this->rows)+count($this->cols);
		$nColsA = range(1, $nColsT);

		$order = implode(', ', $nColsA);
                
		$sql = 'SELECT '.$rows.', 1 AS gorder, '.$rowmd.' AS rowmd, '.$cols.', '.$calcs.', 1 as oorder
		          FROM '.$this->table;
               
		$tokenWhere = false;
		if (is_array($this->mainFilters))
		{
			foreach ($this->mainFilters as $colFilter => $filter)
			{
				if ($filter->selected)
				{
					if (!$tokenWhere)
					{
						$tokenWhere = true;
						$clause = ' WHERE ';
					}
					else
					{
						$clause = ' AND ';
					}
					// TODO: Se muda de base, escqpe_string diferente, tratar...
					$filters = array_map('addslashes', $filter->selected);
					$sql.= $clause.' '.$colFilter.' IN (\''.implode('\', \'', $filters).'\')';
				}
			}
		}
		if (!empty($this->column))
		{
			if (!$tokenWhere)
			{
				$tokenWhere = true;
				$clause = ' WHERE ';
			}
			else
			{
				$clause = ' AND ';
			}
			$sql.=$clause.' '.$this->column.' '.$this->condition.' \''.$this->filter.'\'';
		}
		$sql.=' GROUP BY '.$rowsGroup.', gorder, oorder ';

		if ($this->totalCols)
		{
			$sql.= "\n".' UNION ';
			$sql.= 'SELECT '.$rows.', 1 AS gorder, '.$rowmd.' AS rowmd, '.$colst.', '.$calcs.', 1 as oorder
			          FROM '.$this->table;

			$rowsKU = range(1, count($rowsK));
			$colsKU = range(count($rowsKU)+1, $colsK);
			$rowsGroup = implode(', ', $rowsKU).', rowmd, '.implode(', ', $colsKU);
			$tokenWhere = false;
			if (is_array($this->mainFilters))
			{
				foreach ($this->mainFilters as $colFilter => $filter)
				{
					if ($filter->selected)
					{
						if (!$tokenWhere)
						{
							$tokenWhere = true;
							$clause = ' WHERE ';
						}
						else
						{
							$clause = ' AND ';
						}
						// TODO: Se muda de base, escqpe_string diferente, tratar...
						$filters = array_map('addslashes', $filter->selected);
						$sql.= $clause.' '.$colFilter.' IN (\''.implode('\', \'', $filters).'\')';
					}
				}
			}
			if (!empty($this->column))
			{
				if (!$tokenWhere)
				{
					$tokenWhere = true;
					$clause = ' WHERE ';
				}
				else
				{
					$clause = ' AND ';
				}
				$sql.=$clause.' '.$this->column.' '.$this->condition.' \''.$this->filter.'\'';
			}
			$sql.=' GROUP BY '.$rowsGroup.', gorder, oorder';
		}

		if ($this->subtotalRows)
		{
			$sql.= "\n".' UNION ';
			if (count($this->rows)>1)
			{
				array_pop($rowsA);
				$rowsA[] = '\'SUBTOTAL\' as '.end($rowsK);
				$rowsKU = $rowsK;
				array_pop($rowsKU);
				$rowsKU[] = '\'SUBTOTAL\'';
				$rows = implode(', ', $rowsA);
				$rowmd = 'MD5(CONCAT('.implode(', ', $rowsKU).'))';
				$rowsGroup = implode(', ', $rowsKU).', rowmd, '.implode(', ', $colsK);
			}
			$sql.= 'SELECT '.$rows.', 99 AS gorder, '.$rowmd.' AS rowmd, '.$cols.', '.$calcs.', 1 AS oorder
			           FROM '.$this->table;

			$tokenWhere = false;
			if (is_array($this->mainFilters))
			{
				foreach ($this->mainFilters as $colFilter => $filter)
				{
					if ($filter->selected)
					{
						if (!$tokenWhere)
						{
							$tokenWhere = true;
							$clause = ' WHERE ';
						}
						else
						{
							$clause = ' AND ';
						}
						// TODO: Se muda de base, escqpe_string diferente, tratar...
						$filters = array_map('addslashes', $filter->selected);
						$sql.= $clause.' '.$colFilter.' IN (\''.implode('\', \'', $filters).'\')';
					}
				}
			}
			if (!empty($this->column))
			{
				if (!$tokenWhere)
				{
					$tokenWhere = true;
					$clause = ' WHERE ';
				}
				else
				{
					$clause = ' AND ';
				}
				$sql.=$clause.' '.$this->column.' '.$this->condition.' \''.$this->filter.'\'';
			}		

			$sql.=' GROUP BY '.$rowsGroup.', gorder, oorder ';

			if ($this->totalCols)
			{
				$sql.= "\n".' UNION ';
				// TODO: Suporte para 1 nível, adicionar para mais níveis
				$sql.= 'SELECT '.$rows.', 99 AS gorder, '.$rowmd.' AS rowmd, '.$colst.', '.$calcs.', 1 as oorder
				          FROM '.$this->table;

				$rowsKU = range(1, count($rowsK));
				$colsKU = range(count($rowsKU)+1, $colsK);
				$rowsGroup = implode(', ', $rowsKU).', rowmd, '.implode(', ', $colsKU);
				$tokenWhere = false;
				if (is_array($this->mainFilters))
				{
					foreach ($this->mainFilters as $colFilter => $filter)
					{
						if ($filter->selected)
						{
							if (!$tokenWhere)
							{
								$tokenWhere = true;
								$clause = ' WHERE ';
							}
							else
							{
								$clause = ' AND ';
							}
							// TODO: Se muda de base, escqpe_string diferente, tratar...
							$filters = array_map('addslashes', $filter->selected);
							$sql.= $clause.' '.$colFilter.' IN (\''.implode('\', \'', $filters).'\')';
						}
					}
				}
				if (!empty($this->column))
				{
					if (!$tokenWhere)
					{
						$tokenWhere = true;
						$clause = ' WHERE ';
					}
					else
					{
						$clause = ' AND ';
					}
					$sql.=$clause.' '.$this->column.' '.$this->condition.' \''.$this->filter.'\'';
				}
				$sql.=' GROUP BY '.$rowsGroup.', gorder, oorder';
			}
		}
		if ($this->totalRows)
		{
			$sql.= "\n".' UNION ';
			if (count($this->rows)>=1)
			{
				$rowsA = array_fill(0, count($rowsA), '\'\'');
				array_shift($rowsA);
				array_unshift($rowsA, '\'TOTAL\' as '.reset($rowsK));
				$rowsKU = range(1, count($rowsK));
				$rows = implode(', ', $rowsA);
				$rowmd = '\'TOTAL\'';
				$rowsGroup = implode(', ', $rowsKU).', rowmd, '.implode(', ', $colsK);
			}
			$sql.= 'SELECT '.$rows.', 199 AS gorder, '.$rowmd.' AS rowmd, '.$cols.', '.$calcs.', 2 as oorder
			           FROM '.$this->table;
			$tokenWhere = false;
			if (is_array($this->mainFilters))
			{
				foreach ($this->mainFilters as $colFilter => $filter)
				{
					if ($filter->selected)
					{
						if (!$tokenWhere)
						{
							$tokenWhere = true;
							$clause = ' WHERE ';
						}
						else
						{
							$clause = ' AND ';
						}
						// TODO: Se muda de base, escqpe_string diferente, tratar...
						$filters = array_map('addslashes', $filter->selected);
						$sql.= $clause.' '.$colFilter.' IN (\''.implode('\', \'', $filters).'\')';
					}
				}
			}
			if (!empty($this->column))
			{
				if (!$tokenWhere)
				{
					$tokenWhere = true;
					$clause = ' WHERE ';
				}
				else
				{
					$clause = ' AND ';
				}
				$sql.=$clause.' '.$this->column.' '.$this->condition.' \''.$this->filter.'\'';
			}
			$sql.=' GROUP BY '.$rowsGroup.', gorder, oorder ';
		}
		if (($this->totalRows) && ($this->totalCols))
		{
			$sql.= "\n".' UNION ';
			if (count($this->rows)>=1)
			{
				$rowsA = array_fill(0, count($rowsA), '\'\'');
				array_shift($rowsA);
				array_unshift($rowsA, '\'TOTAL\' as '.reset($rowsK));
				$rows = implode(', ', $rowsA);
				$rowmd = '\'TOTAL\'';
				$rowsKU = range(1, count($rowsK));
				$colsKU = range(count($rowsKU)+1, $colsK);
				$rowsGroup = implode(', ', $rowsKU).', rowmd, '.implode(', ', $colsKU);
			}
			$sql.= 'SELECT '.$rows.', 999 AS gorder, '.$rowmd.' AS rowmd, '.$colst.', '.$calcs.', 3 as oorder
			           FROM '.$this->table;
			$tokenWhere = false;
			if (is_array($this->mainFilters))
			{
				foreach ($this->mainFilters as $colFilter => $filter)
				{
					if ($filter->selected)
					{
						if (!$tokenWhere)
						{
							$tokenWhere = true;
							$clause = ' WHERE ';
						}
						else
						{
							$clause = ' AND ';
						}
						// TODO: Se muda de base, escqpe_string diferente, tratar...
						$filters = array_map('addslashes', $filter->selected);
						$sql.= $clause.' '.$colFilter.' IN (\''.implode('\', \'', $filters).'\')';
					}
				}
			}
			if (!empty($this->column))
			{
				if (!$tokenWhere)
				{
					$tokenWhere = true;
					$clause = ' WHERE ';
				}
				else
				{
					$clause = ' AND ';
				}
				$sql.=$clause.' '.$this->column.' '.$this->condition.' \''.$this->filter.'\'';
			}	
			$sql.=' GROUP BY '.$rowsGroup.', gorder, oorder ';
		}
		$sql.=' ORDER BY  oorder,'.$firstOrder.', gorder, '.$order;
               
                
		$data = $this->database->query($sql);
		$dynCols = array();
		$dynamic = array();
		$matrix = array();
		$lastRow = array();
		$rowmdLast = null;
		$acc = array();
		$total = array();

		$subtotals = array();
		foreach ($data->result() as $res)
		{
			if ($res->gorder == '99')
			{
				$subtotals[] = $res->rowmd;
			}
			$change = false;
			$dynamicCalc = array();
			foreach ($rowsK as $level => $row)
			{
				$dynamic[$row] = $res->{$row};
				if (!isset($lastRow[$row]))
				{
					$lastRow[$row] = $res->{$row};
				}
				else
				{
					if ($lastRow[$row] != $res->{$row})
					{
						$dynamic[$row] = $res->{$row};
						$lastRow[$row] = $res->{$row};
					}
					else
					{
						$dynamic[$row] = '';
					}
				}
			}
			foreach ($colsK as $col)
			{
				// TODO: Put new level on row (permits show more than one calc per line)
				foreach ($calcK as $calc)
				{
					$dynamicCalc[$res->{$col}] = $res->{$calc};
					if (!in_array($res->{$col}, $dynCols))
					{
						$dynCols[$res->{$col}] = $res->{$col};
					}
				}
			}
			if (!isset($matrix[$res->rowmd]))
			{
				$matrix[$res->rowmd] = array();
				$matrix[$res->rowmd] = $dynamic+$dynamicCalc;
			}
			else 
			{
				$matrix[$res->rowmd] = $matrix[$res->rowmd]+$dynamicCalc;
			}
		}
		if (is_array($colsAlias))
		{
			$dynCols = $colsAlias;
		}

		foreach ($matrix as $key => $m)
		{
			if (is_array($colsAlias))
			{
				foreach ($colsAlias as $origValue => $colAlias)
				{
					if (isset($matrix[$key][$origValue]))
					{
						if ($origValue != $colAlias)
						{
							$matrix[$key][$colAlias] = $matrix[$key][$origValue];
							unset($matrix[$key][$origValue]);
						}
					}
					else
					{
						$matrix[$key][$colAlias] = $colsDefValue;
					}
					if (isset($calcFormat))
					{
						if ($calcFormat['type'] == 'number')
						{
							$matrix[$key][$colAlias] = number_format($matrix[$key][$colAlias], $calcFormat['dec'], $calcFormat['sepd'], $calcFormat['sepm']);
						}
					}
				}
			}
			else
			{
				foreach ($dynCols as $dynCol)
				{
					if (!isset($m[$dynCol]))
					{
						$matrix[$key][$dynCol] = 0;
					}
					if (isset($calcFormat))
					{
						if ($calcFormat['type'] == 'number')
						{
							$matrix[$key][$dynCol] = number_format($matrix[$key][$dynCol], $calcFormat['dec'], $calcFormat['sepd'], $calcFormat['sepm']);
						}
					}
				}
			}
		}
		$result = new stdClass();
		$result->subtotals = $subtotals;
		$result->matrix = $matrix;
		$result->cols = $dynCols;
		$result->colsAlign = array();
		return $result;
	}

	protected function filterQuery()
	{
		$CI = &get_instance();
		$result = array();
		foreach ($this->mainFilters as $field => $mainFilter)
		{
			$fieldResult = array();
			if (!isset($mainFilter->table))
			{
				$table = $this->table;
			}
			else
			{
				$table = $mainFilter->table;
			}
			if (!isset($mainFilter->key))
			{
				$sql = 'SELECT DISTINCT '.$field.' FROM '.$table;
				$data = $this->database->query($sql);
				foreach ($data->result() as $d)
				{
					if (isset($mainFilter->aliases))
					{
						if (in_array($d->{$field}, array_keys($mainFilter->aliases)))
						{
							$fieldResult[$d->{$field}] = $mainFilter->aliases[$d->{$field}];
						}
						else
						{
							$fieldResult[$d->{$field}] = $d->{$field};
						}
					}
					else
					{
						$fieldResult[$d->{$field}] = $d->{$field};
					}
				}
			}
			else
			{
				$sql = 'SELECT DISTINCT '.$mainFilter->key.', '.$field.' FROM '.$table;
				$data = $this->database->query($sql);
				foreach ($data->result() as $d)
				{
					if (isset($mainFilter->aliases))
					{
						if (in_array($d->{$field}, array_keys($mainFilter->aliases)))
						{
							$fieldResult[$d->{$mainFilter->key}] = $d->{$mainFilter->aliases[$d->{$field}]};
						}
						else
						{
							$fieldResult[$d->{$mainFilter->key}] = $d->{$field};
						}
					}
					else
					{
						$fieldResult[$d->{$mainFilter->key}] = $d->{$field};
					}
				}
			}
			$result[$field] = new stdClass();
			$result[$field]->result = $fieldResult;
			$result[$field]->name = $field;
			$result[$field]->title = $mainFilter->title;
			$result[$field]->selected = $CI->input->post('filter_'.$field.'_dynatable');
			$this->mainFilters[$field]->selected = $result[$field]->selected;
		}
		return $result;
	}

	public function addMainFilter(Array $filters)
	{
		$this->mainFilters = $filters;
	}

	public function checkSubmit()
	{
		$CI = &get_instance();
		$check = $CI->input->post('rowLevel1');
		if (!empty($check))
		{
			return true;
		}
		return false;
	}

	//
	// Função para gerar a tabela
	//
	public function generate()
	{
		$CI = &get_instance();
		$this->connect();
		$out='    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">
                  <div class="panel-body">
          			<div class="x_content">  ';
                $result = $this->filterQuery();
                $count = 1;
                foreach ($result as $filter)
                {
                	$out.='
                	<div class="col-md-3">
                      <label for="filter'.$filter->name.'_dynatable">'.$filter->title.':</label>'.
                	form_multiselect('filter_'.$filter->name.'_dynatable[]', $filter->result, $filter->selected,'class="form-control" style="width: 100% !important"').'
                	'.form_submit('filter_'.$filter->name.'dynatable_submit', 'Filtrar', 'class="btn btn-success" style="width: 100% !important"').'
            		</div>';
                }


                $out.='</div>
                  </div>
            </div>';

		$result = $this->query();
		// Montar matriz

		$out.='<div class="x-title">';
                  
            if (!empty($this->filterRows))
            {
            	foreach ($this->filterRows as $level => $filterRows)
            	{
            		
            		$out.='<div class="form-group">
                      <label for="ex5">Nível '.$level.':</label>';
                    $select = array();
                    $selected = '';
                    foreach ($filterRows as $pos => $filterRow)
                    {
                    	$select[$pos] = $filterRow->name;
                    	if (isset($filterRow->selected))
                    	{
                    		$selected = $pos;
                    	}
                    }
            		$rowLevel = 'rowLevel'.$level;

            		$out.=form_dropdown($rowLevel, $select, $selected, 'class="form-control" style="margin: 3px; padding: 0px 0px 0px 0px; font-size: 12px; height: 25px;"onChange="this.form.submit()"');
            		
            		$out.='</div>';
            	}
            	$out.=' <div class="form-group">
            	        <label for="calculus_dynatables">Cálculo: </label>';
            	$select = array();
            	foreach ($this->calculus as $pos => $calculus)
            	{
            		$select[$pos] = $calculus->name;
            		if (isset($calculus->selected))
            		{
            			$selected = $pos;
            		}
            	}
            	$out.=form_dropdown('calculus_dynatables', $select, $selected,'class="form-control" style="margin: 3px; padding: 0px 0px 0px 0px; font-size: 12px; height: 25px;" onChange="this.form.submit()"')."
            		</div>";
            }
            $out.='
            </div>

              <div class="x_content">
              	<div class="table-responsive">
               	<table class="table table-striped table-bordered table-condensed">
		          <thead>
		              <tr>';
		foreach ($this->rows as $row)
		{

			$out.='            <th style="padding: 0px 5px 5px 0px; text-align: center;">'.$row->name.'</th>';
		}
		foreach ($result->cols as $col)
		{
			$out.='            <th style="padding: 0px 5px 5px 0px; text-align: center; '.($col=='TOTAL' ? 'background-color: #D0D0D0; text-color: #000000;' : '').'">'.$col.'</th>';
		}
		$out.='        </tr>
		           </thead>
		           <tbody>';
		$last = array();
		$tb = false;
		foreach ($result->matrix as $pos => $matrix)
		{
			$subtotal = false;
			if (in_array($pos, $result->subtotals))
			{
				$cpl = 'style="background-color: #E0E0E0; text-color: #000000;"';
			}
			elseif ($pos == 'TOTAL')
			{
				$cpl = 'style="background-color: #D0D0D0; text-color: #000000;"';
			}
			else
			{
				$cpl = '';
			}

			$out.='<tr '.$cpl.'>';
			foreach ($this->rows as $key => $row)
			{
				$out.='   <td style="padding: 1px 3px 3px 1px;">'.((($pos == 'TOTAL') && (!$tb)) ? 'TOTAL' : $matrix[$key]).'</td>';
				if ($pos == 'TOTAL')
				{
					$tb = true;
				}
			}
			foreach ($result->cols as $col)
			{
				$out.='   <td style="padding: 0px 5px 5px 0px; text-align: right;'.((($col=='TOTAL') || ($pos=='TOTAL')) ? ' background-color: #D0D0D0; text-color: #000000;' : '').'">'.$matrix[$col].'</td>';
			}
			$out.='</tr>';
		}
		$out.='</tbody>
				</table>';

		$out.='</div>
		</div>';

		return $out;
	}

		public function getFileVar()
	{
		$fileVar = new stdClass();
		$CI = &get_instance();
		$this->connect();
		$result = $this->filterQuery();
		$result = $this->query();
		$file = array();
		$out = array();         
		$pos = 0;

		$formatTitle = 'title';
		$formatSubTotal = 'subTotal';
		$formatTotal = 'total';

		foreach ($this->rows as $row)
		{
			$out[] = $row->name;
		}
		foreach ($result->cols as $col)
		{
			$out[] = $col;
		}
		$file[$pos] = $out;
		$format[$pos] = $formatTitle;
		$pos++;
		$tb = false;
		foreach ($result->matrix as $pos => $matrix)
		{
			$subtotal = false;
			if (in_array($pos, $result->subtotals))
			{
				$format[$pos] = $formatSubTotal;
			}
			elseif ($pos == 'TOTAL')
			{
				$format[$pos] = $formatTotal;
			}

			$out = array();
			foreach ($this->rows as $key => $row)
			{
				$out[] = ((($pos == 'TOTAL') && (!$tb)) ? 'TOTAL' : $matrix[$key]);
				if ($pos == 'TOTAL')
				{
					$tb = true;
				}
			}
			foreach ($result->cols as $col)
			{
				$out[] = $matrix[$col];
			}

			$file[$pos] = $out;
			$pos++;
		}

		$fileVar->data = $file;
		$fileVar->style = $format;
		return $fileVar;
	}

	//
	// PRIVATE: Generate the database connection
	// @returns void
	//
	private function connect()
	{
		if (!isset($this->databaseName))
		{
			throw new Exception('Nome da base de dados não definida!');
		}

		$CI =& get_instance();
        $this->database = $CI->load->database($this->databaseName, TRUE);
	}
}