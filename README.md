# GesProjeto
Projeto final de conclus�o de curso de An�lise e Desenvolvimento de Sistemas da Universidade do Vale do Taquari (2017/B)

Manual do Framework back-end
https://codeigniter.com/user_guide/


Release 1
https://drive.google.com/a/universo.univates.br/file/d/0B_HQGwGEFOS-ZWNSb25Zc3ZyRzg/view?usp=sharing

- Login
- Envio de e-mails
- Gerador de PDF
- Estruturador de dados
- Cadastro de Projetos
- Cadastro de Pa�s
- Cadastro de Estados
- Cadastro de Cidades
- Recuperador de senhas
- Cadastro de Setor


Release 2
https://drive.google.com/open?id=0B_HQGwGEFOS-YlMyTDJuLWU1VDg

- Melhorado framework
- Tratamento de logins
- Prote��o de campos
- Cria��o de menus
- Melhorias diversas no estruturador de dados da aplica��o
- Refatora��o do ModeloER
- Restrutura��o dos requisitos



Release 4
https://drive.google.com/open?id=1tKrKfA042T4_v20cBc4HKtsg3gr1n1z6

- Confirma��o antes de excluir
- Upload de arquivos
- Tabela Cruzada de custos
- Cadatro de atividades
- Cria��o de onblur e onfocus no estruturador de dados
- Cria��o de gr�ficos e informa��es na tela inicial
- Cria��o de fases padr�es e status padr�es por categoria de projeto


